// 
// Handling the menu toggle
// 

var mainNav = document.querySelector(".header-width");
function showmenu(x) {
    x.classList.toggle("change");
    mainNav.classList.toggle("open-nav");
}


// var mobileNav = document.querySelector(".mobile-nav");
// function showmenu(x) {
//     x.classList.toggle("change");
//     mobileNav.classList.toggle("open-nav");
// }



// 
// Handling the accordions
// 

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        panel.style.margin = "0px";
        if (panel.style.maxHeight){
        panel.style.maxHeight = null;
        } else {
        panel.style.maxHeight = panel.scrollHeight + 40 + "px";
        } 
    });
}





// 
// Handling the balance card toggle
// 

if (document.querySelector(".view-balance")) {
    var mainCard = document.querySelector(".left-70");
    var balanceCard = document.querySelector(".right-30");
    var balanceText = document.querySelector(".balance-text");
    var viewBalance = document.querySelector(".view-balance");

    function toggleBalance () {
        if (viewBalance.checked == false) {
            mainCard.classList.add("middle");
            balanceText.innerHTML = "Show Swinvoice balance";
            balanceCard.classList.add("hide");
        } else if (viewBalance.checked == true) {
            balanceCard.classList.remove("hide");
            balanceText.innerHTML = "Hide Swinvoice balance";
            mainCard.classList.remove("middle");
        }
    }

    viewBalance.addEventListener("click", toggleBalance)
}




// 
// Handling the button tabs
// 

if (document.querySelector(".defaultOpen")) {

    function openTab(evt, tabName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(tabName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.querySelector(".defaultOpen").click();
}
