import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserService } from  '../shared/user.service';
import { ToasterService } from '../shared/toaster.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {
    email = '';
    phone = '';
  constructor(
      private router: Router,
      private userService:UserService,
      private toaster:ToasterService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
  }
  send(){
      var data = {
          email:this.email,
          verifyBy:this.phone
      }
      this.userService.passwordReset(data).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          console.log(e)
          this.toaster.error(e.error.message)
      })
  }
}
