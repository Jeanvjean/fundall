import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { UserService } from  '../../shared/user.service';
import { ToasterService } from '../../shared/toaster.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
    password='';
  constructor(
      private router: Router,
      private userService:UserService,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
  }
  change(){
      var token = this.route.snapshot.params['token']
      var data = {
          password:this.password,
          token:token
      }
      this.userService.changePassword(data).toPromise().then((data:any)=>{
          this.spinner.hide()
          console.log(data)
          this.toaster.success(data.message)
          this.router.navigate(['/signin'])
      }).catch((e)=>{
          console.log(e)
          this.spinner.hide()
          this.toaster.error(e.error.message)
      })
  }
}
