import { SavetToBorrowComponent } from './user/savet-to-borrow/savet-to-borrow.component';
import { ProcessLoanRequestComponent } from './shared/process-loan-request/process-loan-request.component';
import { CooperativeLoanRequestComponent } from './cooperative-loan-request/cooperative-loan-request.component';
import { CooperativeloanComponent } from './cooperativeloan/cooperativeloan.component';
import { ApplyCooperativeLoanComponent } from './apply-cooperative-loan/apply-cooperative-loan.component';
import { JoinRequestComponent } from './join-request/join-request.component';
import { MemberProfileComponent } from './member-profile/member-profile.component';
import { AdminCommunityComponent } from './admin-community/admin-community.component';
import { ViewMyCommunityComponent } from './view-my-community/view-my-community.component';
import { ViewLinstingComponent } from './admin/add-listing/view-linsting/view-linsting.component';
import { Routes } from '@angular/router'
import { LoginComponent } from './user//login/login.component'
import { SignupComponent } from './user/signup/signup.component'
import { NinjaComponent } from './ninja/ninja.component'
import { EditNinjaComponent } from './ninja/edit-ninja/edit-ninja.component'
import { HomeComponent } from './home/home.component';
import { UserManagementComponent } from './admin/user-management/user-management.component';

import { UserDetailsComponent } from './admin/user-details/user-details.component';

import { ApprovalComponent } from './approval/approval.component';
import { DisburseComponent } from './disburse/disburse.component';
import { CcComponent } from './cc/cc.component';
import { DisburseLoanComponent } from './approval/disburse-loan/disburse-loan.component';

import { AdminComponent } from './admin/admin.component';
import { CreateCcComponent } from './admin/create-cc/create-cc.component';
import { CreateDisburseComponent } from './admin/create-disburse/create-disburse.component';
import { CreateApprovalComponent } from './admin/create-approval/create-approval.component';
import { ViewCcComponent } from './admin/view-cc/view-cc.component';
import { ViewDisburseComponent } from './admin/view-disburse/view-disburse.component';
import { ViewApprovalComponent } from './admin/view-approval/view-approval.component';
import { AmInfoComponent } from './admin/am-info/am-info.component';
import { DInfoComponent } from './admin/d-info/d-info.component';
import { CcInfoComponent } from './admin/cc-info/cc-info.component';
import { AddListingComponent } from './admin/add-listing/add-listing.component';
import { CreateLoanComponent } from './admin/create-loan/create-loan.component';
import { UpdateLoanComponent } from './admin/update-loan/update-loan.component';
import { ViewLoansComponent } from './admin/view-loans/view-loans.component';
import { CommunitiesComponent } from './admin/communities/communities.component';
import { CreateAdminCommunitiesComponent } from './admin/communities/create-admin-communities/create-admin-communities.component';
import { AdminAllCooprativesComponent } from './admin/communities/admin-all-coopratives/admin-all-coopratives.component';
import { CommunityDetailsComponent } from './admin/community-details/community-details.component';
import { AdminWomenInTradeComponent } from './admin/communities/admin-women-in-trade/admin-women-in-trade.component';
import { LoanDetailsComponent } from './admin/loan-details/loan-details.component';



import { UserComponent } from './user/user.component';
import { NotificationComponent } from './user/notification/notification.component';
import { ProfileComponent } from './user/profile/profile.component';
import { EditProfileComponent } from './user/edit-profile/edit-profile.component';
import { RequestLoanComponent } from './user/request-loan/request-loan.component';
import { LoanHistoryComponent } from './user/loan-history/loan-history.component';
import { PendingLoanComponent } from './user/loan-history/pending-loan/pending-loan.component';
import { ActiveLoanComponent } from './user/loan-history/active-loan/active-loan.component';
import { RejectedLoanComponent } from './user/loan-history/rejected-loan/rejected-loan.component';
import { DueLoanComponent } from './user/loan-history/due-loan/due-loan.component';

import { ConfirmEmailComponent } from './user/confirm-email/confirm-email.component';
import { UserListingDetailsComponent } from './user/listing/user-listing-details/user-listing-details.component';

import { ListingComponent } from './user/listing/listing.component';
import { CreateListingComponent } from './user/listing/create-listing/create-listing.component';
import { UpdateListingComponent } from './user/listing/update-listing/update-listing.component';
import { SettingsComponent } from './user/settings/settings.component';

import { AuthGuard } from './auth/auth.guard'
import { UserTransactionsComponent } from './user/user-transactions/user-transactions.component';
import { AccountVerifiedComponent } from './user/account-verified/account-verified.component';
import { HistoryComponent } from './user/loan-history/history/history.component';
import { LDetailsComponent } from './user/l-details/l-details.component';

import { SmeLoanComponent } from './user/request-loan/sme-loan/sme-loan.component';
import { SalaryLoanComponent } from './user/request-loan/salary-loan/salary-loan.component';
import { PayLoanComponent } from './user/request-loan/pay-loan/pay-loan.component';
import { AutoLoanComponent } from './user/request-loan/auto-loan/auto-loan.component';
import { PublicLoanComponent } from './user/request-loan/public-loan/public-loan.component';

import { WalletComponent } from './user/wallet/wallet.component';
import { AddCardComponent } from './user/wallet/add-card/add-card.component';
import { AddAccountComponent } from './user/wallet/add-account/add-account.component';
import { FundWalletComponent } from './user/wallet/fund-wallet/fund-wallet.component';

import { CreateCooperativeComponent } from './user/user-communities/create-cooperative/create-cooperative.component';
import { ViewUserCooperativeComponent } from './user/user-communities/view-user-cooperative/view-user-cooperative.component';
import { CooperativeDetailsComponent } from './user/user-communities/cooperative-details/cooperative-details.component';
import { UpdateCooperativeComponent } from './user/user-communities/update-cooperative/update-cooperative.component';

import { AutoLoanDetailsComponent } from './user/l-details/auto-loan-details/auto-loan-details.component';
import { PayLoanDetailsComponent } from './user/l-details/pay-loan-details/pay-loan-details.component';
import { SmeLoanDetailsComponent } from './user/l-details/sme-loan-details/sme-loan-details.component';
import { SalaryLoanDetailsComponent } from './user/l-details/salary-loan-details/salary-loan-details.component';
import { PublicLoanDetailsComponent } from './user/l-details/public-loan-details/public-loan-details.component';

import { CcLoanRequestsComponent } from './cc/cc-loan-requests/cc-loan-requests.component';
import { CcLoanDetailsComponent } from './cc/cc-loan-details/cc-loan-details.component';
import { DuePaymentsComponent } from './cc/due-payments/due-payments.component';

import { CcTicketsComponent } from './cc/cc-tickets/cc-tickets.component';
import { CcTicketsViewComponent } from './cc/cc-tickets/cc-tickets-view/cc-tickets-view.component';
import { CcTicketsResolvedComponent } from './cc/cc-tickets/cc-tickets-resolved/cc-tickets-resolved.component';

import { CcListingRequestsComponent } from './cc/cc-listing-requests/cc-listing-requests.component';
import { CcListingDetailComponent } from './cc/cc-listing-requests/cc-listing-detail/cc-listing-detail.component';
import { RunningListingComponent } from './cc/cc-listing-requests/running-listing/running-listing.component';
import { PendingListingRequestsComponent } from './cc/cc-listing-requests/pending-listing-requests/pending-listing-requests.component';
import { OpenTicketsComponent } from './cc/cc-tickets/open-tickets/open-tickets.component';

import { PasswordComponent } from './password/password.component';
import { ChangePasswordComponent } from './password/change-password/change-password.component';
import { ActivityComponent } from './approval/activity/activity.component';
import { ApprovalLoansComponent } from './approval/approval-loans/approval-loans.component';
import { ApprovalCommunityComponent } from './approval/approval-community/approval-community.component';


import { ApprRejectedLoansComponent } from './approval/approval-loans/appr-rejected-loans/appr-rejected-loans.component';
import { ApprApprovedLoansComponent } from './approval/approval-loans/appr-approved-loans/appr-approved-loans.component';
import { ApprRequestedLoansComponent } from './approval/approval-loans/appr-requested-loans/appr-requested-loans.component';
import { ApprAllLoansComponent } from './approval/approval-loans/appr-all-loans/appr-all-loans.component';
import { ApprLoanReqDetailsComponent } from './approval/appr-loan-req-details/appr-loan-req-details.component';
import { ApprPayLoanComponent } from './approval/approval-loans/appr-pay-loan/appr-pay-loan.component';
import { ApprPublicLoanComponent } from './approval/approval-loans/appr-public-loan/appr-public-loan.component';
import { ApprSalaryLoanComponent } from './approval/approval-loans/appr-salary-loan/appr-salary-loan.component';
import { ApprSmeLoanComponent } from './approval/approval-loans/appr-sme-loan/appr-sme-loan.component';
import { ApprAutoLoanComponent } from './approval/approval-loans/appr-auto-loan/appr-auto-loan.component';
import { ApprWitLoanComponent } from './approval/approval-loans/appr-wit-loan/appr-wit-loan.component';

import { CreateWomenInTradeComponent } from './user/user-communities/create-women-in-trade/create-women-in-trade.component';
import { ViewAllWomenInTradeComponent } from './user/user-communities/view-all-women-in-trade/view-all-women-in-trade.component';
import { ViewOneWomenInTradeComponent } from './user/user-communities/view-one-women-in-trade/view-one-women-in-trade.component';
import { UserComplainComponent } from './user/user-complain/user-complain.component';
import { ReadResponseComponent } from './user/user-complain/read-response/read-response.component';
import { UserCommunitiesComponent } from './user/user-communities/user-communities.component';
import { CcDueMessageComoponent } from './cc/cc-due-message/cc-due-message.component';

import { CcUserDetailsComponent } from './cc/cc-user-details/cc-user-details.component';
import { CcRejectedComponent } from './cc/cc-loan-details/cc-rejected/cc-rejected.component';
import { CcRequestedComponent } from './cc/cc-loan-details/cc-requested/cc-requested.component';
import { CcApprovedComponent } from './cc/cc-loan-details/cc-approved/cc-approved.component';
import { CcCommunitiesComponent } from './cc/cc-communities/cc-communities.component';
import { CcJoinCommunityComponent } from './cc/cc-communities/cc-join-community/cc-join-community.component';
import { CcMemberDetailComponent } from './cc/cc-communities/cc-member-detail/cc-member-detail.component';

export const appRoutes: Routes = [
    { path: '', component: HomeComponent,canActivate:[AuthGuard]},

    //===========================admin routes=========================//
    { path: 'admin/dashboard', component: AdminComponent },
    { path: 'admin/user-management/:type', component: UserManagementComponent },
    { path: 'admin/user-details/:user_id', component: UserDetailsComponent },

    { path: 'admin/create-loan', component: CreateLoanComponent },
    { path: 'admin/loan-management', component: ViewLoansComponent },
    { path: 'admin/update-loan/:loan_id', component: UpdateLoanComponent },
    { path: 'admin/loan_details/:loan_id', component: LoanDetailsComponent },
    //Admin manager route

    {path:'admin/customerCs',component:ViewCcComponent},
    {path:'admin/disburseManagers',component:ViewDisburseComponent},
    {path:'admin/approvalManagers',component:ViewApprovalComponent},
    {path:'admin/approval-details/:appId',component:AmInfoComponent},
    {path:'admin/disburse-details/:dId',component:DInfoComponent},
    {path:'admin/cc-details/:ccId',component:CcInfoComponent},
    {path:'admin/create/user/:type',component:CreateCcComponent},
    {path:'admin/create-disburseManager',component:CreateDisburseComponent},
    {path:'admin/create-approvalManager',component:CreateApprovalComponent},
    {path:'admin/all_coopratives',component:AdminAllCooprativesComponent},
    {path:'admin/create_cooprative',component:CreateAdminCommunitiesComponent},
    {path:'admin/cooprative_details/com_id',component:CommunityDetailsComponent},
    {path:'admin/women_in_trade',component:AdminWomenInTradeComponent},
    {path:'admin/communities',component:CommunitiesComponent},
    //listings route
    { path: "admin/listings", component: AddListingComponent },
    { path: "listing/:id", component: ViewLinstingComponent },
    { path: "view-community/:id", component: ViewMyCommunityComponent },
    { path: "admin-community/:id", component: AdminCommunityComponent },
    { path: "my-community/:id", component: ViewLinstingComponent },
    //community Route
    { path: "admin/community", component: CommunitiesComponent },
    { path: "admin/cooperatives", component: CommunityDetailsComponent },
    { path: "cooperative/member/profile/:id/:member/:process", component: MemberProfileComponent },
    { path: "cooperative/join-request/:id", component: JoinRequestComponent },
    { path: "cooperative/request-loan/:id", component: ApplyCooperativeLoanComponent },

    //managers route
    { path: 'approval-manager/dashboard', component: ApprovalComponent },
    { path: 'approval-manager/activity', component: ActivityComponent },
    { path: 'approval-manager/community', component: ApprovalCommunityComponent },
    { path: 'request_details/:request_id', component: ApprLoanReqDetailsComponent },

    { path: 'approval-manager/public_loans', component: ApprPublicLoanComponent },
    { path: 'approval-manager/salary_loans', component: ApprSalaryLoanComponent },
    { path: 'approval-manager/sme_loans', component: ApprSmeLoanComponent },
    { path: 'approval-manager/women_in_trade_loans', component: ApprWitLoanComponent },
    { path: 'approval-manager/auto_loans', component: ApprAutoLoanComponent },
    { path: 'loans/disbursed', component: ApprAllLoansComponent },

    //fix for disbursement manager
    {
        path: 'pending_disbursements', component: ApprovalLoansComponent,
        children: [{ path: '', component: ApprApprovedLoansComponent }]
    },
    //fix for disbursement manager
    {
        path: 'repaid_loans', component: ApprovalLoansComponent,
        children: [{ path: '', component: ApprRejectedLoansComponent }]
    },
    {
        path: 'requested_loans', component: ApprovalLoansComponent,
        children: [{ path: '', component: ApprRequestedLoansComponent }]
    },
    {
        path: 'active_loans', component: ApprovalLoansComponent,
        children: [{ path: '', component: ApprPayLoanComponent }]
    },


    { path: 'disburse-manager/dashboard', component: DisburseComponent },

    {path:'customer-care-agent/dashboard',component:CcComponent},
    {path:'customercare/due_payments',component:DuePaymentsComponent},
    {path:'customercare/communities',component:CcCommunitiesComponent},
    {path:'customercare/join_community',component:CcJoinCommunityComponent},
    {path:'customercare/member_details',component:CcMemberDetailComponent},

    {path:'customercare/loan_requests_detail/:request_id',component:CcLoanRequestsComponent},
    {path:'customercare/view_user_rejected_loans',component:CcLoanDetailsComponent,
        children:[{path:'',component:CcRejectedComponent}]},
    {path:'customercare/view_user_requested_loans',component:CcLoanDetailsComponent,
        children:[{path:'',component:CcRequestedComponent}]},
    {path:'customercare/view_user_approved_loans',component:CcLoanDetailsComponent,
        children:[{path:'',component:CcApprovedComponent}]},

    {path:'customercare/view_listing_details/:listing_id',component:CcListingDetailComponent},
    {path:'view_user_details/:user_id',component:CcUserDetailsComponent},

    {path:'customercare/send_loan_reminder_details/:user_id',component:CcDueMessageComoponent},

    {path:'customercare/pending_listing_request',component:CcListingRequestsComponent,
        children:[{path:'',component:PendingListingRequestsComponent}]},
    {path:'customercare/running_listings',component:CcListingRequestsComponent,
        children:[{path:'',component:RunningListingComponent}]},

    {path:'customercare/tickets/:ticket_id',component:CcTicketsViewComponent},
    {path:'customercare/open_tickets',component:CcTicketsComponent,
        children:[{path:'',component:OpenTicketsComponent}]},
    {path:'customercare/resolved_tickets',component:CcTicketsComponent,
        children:[{path:'',component:CcTicketsResolvedComponent}]},

    //authentication route
    { path: 'signin', component: LoginComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'forgot_password', component: PasswordComponent },
    { path: 'change-password/:token', component: ChangePasswordComponent },

    //========================user routes====================================//
    { path: 'u/dashboard', component: UserComponent },
    { path: 'u/profile', component: ProfileComponent },
    { path: 'u/update-profile', component: EditProfileComponent },
    { path: 'u/my-communities', component: UserCommunitiesComponent },
    //userLoan Route
    { path: 'u/due-loans', component: DueLoanComponent },
    { path: 'u/confirmation-email', component: ConfirmEmailComponent },
    { path: 'u/loan-detail/:loan_id', component: LDetailsComponent },

    {
        path: 'u/active-loans', component: LoanHistoryComponent,
        children: [{ path: '', component: ActiveLoanComponent }]
    },
    {
        path: 'u/pending-loans', component: LoanHistoryComponent,
        children: [{ path: '', component: PendingLoanComponent }]
    },
    {
        path: 'u/rejected-loans', component: LoanHistoryComponent,
        children: [{ path: '', component: RejectedLoanComponent }]
    },
    {
        path: 'u/loan-history', component: LoanHistoryComponent,
        children: [{ path: '', component: HistoryComponent }]
    },
    //new route>
    { path: 'u/complains', component: UserComplainComponent },
    { path: 'u/response', component: ReadResponseComponent },

    { path: 'u/listings', component: ListingComponent },
    { path: 'u/request-listing', component: CreateListingComponent },
    { path: 'u/update-listing-request', component: UpdateListingComponent },
    { path: 'listing-details', component: UserListingDetailsComponent },
    { path: 'view-listings/:type', component: UserListingDetailsComponent },

    { path: 'u/settings', component: SettingsComponent },
    { path: 'u/transactions', component: UserTransactionsComponent },
    //loan request
    { path: 'u/request_sme_loan', component: SmeLoanComponent },
    { path: 'u/request_salary_advance_loan', component: SalaryLoanComponent },
    { path: 'u/request_pay_loan', component: PayLoanComponent },
    { path: 'u/request_auto_loan', component: AutoLoanComponent },
    { path: 'u/request_publicsector_loan', component: PublicLoanComponent },
    //cooprative routes
    { path: 'coopratives/:community', component: CreateCooperativeComponent },
    { path: 'coopratives/loans/:community', component: CooperativeloanComponent },
    { path: 'coopratives/loans/:community/:user', component: CooperativeloanComponent },
    { path: 'coopratives/loan/requests/:community', component:CooperativeLoanRequestComponent},
    { path: 'coopratives/loan/processrequest/:community/:user/:loan', component:ProcessLoanRequestComponent},
    // { path: 'coopratives/loans/:community/:user', component: CooperativeloanComponent },
    { path: 'coopratives/loans/:community/:user/:filter', component: CooperativeloanComponent },
    { path: 'communities/all/:type', component: ViewUserCooperativeComponent },
    { path: 'u/update-cooprative/:id', component: UpdateCooperativeComponent },
    { path: 'u/coopratives', component: ViewUserCooperativeComponent },
    { path: 'u/cooprative/:id', component: CooperativeDetailsComponent },
    { path: 'u/create_women_in_trade_society', component: CreateWomenInTradeComponent },
    { path: 'u/all_women_in_trade_societies', component: ViewAllWomenInTradeComponent },
    { path: 'u/view_women_in_trade_societies_detail/:w_id', component: ViewOneWomenInTradeComponent },


    //======== APPROPRIATE ROUTE STRUCTURE FOR PROPER UI ========//
    //-------- Format: <unique-parent-identifier>/<child>... --------//
    { path: 'home', component: UserComponent },
    { path: 'home/notifications', component: NotificationComponent },
    { path: 'home/auto-loan', component: AutoLoanDetailsComponent },
    { path: 'home/pay-loan', component: PayLoanDetailsComponent },
    { path: 'home/salary-loan', component: SalaryLoanDetailsComponent },
    { path: 'home/sme-loan', component: SmeLoanDetailsComponent },
    { path: 'home/public-loan', component: PublicLoanDetailsComponent },

    { path: 'profile', component: ProfileComponent },
    { path: 'profile/update-profile', component: EditProfileComponent },

    {
        path: 'loans', component: LoanHistoryComponent,
        children: [{ path: '', component: ActiveLoanComponent }]
    },
    {
        path: 'loans/pending', component: LoanHistoryComponent,
        children: [{ path: '', component: PendingLoanComponent }]
    },
    {
        path: 'loans/rejected', component: LoanHistoryComponent,
        children: [{ path: '', component: RejectedLoanComponent }]
    },
    {
        path: 'loans/history', component: LoanHistoryComponent,
        children: [{ path: '', component: HistoryComponent }]
    },

    { path: 'due', component: DueLoanComponent },

    { path: 'community', component: UserCommunitiesComponent },
    { path: 'community/all/:type', component: ViewUserCooperativeComponent },
    { path: 'community/coopratives/:community', component: CreateCooperativeComponent },


    //wallet routes
    { path: 'my-wallet', component: WalletComponent },
    { path: 'u/fund-wallet', component: FundWalletComponent },
    { path: 'u/add-card', component: AddCardComponent },
    { path: 'u/add-account', component: AddAccountComponent },
    //loan description routes
    { path: 'auto-loan/description', component: AutoLoanDetailsComponent },
    { path: 'pay-loan/description', component: PayLoanDetailsComponent },
    { path: 'salary-loan/description', component: SalaryLoanDetailsComponent },
    { path: 'sme-loan/description', component: SmeLoanDetailsComponent },
    { path: 'public-loan/description', component: PublicLoanDetailsComponent },
    //verify user token
    { path: 'verify-user/:token', component: AccountVerifiedComponent },


    //darangi's not so shitty Routes
    //request loan
    { path: 'loan/request/:Name/:Id/:interest', component: RequestLoanComponent },
    { path: 'savetoborrow', component: SavetToBorrowComponent },

    //default route
    { path: '**', redirectTo: '/', pathMatch: 'prefix' }
]
