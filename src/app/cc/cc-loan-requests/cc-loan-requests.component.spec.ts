import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcLoanRequestsComponent } from './cc-loan-requests.component';

describe('CcLoanRequestsComponent', () => {
  let component: CcLoanRequestsComponent;
  let fixture: ComponentFixture<CcLoanRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcLoanRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcLoanRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
