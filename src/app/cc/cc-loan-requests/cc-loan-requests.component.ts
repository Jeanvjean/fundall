import { BASE_API } from './../../shared/api.service';
import { Component, OnInit } from '@angular/core';
import { LoanService } from '../../shared/loan.service';
import { UserService } from '../../shared/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-cc-loan-requests',
    templateUrl: './cc-loan-requests.component.html',
    styleUrls: ['./cc-loan-requests.component.css']
})
export class CcLoanRequestsComponent implements OnInit {
    reason = '';
    r = {
      amount:'',
      accountHolder:'',
      duration:'',
      salaryEarned:'',
      interestRate:'',
      total_cost:'',
      applicationState:'',
      disbursed:'',
      goToApproval:false,
      bankStatement:'',
        user: {
            firstName: '',
            lastName: '',
            identificationImage: '',
            image: ".../../../assets/fundal/admin/Profile (3) (1).png",
            phone:'',
            email:'',
        }
    };
    p: number = 1;
    constructor(
        private loanService: LoanService,
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService,
        private spinner: NgxSpinnerService,
        private toaster: ToasterService) { }

    ngOnInit() {
        this.loanRequests()
    }
    loanRequests() {
        this.spinner.show()
        var req_id = this.route.snapshot.params['request_id']
        this.loanService.loanRequestDetails(req_id).toPromise().then((data: any) => {
            this.r = data.result;
            // if(this.r.user.image){
            this.r.user.image = BASE_API + this.r.user.image;
            // }
            this.toaster.success(data.message)
            this.spinner.hide()
            console.log(data)
        }).catch((e) => {
            this.spinner.hide()
            this.toaster.error(e.error.message)
            console.log(e)
        })
    }
    reject() {
        var reason = {
            reason: this.reason
        }
        var req_id = this.route.snapshot.params['request_id']
        this.loanService.rejectLoan(req_id, reason).toPromise().then((data: any) => {
            console.log(data)
        }).catch((e) => {
            console.log(e)
        })
    }
    approve() {
        var req_id = this.route.snapshot.params['request_id']
        this.spinner.show();
        this.loanService.approveLoan(req_id).toPromise().then((data: any) => {
            if (data.success) {
                this.toaster.success(data.message);
            }
            else {
                this.toaster.error(data.message);
            }

            this.spinner.hide()
            this.router.navigate(["customercare/view_user_requested_loans"]);
        }).catch((e:any) => {
            this.toaster.error("An error occurred");
            this.spinner.hide()

        })
    }
    logout(){
        localStorage.clear()
        this.router.navigate(['/signin'])
    }
}
