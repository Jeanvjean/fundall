import { Component, OnInit } from '@angular/core';
import { LoanService } from '../../shared/loan.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-cc-loan-details',
  templateUrl: './cc-loan-details.component.html',
  styleUrls: ['./cc-loan-details.component.css']
})
export class CcLoanDetailsComponent implements OnInit {
    request = {};
  constructor(
      private loanService:LoanService,
      private router:Router,
      private route:ActivatedRoute,
      private spinner:NgxSpinnerService,
      private toaster:ToasterService) { }

      ngOnInit() {
          // this.details()
      }
      // details(){
      //     this.spinner.show()
      //     var req_id = this.route.snapshot.params['req_id']
      //     this.loanService.loanRequestDetails(req_id).toPromise().then((data:any)=>{
      //         this.request = data.result
      //         this.spinner.hide()
      //     }).catch((e)=>{
      //         this.spinner.hide()
      //         this.toaster.error(e.error.message)
      //         console.log(e)
      //     })
      // }
      // reject(){
      //     this.spinner.show()
      //     var req_id = this.route.snapshot.params['req_id']
      //     this.loanService.rejectLoan(req_id).toPromise().then((data:any)=>{
      //         this.spinner.hide()
      //         this.toaster.success(data.message)
      //     }).catch((e)=>{
      //         this.spinner.hide()
      //         this.toaster.error(e.error.message)
      //         console.log(e)
      //     })
      // }
      // approve(){
      //     this.spinner.show()
      //     var req_id = this.route.snapshot.params['req_id']
      //     this.loanService.approveLoan(req_id).toPromise().then((data:any)=>{
      //         this.spinner.hide()
      //         this.toaster.success(data.message)
      //     }).catch((e)=>{
      //         this.spinner.hide()
      //         this.toaster.error(e.error.message)
      //         console.log(e)
      //     })
      // }

}
