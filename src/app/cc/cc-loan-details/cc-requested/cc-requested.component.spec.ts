import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcRequestedComponent } from './cc-requested.component';

describe('CcRequestedComponent', () => {
  let component: CcRequestedComponent;
  let fixture: ComponentFixture<CcRequestedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcRequestedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcRequestedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
