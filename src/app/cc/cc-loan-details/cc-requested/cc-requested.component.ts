import { Component, OnInit } from '@angular/core';
import { LoanService } from '../../../shared/loan.service';
import { CcService } from '../../../shared/cc.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-cc-requested',
  templateUrl: './cc-requested.component.html',
  styleUrls: ['./cc-requested.component.css']
})
export class CcRequestedComponent implements OnInit {
    loan:any = '';
    p:number = 1;
    reason = '';
    firstName:'';
  constructor(
      private loanService:LoanService,
      private router:Router,
      private ccService:CcService,
      private spinner:NgxSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.pending_loan_requests()
  }
  pending_loan_requests(){
      this.ccService.get_all_pending_loan().toPromise().then((data:any)=>{
          this.loan = data.result.loans
      }).catch((e)=>{
          console.log(e)
      })
  }
  reject(req_id){
      // var req_id = this.route.snapshot.params['request_id']
      var reason = this.reason
      this.loanService.rejectLoan(req_id,reason).toPromise().then((data:any)=>{
        this.toaster.success(data.message)
          console.log(data)
      }).catch((e)=>{
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  approve(req_id){
      // var req_id = this.route.snapshot.params['request_id']
      this.loanService.approveLoan(req_id).toPromise().then((data:any)=>{
        this.toaster.success(data.message)
          console.log(data)
      }).catch((e)=>{
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
