import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcLoanDNavComponent } from './cc-loan-d-nav.component';

describe('CcLoanDNavComponent', () => {
  let component: CcLoanDNavComponent;
  let fixture: ComponentFixture<CcLoanDNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcLoanDNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcLoanDNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
