import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcApprovedComponent } from './cc-approved.component';

describe('CcApprovedComponent', () => {
  let component: CcApprovedComponent;
  let fixture: ComponentFixture<CcApprovedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcApprovedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
