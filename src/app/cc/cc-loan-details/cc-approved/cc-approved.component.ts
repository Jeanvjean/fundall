import { Component, OnInit } from '@angular/core';
import { LoanService } from '../../../shared/loan.service';
import { CcService } from '../../../shared/cc.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-cc-approved',
  templateUrl: './cc-approved.component.html',
  styleUrls: ['./cc-approved.component.css']
})
export class CcApprovedComponent implements OnInit {
    loan:any = '';
    p:number = 1;
    firstName:'';
  constructor(
      private loanService:LoanService,
      private router:Router,
      private ccService:CcService,
      private spinner:NgxSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.approved_loan_requests()
  }
  approved_loan_requests(){
      this.ccService.get_all_approved_loans().toPromise().then((data:any)=>{
          this.loan = data.result
          console.log(data)
      }).catch((e)=>{
          console.log(e)
      })
  }
}
