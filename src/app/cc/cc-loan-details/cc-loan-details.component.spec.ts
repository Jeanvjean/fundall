import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcLoanDetailsComponent } from './cc-loan-details.component';

describe('CcLoanDetailsComponent', () => {
  let component: CcLoanDetailsComponent;
  let fixture: ComponentFixture<CcLoanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcLoanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcLoanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
