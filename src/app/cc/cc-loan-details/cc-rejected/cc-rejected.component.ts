import { Component, OnInit } from '@angular/core';
import { LoanService } from '../../../shared/loan.service';
import { CcService } from '../../../shared/cc.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-cc-rejected',
  templateUrl: './cc-rejected.component.html',
  styleUrls: ['./cc-rejected.component.css']
})
export class CcRejectedComponent implements OnInit {
    loan:any = '';
    p:number = 1;
    firstName = '';
  constructor(
      private loanService:LoanService,
      private router:Router,
      private ccService:CcService,
      private spinner:NgxSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.rejected_loan_requests()
  }
  rejected_loan_requests(){
      this.ccService.get_all_rejected_loans().toPromise().then((data:any)=>{
          this.loan = data.result.loans
          console.log(data)
      }).catch((e)=>{
          console.log(e)
      })
  }
}
