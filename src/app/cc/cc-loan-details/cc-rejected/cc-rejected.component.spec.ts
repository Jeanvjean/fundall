import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcRejectedComponent } from './cc-rejected.component';

describe('CcRejectedComponent', () => {
  let component: CcRejectedComponent;
  let fixture: ComponentFixture<CcRejectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcRejectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcRejectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
