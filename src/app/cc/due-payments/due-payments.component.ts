import { Component, OnInit } from '@angular/core';
import { LoanService } from '../../shared/loan.service';
import { CcService } from '../../shared/cc.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-due-payments',
  templateUrl: './due-payments.component.html',
  styleUrls: ['./due-payments.component.css']
})
export class DuePaymentsComponent implements OnInit {
    due:any = '';
    p:number = 1;
    firstname = '';
    lastname = '';
  constructor(
      private ccService:CcService,
      private router:Router,
      private toaster:ToasterService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.dueLoan()
  }
  dueLoan(){
    this.spinner.show()
      this.ccService.getDuePayments().toPromise().then((data:any)=>{
        this.spinner.hide()
        this.toaster.success(data.message)
          console.log(data)
          this.due = data.result
      }).catch((e)=>{
        this.spinner.hide()
        this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  logout(){
      localStorage.clear()
      this.router.navigate(['/signin'])
  }
}
