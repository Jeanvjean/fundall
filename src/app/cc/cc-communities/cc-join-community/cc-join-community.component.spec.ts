import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcJoinCommunityComponent } from './cc-join-community.component';

describe('CcJoinCommunityComponent', () => {
  let component: CcJoinCommunityComponent;
  let fixture: ComponentFixture<CcJoinCommunityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcJoinCommunityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcJoinCommunityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
