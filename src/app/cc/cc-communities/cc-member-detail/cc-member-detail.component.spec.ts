import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcMemberDetailComponent } from './cc-member-detail.component';

describe('CcMemberDetailComponent', () => {
  let component: CcMemberDetailComponent;
  let fixture: ComponentFixture<CcMemberDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcMemberDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcMemberDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
