import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcCommunitiesComponent } from './cc-communities.component';

describe('CcCommunitiesComponent', () => {
  let component: CcCommunitiesComponent;
  let fixture: ComponentFixture<CcCommunitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcCommunitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcCommunitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
