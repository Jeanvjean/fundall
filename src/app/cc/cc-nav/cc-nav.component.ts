import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'cc-nav',
  templateUrl: './cc-nav.component.html',
  styleUrls: ['./cc-nav.component.css']
})
export class CcNavComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  logout(){
      localStorage.clear()
      this.router.navigate(['/signin'])
  }
}
