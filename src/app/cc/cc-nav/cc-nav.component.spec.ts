import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcNavComponent } from './cc-nav.component';

describe('CcNavComponent', () => {
  let component: CcNavComponent;
  let fixture: ComponentFixture<CcNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
