import { Component, OnInit } from '@angular/core';
import { ListingService } from '../../../shared/listing.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-pending-listing-requests',
  templateUrl: './pending-listing-requests.component.html',
  styleUrls: ['./pending-listing-requests.component.css']
})
export class PendingListingRequestsComponent implements OnInit {
    listings:any = '';
    p:number = 1;
    company:'';
    page = 1;

  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private listingService:ListingService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.pending_listing()
  }
  pending_listing(){
      this.spinner.show()
      
      this.listingService.pending_listing(this.page).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.info(data.message)
          this.listings = data.result
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          this.spinner.hide()
          console.log(e)
      }) 
  }
  approve(list_id){
      if(!confirm("Approve?"))
        return;
      this.spinner.show()
      this.listingService.approve(list_id).toPromise().then((data:any)=>{
          this.spinner.hide();
          
          this.toaster.success(data.message);
          this.pending_listing();
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          
      })
  }
  reject(list_id){
      if(!confirm('Reject?'))
        return;

      this.spinner.show()
      this.listingService.reject(list_id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          this.pending_listing();
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  delete(list_id){
      if(!confirm("Delete?"))
        return;

      this.spinner.show()
      this.listingService.delete(list_id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  nextPage(){
      if(this.page == this.listings.pages){
          return;
      }
      this.page++;
      this.pending_listing();
  }
  previousPage(){
      if(this.page == 1){
        return;
      }
      this.page--;
      this.pending_listing();
  }
}
