import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingListingRequestsComponent } from './pending-listing-requests.component';

describe('PendingListingRequestsComponent', () => {
  let component: PendingListingRequestsComponent;
  let fixture: ComponentFixture<PendingListingRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingListingRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingListingRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
