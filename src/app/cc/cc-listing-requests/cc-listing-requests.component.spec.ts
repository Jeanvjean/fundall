import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcListingRequestsComponent } from './cc-listing-requests.component';

describe('CcListingRequestsComponent', () => {
  let component: CcListingRequestsComponent;
  let fixture: ComponentFixture<CcListingRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcListingRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcListingRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
