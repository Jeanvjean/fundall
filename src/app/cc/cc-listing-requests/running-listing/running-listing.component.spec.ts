import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunningListingComponent } from './running-listing.component';

describe('RunningListingComponent', () => {
  let component: RunningListingComponent;
  let fixture: ComponentFixture<RunningListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunningListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunningListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
