import { Component, OnInit } from '@angular/core';
import { ListingService } from '../../../shared/listing.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-running-listing',
  templateUrl: './running-listing.component.html',
  styleUrls: ['./running-listing.component.css']
})
export class RunningListingComponent implements OnInit {
    listings:any = '';
    p:number = 1;
    company:'';
    active:boolean = false;
    suspended:boolean = false;
    blocked:boolean = false;
  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private listingService:ListingService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.running_listing()
  }
  running_listing(){
      this.spinner.show()
      this.listingService.running_listing().toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.info(data.message)
          this.listings = data.result
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          // this.toaster.error(e.error.message)
          this.spinner.hide()
          console.log(e)
      })
  }
}
