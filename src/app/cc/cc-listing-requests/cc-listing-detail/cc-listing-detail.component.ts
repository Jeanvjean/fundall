import { BASE_API } from './../../../shared/api.service';
import { Component, OnInit } from '@angular/core';
import { ListingService } from '../../../shared/listing.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-cc-listing-detail',
  templateUrl: './cc-listing-detail.component.html',
  styleUrls: ['./cc-listing-detail.component.css']
})
export class CcListingDetailComponent implements OnInit {
    listing:any = {
        name:'',
        company:'',
        date_created:'',
        description:'',
        active:'',
        advert_image:'',
        _id:'',
        listingState:'',
        approved:'',
    }
  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private listingService:ListingService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.viewListing()
  }
  viewListing(){
      this.spinner.show()
      var list_id = this.route.snapshot.params['listing_id']
      this.listingService.viewListing(list_id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          this.listing = data.result
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  suspend(){
      this.spinner.show()
      var id = this.route.snapshot.params['listing_id']
      this.listingService.suspend_listing(id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  block(){
      this.spinner.show()
      var id = this.route.snapshot.params['listing_id']
      this.listingService.block_listing(id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  activate(){
      this.spinner.show()
      var id = this.route.snapshot.params['listing_id']
      this.listingService.activate_listing(id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  delete(){
      this.spinner.show()
      var id = this.route.snapshot.params['listing_id']
      this.listingService.delete(id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  approve(){
      this.spinner.show()
      var list_id = this.route.snapshot.params['listing_id']
      this.listingService.approve(list_id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  reject(){
      this.spinner.show()
      var list_id = this.route.snapshot.params['listing_id']
      this.listingService.reject(list_id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  setImage(img) {
    return BASE_API + img;
    }
}
