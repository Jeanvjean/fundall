import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcListingDetailComponent } from './cc-listing-detail.component';

describe('CcListingDetailComponent', () => {
  let component: CcListingDetailComponent;
  let fixture: ComponentFixture<CcListingDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcListingDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcListingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
