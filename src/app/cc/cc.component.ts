import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cc',
  templateUrl: './cc.component.html',
  styleUrls: ['./cc.component.css']
})
export class CcComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
      var role = localStorage.getItem('role')
      if(role == '1'){
          this.router.navigate(['/admin/dashboard'])
      }else if(role =='2'){
          this.router.navigate(['/approval-manager/dashboard'])
      }else if(role =='3'){
          this.router.navigate(['/disburse-manager/dashboard'])
      }else if(role == '4'){
          this.router.navigate(['/customer-care-agent/dashboard'])
      }else if(role == '5'){
          this.router.navigate(['/u/dashboard'])
      }
  }

}
