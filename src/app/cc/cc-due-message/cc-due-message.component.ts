import { Component, OnInit } from '@angular/core';
import { LoanService } from '../../shared/loan.service';
import { CcService } from '../../shared/cc.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-cc-due-message',
  templateUrl: './cc-due-message.component.html',
  styleUrls: ['./cc-due-message.component.css']
})
export class CcDueMessageComoponent implements OnInit {
    message = '';
  constructor(
      private ccService:CcService,
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
  }
  send_message(){
      this.spinner.show()
      var user_id = this.route.snapshot.params['user_id']
      var data = {
          message:this.message,
          accountHolder:user_id
      }
      this.ccService.duePaymentReminder(data).toPromise().then((data:any)=>{
          console.log(data)
          this.spinner.hide()
          this.toaster.success(data.message)
      }).catch((e)=>{
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
