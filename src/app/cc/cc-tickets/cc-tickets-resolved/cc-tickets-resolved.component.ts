import { Component, OnInit } from '@angular/core';
import { ComplainService } from '../../../shared/complain.service';
import { CcService } from '../../../shared/cc.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-cc-tickets-resolved',
  templateUrl: './cc-tickets-resolved.component.html',
  styleUrls: ['./cc-tickets-resolved.component.css']
})
export class CcTicketsResolvedComponent implements OnInit {
    ticket:any = '';
  constructor(
      private ccService:CcService,
      private router:Router,
      private toaster:ToasterService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.resolved()
  }
  resolved(){
      this.spinner.show()
      this.ccService.fetch_all_resolved().toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          console.log(data)
          this.ticket = data.result
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
