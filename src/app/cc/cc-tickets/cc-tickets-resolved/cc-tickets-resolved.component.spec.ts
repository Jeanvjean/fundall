import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcTicketsResolvedComponent } from './cc-tickets-resolved.component';

describe('CcTicketsResolvedComponent', () => {
  let component: CcTicketsResolvedComponent;
  let fixture: ComponentFixture<CcTicketsResolvedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcTicketsResolvedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcTicketsResolvedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
