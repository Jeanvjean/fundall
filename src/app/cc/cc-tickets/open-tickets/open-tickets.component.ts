import { Component, OnInit } from '@angular/core';
import { CcService } from '../../../shared/cc.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-open-tickets',
  templateUrl: './open-tickets.component.html',
  styleUrls: ['./open-tickets.component.css']
})
export class OpenTicketsComponent implements OnInit {
    complains:any = '';
  constructor(
      private ccService:CcService,
      private router:Router,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

      ngOnInit() {
          this.open_complains()
      }
      open_complains(){
          this.spinner.show()
          this.ccService.fetch_all_open_complains().toPromise().then((data:any)=>{
              console.log(data)
              this.spinner.hide()
              this.complains = data.result
              this.toaster.success(data.message)
          }).catch((e)=>{
              this.spinner.hide()
              this.toaster.error(e.error.message)
              console.log(e)
          })
      }
      mark_as_read(complaint_id){
          this.ccService.mark_complaint_asresolved(complaint_id).toPromise().then((data:any)=>{
              console.log(data)
          }).catch((e)=>{
              console.log(e)
          })
      }

}
