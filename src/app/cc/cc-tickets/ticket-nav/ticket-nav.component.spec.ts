import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketNavComponent } from './ticket-nav.component';

describe('TicketNavComponent', () => {
  let component: TicketNavComponent;
  let fixture: ComponentFixture<TicketNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
