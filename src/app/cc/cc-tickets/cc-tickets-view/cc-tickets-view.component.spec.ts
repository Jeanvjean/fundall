import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcTicketsViewComponent } from './cc-tickets-view.component';

describe('CcTicketsViewComponent', () => {
  let component: CcTicketsViewComponent;
  let fixture: ComponentFixture<CcTicketsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcTicketsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcTicketsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
