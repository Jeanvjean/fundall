import { Component, OnInit } from '@angular/core';
import { ComplainService } from '../../../shared/complain.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-cc-tickets-view',
  templateUrl: './cc-tickets-view.component.html',
  styleUrls: ['./cc-tickets-view.component.css']
})
export class CcTicketsViewComponent implements OnInit {
    complain = {
        complaint:'',
        complaintBy:'',
        responses:[]
     };
    response = '';

  constructor(
      private complainService:ComplainService,
      private route:ActivatedRoute,
      private router:Router,
      private spinner:NgxSpinnerService,
      private toaster:ToasterService) { }

      ngOnInit() {
          this.read()
      }
      read(){
          var comp_id = this.route.snapshot.params['ticket_id']
          this.complainService.read(comp_id).toPromise().then((data:any)=>{
              this.complain = data.result
              console.log(data)
              this.spinner.hide()
          }).catch((e)=>{
              console.log(e)
          })
      }
      respond(){
          var comp_id = this.route.snapshot.params['ticket_id']
          var data = {response:this.response}
          this.complainService.respondToComplain(comp_id,data).toPromise().then((data:any)=>{
              this.spinner.hide()
              console.log(data)
              window.location.reload()
              this.toaster.success(data.message)
          }).catch((e)=>{
              this.spinner.hide()
              console.log(e)
          })
      }

}
