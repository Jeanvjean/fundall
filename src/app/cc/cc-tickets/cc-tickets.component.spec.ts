import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcTicketsComponent } from './cc-tickets.component';

describe('CcTicketsComponent', () => {
  let component: CcTicketsComponent;
  let fixture: ComponentFixture<CcTicketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcTicketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
