import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcUserDetailsComponent } from './cc-user-details.component';

describe('CcUserDetailsComponent', () => {
  let component: CcUserDetailsComponent;
  let fixture: ComponentFixture<CcUserDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcUserDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcUserDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
