import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/user.service';
import { CcService } from '../../shared/cc.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-cc-user-details',
  templateUrl: './cc-user-details.component.html',
  styleUrls: ['./cc-user-details.component.css']
})
export class CcUserDetailsComponent implements OnInit {
    difImage="/assets/fundal/admin/Profile (3) (3).png";
    message='';
    user:any = {
        firstName:'',
        lastName:'',
        email:'',
        identificationImage:'',
        isVerified:false,
        phone:'',
        amountOwed:'',
        loanTaken:'',
        listings:'',
        lastDisbursal:'',
        suspended:'',
        lastLogin:''
    };
    coop = [];
    w = [];
    taken = [];
    owed = 0;
  constructor(
      private userService:UserService,
      private ccService:CcService,
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private spinner:Ng4LoadingSpinnerService) { }

      ngOnInit() {
          this.getUser()
          this.user_coopratives()
          this.fetch_all_user_women_group()
          this.fetch_all_user_loan()
      }
      getUser(){
          this.spinner.show()
          var user_id = this.route.snapshot.params['user_id']
          this.userService.userDetails(user_id).toPromise().then((data:any)=>{
              this.user = data.result
              this.difImage= data.result.identificationImage
              this.spinner.hide()
              console.log(data)
          }).catch((e)=>{
              this.spinner.hide()
              this.toaster.error(e.error.message)
              console.log(e)
          })
      }
      send_message(){
          this.spinner.show()
          var user_id = this.route.snapshot.params['user_id']
          var data = {
              message:this.message,
              accountHolder:user_id
          }
          this.ccService.duePaymentReminder(data).toPromise().then((data:any)=>{
              console.log(data)
              this.spinner.hide()
              this.toaster.success(data.message)
          }).catch((e)=>{
              this.toaster.error(e.error.message)
              console.log(e)
          })
      }
      user_coopratives(){
        var user_id = this.route.snapshot.params['user_id']
        this.ccService.fetch_all_user_coopratives(user_id).toPromise().then((data:any)=>{
          this.coop = data.result.coops
          console.log(data.result)
        }).catch((e)=>{
          console.log(e)
        })
      }
      fetch_all_user_women_group(){
        var user_id = this.route.snapshot.params['user_id']
        this.ccService.fetch_all_user_women_group(user_id).toPromise().then((data:any)=>{
          this.w = data.result.coop
          console.log(data)
        }).catch((e)=>{
          console.log(e)
        })
      }
      disable(){
          var id = this.route.snapshot.params['user_id']
          var data = {
              enabled:false
          }
          this.userService.disable(id,data).toPromise().then((data:any)=>{
              console.log(data)
          }).catch((e)=>{
              console.log(e)
          })
      }
      enable(){
          var id = this.route.snapshot.params['user_id']
          var data = {
              enabled:true
          }
          this.userService.disable(id,data).toPromise().then((data:any)=>{
              console.log(data)
          }).catch((e)=>{
              console.log(e)
          })
      }
      fetch_all_user_loan(){
        var id = this.route.snapshot.params['user_id']
        this.ccService.fetch_all_user_loan(id).toPromise().then((data:any)=>{
          for(var i = 0; i < data.result.loans.length; i++){
            if(data.result.loans[i].disbursed){
              this.taken.push(data.result.loans[i])
              // console.log(this.taken)
            }
            if(data.result.loans[i].disbursed && !data.result.loans[i].repaid){
              this.owed += Number(data.result.loans[i].amount)
            }
          }
          console.log(data)
        }).catch((e)=>{
          console.log(e)
        })
      }
}
