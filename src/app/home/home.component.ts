import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
    //   debugger;
      var role = localStorage.getItem('role')
      if(role == '1'){
          this.router.navigate(['/admin/dashboard'])
      }else if(role =='2'){
          this.router.navigate(['/approval-manager/dashboard'])
      }else if(role =='3'){
          this.router.navigate(['/disburse-manager/dashboard'])
      }else if(role == '4'){
          this.router.navigate(['/customer-care-agent/dashboard'])
      }else if(role == '5'){
          this.router.navigate(['/u/dashboard'])
      }
  }

}
