import { ToasterService } from './../shared/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { CcService } from './../shared/cc.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-my-community',
  templateUrl: './view-my-community.component.html',
  styleUrls: ['./view-my-community.component.css'],
})
export class ViewMyCommunityComponent implements OnInit {

  constructor(private api: CcService, private route: ActivatedRoute,
    private spinner: NgxSpinnerService, private toaster: ToasterService, private router: Router) { }
  community = {
     _id: "" ,
     name:'',
     contribution:'',
     amount:'',
     admin:'',
     members:[]
   };
  ngOnInit() {
    this.getDetails();
  }
  isAdmin(members, id) {
    return members.find(m => m.id == id && m.type == "admin");
  }
  applyLoan() {
    this.router.navigate(["cooperative/request-loan/" + this.community._id]);
  }
  viewLoans() {
    this.router.navigate(["coopratives/loans/" + this.community._id + "/" + localStorage.getItem("user_id")+"/0"]);
  }
  getDetails() {
    this.spinner.show();
    var id = this.route.snapshot.params['id'];
    this.api.fetchCommunityDetails(id).toPromise().then((data:any) => {
      this.community = data.result;
      this.spinner.hide();
    })
      .catch(err => {
        this.toaster.error("Community details unavailable");
        this.spinner.hide();
      })

  }
}
