import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMyCommunityComponent } from './view-my-community.component';

describe('ViewMyCommunityComponent', () => {
  let component: ViewMyCommunityComponent;
  let fixture: ComponentFixture<ViewMyCommunityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMyCommunityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMyCommunityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
