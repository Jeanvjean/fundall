import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingLoanComponentComponent } from './pending-loan-component.component';

describe('PendingLoanComponentComponent', () => {
  let component: PendingLoanComponentComponent;
  let fixture: ComponentFixture<PendingLoanComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingLoanComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingLoanComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
