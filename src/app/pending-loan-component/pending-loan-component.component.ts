import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'pending-loan-component',
  templateUrl: './pending-loan-component.component.html',
  styleUrls: ['./pending-loan-component.component.css']
})
export class PendingLoanComponentComponent implements OnInit {
  @Input() loan = {
    dateApplied:'',
    amount:'',
    monthlyRepayment:'',
    duration:'',
    status:''
  };
  constructor() { }

  ngOnInit() {
  }

}
