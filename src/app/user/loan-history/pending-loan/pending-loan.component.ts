import { PendingLoanCardComponent } from './../../../shared/pending-loan-card/pending-loan-card.component';
import { Component, OnInit } from '@angular/core';
import { LoanNavComponent } from '../loan-nav/loan-nav.component'
import { LoanService } from '../../../shared/loan.service';
@Component({
  selector: 'app-pending-loan',
  templateUrl: './pending-loan.component.html',
  styleUrls: ['./pending-loan.component.css']
})
export class PendingLoanComponent implements OnInit {
    loans = [];
  constructor(private loanService:LoanService) { }

  ngOnInit() {
      this.pendingLoan();
  }
  pendingLoan(){
      var user_id = localStorage.getItem('user_id')
      this.loanService.getLoanHistory(user_id).toPromise().then((data:any)=>{
          this.loans = data.result.history;
          this.loans = this.loans = this.loans.filter(loan=>{
            if(loan.applicationState == 0){
              return true;
            }
          })
      }).catch((e)=>{
          console.log(e)
      })
  }
}
