import { RejectedLoanCardComponent } from './../../../shared/rejected-loan-card/rejected-loan-card.component';
import { Component, OnInit } from '@angular/core';
import { LoanNavComponent } from '../loan-nav/loan-nav.component';
import { LoanService } from '../../../shared/loan.service';

@Component({
  selector: 'app-rejected-loan',
  templateUrl: './rejected-loan.component.html',
  styleUrls: ['./rejected-loan.component.css']
})

export class RejectedLoanComponent implements OnInit {
    loans = [];
  constructor(private loanService:LoanService) { }

  ngOnInit() {
      this.rejectedLoan()
  }
  rejectedLoan(){
      var user_id = localStorage.getItem('user_id')
      this.loanService.getLoanHistory(user_id).toPromise().then((data:any)=>{
           this.loans = data.result.history;
          this.loans = this.loans = this.loans.filter(loan=>{
            if(loan.applicationState == 1){
              return true;
            }
          })
      }).catch((e)=>{
          console.log(e)
      })
  }
}
