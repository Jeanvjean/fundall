import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DueLoanComponent } from './due-loan.component';

describe('DueLoanComponent', () => {
  let component: DueLoanComponent;
  let fixture: ComponentFixture<DueLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DueLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DueLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
