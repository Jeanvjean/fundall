import { Component, OnInit } from '@angular/core';
import { LoanService } from '../../../shared/loan.service';

@Component({
  selector: 'app-due-loan',
  templateUrl: './due-loan.component.html',
  styleUrls: ['./due-loan.component.css']
})
export class DueLoanComponent implements OnInit {
 loans = [];
  constructor(private loanService:LoanService) { }

  ngOnInit() {
      this.dueLoan()
  }
    dueLoan(){
        var user_id = localStorage.getItem('user_id')
        this.loanService.getDueLoans().toPromise().then((data:any)=>{
            this.loans = data.result;
        }).catch((e)=>{
            console.log(e)
        })
    }
}
