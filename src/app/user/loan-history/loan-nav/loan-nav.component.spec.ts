import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanNavComponent } from './loan-nav.component';

describe('LoanNavComponent', () => {
  let component: LoanNavComponent;
  let fixture: ComponentFixture<LoanNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
