import { Component, OnInit } from '@angular/core';
import { LoanNavComponent } from '../loan-nav/loan-nav.component'
import { LoanService } from '../../../shared/loan.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
 totalLoans:number;
  repaidLoans:number;
  activeLoans:number;
  totalAmountOwned:number = 0;
  loans = [];
  status = {0:"Pending",1:"Rejected",2:"Approved"};
  constructor(private loanService:LoanService) { }

  ngOnInit() {
      this.setHistory();
  }
  dueLoan(){
      var user_id = localStorage.getItem('user_id')
      this.loanService.getLoanHistory(user_id).toPromise().then((data:any)=>{
          console.log(data)
      }).catch((e)=>{
          console.log(e)
      })
  }
   setHistory(){
      var user_id = localStorage.getItem('user_id');
      this.loanService.getLoanHistory(user_id).toPromise().then((data:any)=>{
           this.loans = data.result.history;
           this.totalLoans = this.loans.length;
           this.repaidLoans = this.loans.filter(loan => loan.repaid).length;
           this.activeLoans = this.loans.filter(loan => loan.applicationState == 2).length;
           this.loans.forEach(loan=>{
             if(loan.total_cost){
             this.totalAmountOwned = this.totalAmountOwned + loan.total_cost;
             }
           })
       
      }).catch((e)=>{
          console.log(e)
      })
  }
}
