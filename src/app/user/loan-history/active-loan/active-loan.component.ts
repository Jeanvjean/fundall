import { SpinnerComponent } from './../../../shared/my-spinner';
import { LoanCardComponent } from './../../../shared/loan-card/loan-card.component';
import { Component, OnInit } from '@angular/core';
import { LoanNavComponent } from '../loan-nav/loan-nav.component';
import { LoanService } from '../../../shared/loan.service';

@Component({
  selector: 'app-active-loan',
  templateUrl: './active-loan.component.html',
  styleUrls: ['./active-loan.component.css']
})
export class ActiveLoanComponent implements OnInit {
  loans = [];
  isLoading = false;
  constructor(private loanService:LoanService) { }

  ngOnInit() {
      this.activeLoan()
  }

  activeLoan(){
      var user_id = localStorage.getItem('user_id');
      this.isLoading = true;
      this.loanService.getLoanHistory(user_id).toPromise().then((data:any)=>{
          this.loans = data.result.history;
          this.loans = this.loans.filter(loan=>{
            if(loan.applicationState == 2 && loan.disbursed){
              return true;
            }
          })
          this.isLoading = false;
      }).catch((e)=>{
        this.isLoading = false;
        //   console.log(e)
      })
  }
}
