import { Component, OnInit } from '@angular/core';
import { UserNavComponent } from './user-nav/user-nav.component';
import { UserService } from  '../shared/user.service';
import { LoanService } from '../shared/loan.service';
import { Router } from '@angular/router';
import { ToasterService } from '../shared/toaster.service';
import * as GLOBAL from  '../shared/api.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
    loans = [];
    baseApi = GLOBAL.BASE_API;
    updated:boolean = false;
  constructor(
      private loanService:LoanService,
      private router:Router,
      private userService:UserService,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.updated = undefined;
      var role = localStorage.getItem('role')
      if(role == '1'){
          this.router.navigate(['/admin/dashboard'])
      }else if(role =='2'){
          this.router.navigate(['/approval-manager/dashboard'])
      }else if(role =='3'){
          this.router.navigate(['/disburse-manager/dashboard'])
      }else if(role == '4'){
          this.router.navigate(['/customer-care-agent/dashboard'])
      }else if(role == '5'){
        //   this.router.navigate(['/u/dashboard'])
      }
      this.getLoan()
      this.user_info()
  }
  getLoan(){
      this.spinner.show();
      this.loanService.getLoan().toPromise().then((data:any)=>{
        //   console.log(data)
          this.loans = data.result
          // this.updated = localStorage.getItem('updated')
          this.spinner.hide()
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  user_info(){
      this.spinner.show();
      var user_id = localStorage.getItem('user_id')
      this.userService.userDetails(user_id).toPromise().then((data:any)=>{
          this.updated = data.result.profile_status
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
      })
  }
}
