import {BASE_API} from './../../shared/api.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserService } from  '../../shared/user.service';
import { ToasterService } from '../../shared/toaster.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { en_US, zh_CN, NzI18nService } from 'ng-zorro-antd';

@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
    photo: File = null;
    id_card: File = null;
    dPhoto = "/assets/fundal/home/icon/Profile (3) (1).svg"
    updated = false;
    profile = {
        firstName: '',
        lastName: '',
        email: '',
        username: '',
        gender: '',
        phone: '',
        bvn: '',
        dob:'',
        yy: '',
        mm: '',
        dd: '',
        position: '',
        company: '',
        employment: '',
        form_of_Id: '',
        identificationImage: '',
        image: ''
    }
    constructor(
        private userService: UserService,
        private router: Router,
        private toaster: ToasterService,
        private spinner: Ng4LoadingSpinnerService,
          private i18n: NzI18nService) { }
    selectFile(event) {
        // this.profilePic = <File>event.target.files[0]
        var files = event.target.files
        var filename = files.name

        const fileReader = new FileReader()
        fileReader.addEventListener('load', () => {
            this.dPhoto = fileReader.result
            this.profile.image = this.dPhoto;
        })
        fileReader.readAsDataURL(files[0])
        this.photo = files[0]
    }
    idUpload(event) {
        this.id_card = <File>event.target.files[0]
        // var files = event.target.files
        // var filename = files.name
        //
        // const fileReader = new FileReader()
        // fileReader.addEventListener('load',()=>{
        //     this.dPhoto = fileReader.result
        // })
        // fileReader.readAsDataURL(files[0])
        // this.id_card = files[0]
    }
    ngOnInit() {
        this.userProfile()
    }
    update() {
        this.spinner.show()
        var user_id = localStorage.getItem('user_id')
        var fd = new FormData()
        fd.append('firstName', this.profile.firstName)
        fd.append('lastName', this.profile.lastName)
        fd.append('email', this.profile.email)
        fd.append('phone', this.profile.phone)
        fd.append('username', this.profile.username)
        fd.append('gender', this.profile.gender)
        fd.append('position', this.profile.position)
        fd.append('form_of_Id', this.profile.form_of_Id)
        fd.append('bvn', this.profile.bvn)
        fd.append('card_expiry', this.profile.dd + '-' + this.profile.mm + '-' + this.profile.yy)
        if (this.photo != null) {
            fd.append('photo', this.photo, this.photo.name)
        }
        if (this.id_card != null) {
            fd.append('proof_of_id_upload', this.id_card, this.id_card.name)
        }
        this.userService.updateUser(user_id, fd).toPromise().then((data: any) => {
            console.log(data)
            this.toaster.success(data.message)
            this.profile = data.result
            // localStorage.setItem('updated', data.result.user.profile_status)
            this.spinner.hide()
            // this.router.navigate(['/u/profile'])
        }).catch((e) => {
            this.spinner.hide()
            this.toaster.error(e.error.message)
            console.log(e)
        })
    }
    userProfile() {
        this.spinner.show()
        var user_id = localStorage.getItem('user_id')
        this.userService.userDetails(user_id).toPromise().then((data: any) => {
            console.log(data)
            this.spinner.hide()
            this.toaster.success(data.message)
            this.profile = data.result
            this.profile.identificationImage = BASE_API + this.profile.identificationImage
            this.profile.image = BASE_API + this.profile.image
            this.updated = data.result.profile_status
        }).catch((e) => {
            this.spinner.hide()
            this.toaster.error(e.error.message)
        })
    }
}
