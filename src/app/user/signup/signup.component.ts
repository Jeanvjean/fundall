import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserService } from  '../../shared/user.service';
import { ToasterService } from '../../shared/toaster.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
    message: any = '';
    user = {
        firstname: '',
        lastname: '',
        phone: '',
        email: '',
        bvn: '',
        security_question: '',
        security_answer: '',
        password: '',
        cPassword: '',
        username: '',
        verifyBy: ''
    };
    errorMsg = "";
    constructor(
        private router: Router,
        private userService: UserService,
        private toaster: ToasterService,
        private spinner: Ng4LoadingSpinnerService) { }

    ngOnInit() {
        var token = localStorage.getItem('token')
        if (token) {
            this.router.navigate([''])
        }
    }
    signup() {
        if (this.user.password != this.user.cPassword) {
            this.spinner.hide()
            return this.errorMsg = 'passwords do not match'
        }
        var user = {
            firstName: this.user.firstname,
            lastName: this.user.lastname,
            phone: this.user.phone,
            email: this.user.email,
            bvn: this.user.bvn,
            verifyBy: this.user.verifyBy,
            security_answer: this.user.security_answer,
            security_question: this.user.security_question,
            // username: this.user.username,
            password: this.user.password
        }
        var error = false;
        for (var i in user) {
            if (!user[i]) {
                var field = i.replace("-"," ");
                field = field.toUpperCase();
                this.toaster.warning(`${field} IS REQUIRED`);
                error = true;
                return;
            }
        }
        if (!error) {
            this.spinner.show()
            this.userService.signUp(user).toPromise().then((data: any) => {
                this.spinner.hide()
                console.log(data)
                if (data.success == true) {
                    localStorage.setItem('user_email', data.result.email)
                    this.toaster.success(data.message)
                    this.router.navigate(['/u/confirmation-email'])
                } else if (data.success == false) {
                    return this.toaster.error(data.message)
                }
            }).catch((e) => {
                this.spinner.hide()
                this.toaster.error("An unknown error occurred, try again")
                console.log(e)
            })
        }

    }
}
