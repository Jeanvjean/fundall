import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from  '../../shared/user.service';
import * as GLOBAL from  '../../shared/api.service';
import { ToasterService } from '../../shared/toaster.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    profileImage = '/assets/fundal/home/icon/Profile (3) (1).svg';
    updated:boolean = false;
    profile = {
        firstName:'',
        lastName:'',
        email:'',
        username:'',
        gender:'',
        phone:'',
        card_expiry:'',
        position:'',
        company:'',
        bvn:'',
        employment:'',
        form_of_Id:'',
        identificationImage:'',
        image:''
    }
  constructor(
      private userService: UserService,
      private router:Router,
      private toaster:ToasterService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.userProfile();
      console.log(GLOBAL);
  }
  userProfile(){
      this.spinner.show()
      var user_id = localStorage.getItem('user_id')
      this.userService.userDetails(user_id).toPromise().then((data:any)=>{
          this.spinner.hide()
        //   console.log(ApiService)
        //  this.profile.
          this.profile = data.result;
          this.profile.identificationImage =  GLOBAL.BASE_API+this.profile.identificationImage
          this.profile.image =  GLOBAL.BASE_API +this.profile.image
          this.updated = data.result.profile_status
          this.toaster.success(data.message)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
      })
  }
}
