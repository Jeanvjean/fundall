import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { LoanService } from  '../../shared/loan.service';
import { ToasterService } from '../../shared/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-request-loan',
    templateUrl: './request-loan.component.html',
    styleUrls: ['auto-loan/auto-loan.component.css']
})
export class RequestLoanComponent implements OnInit {
    bank_statement: File = null;
    proof_of_id_upload: File = null;
    proof_upload: File = null;
    cac_doc: File = null;
    updated: boolean = false;
    isRequesting: boolean = false;
    loan: any = {
        amount: "",
        duration: 1,
        salaryEarned: '',
        proof_employment: '',
        proof_of_id: '',
        total_cost: 0,
        monthly_repayments: 0.00,
        interestRate: 0.00,
        productName: '',
        job_type: '',
        company: '',
        company_description: ''
    }
    calc(amounts) {
        if (amounts) {
            this.formatToNaira(amounts);
        }
        let interest = parseFloat(this.loan.interestRate);
        let amount = parseFloat(this.loan.amount)
        this.loan.total_cost = parseFloat(this.loan.amount) + (interest / 100) * (amount);
        this.loan.monthly_repayments = this.loan.duration ? this.loan.total_cost / this.loan.duration : 0;
        // debugger;
    }
    constructor(
        private loanService: LoanService,
        private router: Router,
        private route: ActivatedRoute,
        private toaster: ToasterService,
        private spinner: NgxSpinnerService) { }
    onselect_bs(event) {
        this.bank_statement = <File>event.target.files[0]
    }
    on_change_files(event, i) {
        this.loan.requirements[i].file = <File>event.target.files[0];
    }
    select_Id(event) {
        this.proof_of_id_upload = <File>event.target.files[0]
    }
    select_empl_Id(event) {
        this.proof_of_id_upload = <File>event.target.files[0]
    }
    select_cacdoc(event) {
        this.proof_of_id_upload = <File>event.target.files[0]
    }
    ngOnInit() {
        this.loan.loanName = this.route.snapshot.params['Name'];
        this.loan.Id = this.route.snapshot.params['Id'];
        this.loan.interestRate = this.route.snapshot.params['interest'];
        this.loanService.loanDetail(this.loan.Id).toPromise().then((data: any) => {
            this.spinner.hide()
            //   this.toaster.success(data.message)
            this.loan = data;
            this.loan.amount = 0;
        }).catch((e) => {
            this.spinner.hide()
            this.toaster.error(e.error.message)
            //   console.log(e)
        })

    }
    getOptions(r) {
        return r.split(",");
    }
    formatToNaira(obj) {
        // this.loan.amount = "N"+this.loan.amount;
    }
    request() {
        this.isRequesting = true;
        var fd = new FormData()
        fd.append('amount', this.loan.amount)
        fd.append('duration', this.loan.duration)
        fd.append('salaryEarned', this.loan.salaryEarned)
        fd.append('productName', this.loan.loanName)
        fd.append('Id', this.route.snapshot.params['Id'])
        for (let i = 0; i < this.loan.requirements.length; i++) {
            fd.append('requirements_' + i, this.loan.requirements[i].type == 'file' ? this.loan.requirements[i].file : this.loan.requirements[i].value);
        }
    
        this.loanService.apply(fd).toPromise().then((data: any) => {
            this.spinner.hide()
            this.isRequesting = false;
            if (data.success) {
                this.toaster.success(data.message);
                this.router.navigate(["u/pending-loans"]);
            }
            else {
                this.toaster.error(data.message);
            }
        }).catch((e) => {
            this.spinner.hide()
            this.isRequesting = false;
            this.toaster.error(e.error.message)
            console.log(e)
        })
    }
}
