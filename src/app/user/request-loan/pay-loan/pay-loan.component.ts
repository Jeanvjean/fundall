import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { LoanService } from  '../../../shared/loan.service';
import { ToasterService } from '../../../shared/toaster.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-pay-loan',
  templateUrl: './pay-loan.component.html',
  styleUrls: ['../auto-loan/auto-loan.component.css']
})
export class PayLoanComponent implements OnInit {
    bank_statement:File = null;
    proof_of_id_upload: File = null;
    proof_upload:File = null;
    cac_doc:File = null;
    updated:boolean = false;
    isRequesting:boolean = false;
    pay:any={
        amount:'',
        duration:'',
        salaryEarned:'',
        proof_employment:'',
        proof_of_id:'',
        total_cost:'',
        monthly_repayments:'',
        interestRate:'0.05',
        productName:'Pay Loan'
    }
    calc(){
        this.pay.total_cost = +this.pay.amount + +( this.pay.interestRate * this.pay.amount);
        this.pay.monthly_repayments = this.pay.total_cost / this.pay.duration;
    }
  constructor(
      private loanService: LoanService,
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private spinner:NgxSpinnerService) { }
      onselect_bs(event){
          this.bank_statement = <File>event.target.files[0]
      }
      select_Id(event){
          this.proof_of_id_upload = <File>event.target.files[0]
      }
      select_empl_Id(event){
          this.proof_of_id_upload = <File>event.target.files[0]
      }
      select_cacdoc(event){
          this.proof_of_id_upload = <File>event.target.files[0]
      }
  ngOnInit() {
  }
  request(){
      this.spinner.show();
      this.isRequesting = true;
      var fd = new FormData()
      fd.append('amount',this.pay.amount)
      fd.append('duration',this.pay.duration)
      fd.append('salaryEarned',this.pay.salaryEarned)
      fd.append('proof_employment',this.pay.proof_employment)
      fd.append('total_cost',this.pay.total_cost)
      fd.append('monthly_repayments',this.pay.monthly_repayments)
      fd.append('interestRate',this.pay.interestRate)
      fd.append('productName',this.pay.productName)
      if(this.bank_statement != null){
          fd.append('bank_statement',this.bank_statement.name)
      }
      if(this.proof_upload != null){
          fd.append('proof_upload',this.proof_upload.name)
      }
      this.loanService.auto(fd).toPromise().then((data:any)=>{
          console.log(data)
          this.spinner.hide()
          this.isRequesting = false;
          this.toaster.success(data.message)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
