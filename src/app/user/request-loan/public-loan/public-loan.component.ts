import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { LoanService } from  '../../../shared/loan.service';
import { ToasterService } from '../../../shared/toaster.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'loan-form',
  templateUrl: './public-loan.component.html',
  styleUrls: ['../auto-loan/auto-loan.component.css']
})
export class PublicLoanComponent implements OnInit {
    bank_statement:File = null;
    proof_of_id_upload: File = null;
    proof_upload:File = null;
    cac_doc:File = null;
    updated:boolean = false;
    isRequesting = false;
    public:any={
        amount:'',
        duration:'',
        salaryEarned:'',
        proof_employment:'',
        proof_of_id:'',
        total_cost:'',
        monthly_repayments:'',
        interestRate:'0.05',
        productName:'Public Loan'
    }
    calc(){
        this.public.total_cost = +this.public.amount + +( this.public.interestRate * this.public.amount);
        this.public.monthly_repayments = this.public.total_cost / this.public.duration;
    }
  constructor(
      private loanService: LoanService,
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private spinner:NgxSpinnerService) { }
  onselect_bs(event){
      this.bank_statement = <File>event.target.files[0]
  }
  select_Id(event){
      this.proof_of_id_upload = <File>event.target.files[0]
  }
  select_empl_Id(event){
      this.proof_of_id_upload = <File>event.target.files[0]
  }
  select_cacdoc(event){
      this.proof_of_id_upload = <File>event.target.files[0]
  }
  ngOnInit() {
  }
  request(){
      this.isRequesting = true;
      this.spinner.show();
      var fd = new FormData()
      fd.append('amount',this.public.amount)
      fd.append('duration',this.public.duration)
      fd.append('salaryEarned',this.public.salaryEarned)
      fd.append('proof_employment',this.public.proof_employment)
      fd.append('total_cost',this.public.total_cost)
      fd.append('monthly_repayments',this.public.monthly_repayments)
      fd.append('interestRate',this.public.interestRate)
      fd.append('productName',this.public.productName)
      if(this.bank_statement != null){
          fd.append('bank_statement',this.bank_statement)
      }
      if(this.proof_of_id_upload != null){
          fd.append('proof_upload',this.proof_of_id_upload)
      }
      this.loanService.auto(fd).toPromise().then((data:any)=>{
          console.log(data)
          this.spinner.hide()
          this.toaster.success(data.message);
          this.isRequesting = false;
      }).catch((e)=>{
          this.isRequesting = false;
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
