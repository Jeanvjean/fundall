import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicLoanComponent } from './public-loan.component';

describe('PublicLoanComponent', () => {
  let component: PublicLoanComponent;
  let fixture: ComponentFixture<PublicLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
