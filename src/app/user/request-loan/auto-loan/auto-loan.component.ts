import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { LoanService } from  '../../../shared/loan.service';
import { ToasterService } from '../../../shared/toaster.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-auto-loan',
  templateUrl: './auto-loan.component.html',
  styleUrls: ['./auto-loan.component.css']
})
export class AutoLoanComponent implements OnInit {
    bank_statement:File = null;
    proof_of_id_upload: File = null;
    proof_upload:File = null;
    cac_doc:File = null;
    updated:boolean = false;
    auto:any={
        amount:'',
        duration:'',
        salaryEarned:'',
        proof_employment:'',
        proof_of_id:'',
        total_cost:'',
        monthly_repayments:'',
        interestRate:'0.05',
        productName:'Public Loan'
    }
    calc(){
        this.auto.total_cost = +this.auto.amount + +( this.auto.interestRate * this.auto.amount);
        this.auto.monthly_repayments = this.auto.total_cost / this.auto.duration;
    }
  constructor(
      private loanService: LoanService,
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private spinner:NgxSpinnerService) { }
      onselect_bs(event){
          this.bank_statement = <File>event.target.files[0]
      }
      select_Id(event){
          this.proof_of_id_upload = <File>event.target.files[0]
      }
      select_empl_Id(event){
          this.proof_of_id_upload = <File>event.target.files[0]
      }
      select_cacdoc(event){
          this.proof_of_id_upload = <File>event.target.files[0]
      }
  ngOnInit() {
  }
  request(){
      this.spinner.show();
      var fd = new FormData()
      fd.append('amount',this.auto.amount)
      fd.append('duration',this.auto.duration)
      fd.append('salaryEarned',this.auto.salaryEarned)
      fd.append('proof_employment',this.auto.proof_employment)
      fd.append('total_cost',this.auto.total_cost)
      fd.append('monthly_repayments',this.auto.monthly_repayments)
      fd.append('interestRate',this.auto.interestRate)
      fd.append('productName',this.auto.productName)
      if(this.bank_statement != null){
          fd.append('bank_statement',this.bank_statement)
      }
      if(this.proof_upload != null){
          fd.append('proof_upload',this.proof_upload)
      }
      this.loanService.auto(fd).toPromise().then((data:any)=>{
          console.log(data)
          this.spinner.hide()
          this.toaster.success(data.message)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
