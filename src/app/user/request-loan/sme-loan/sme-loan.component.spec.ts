import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmeLoanComponent } from './sme-loan.component';

describe('SmeLoanComponent', () => {
  let component: SmeLoanComponent;
  let fixture: ComponentFixture<SmeLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmeLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmeLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
