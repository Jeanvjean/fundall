import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { LoanService } from  '../../../shared/loan.service';
import { ToasterService } from '../../../shared/toaster.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-sme-loan',
  templateUrl: './sme-loan.component.html',
  styleUrls: ['../auto-loan/auto-loan.component.css']
})
export class SmeLoanComponent implements OnInit {
    bank_statement:File = null;
    proof_of_id_upload: File = null;
    proof_upload:File = null;
    cac_doc:File = null;
    updated:boolean = false;
    isRequesting:boolean = false;
    sme:any={
        amount:'',
        duration:'',
        salaryEarned:'',
        proof_employment:'',
        proof_of_id:'',
        monthly_repayments:'',
        company:'',
        companyRegisterationNumber:'',
        productName:'SME Loan',
        businessName:'0.05',
        total_cost:'',
        interestRate:''
    }
    calc(){
        this.sme.total_cost = +this.sme.amount + +( this.sme.interestRate * this.sme.amount);
        this.sme.monthly_repayments = this.sme.total_cost / this.sme.duration;
    }
  constructor(
      private loanService: LoanService,
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private spinner:NgxSpinnerService) { }
      onselect_bs(event){
          this.bank_statement = <File>event.target.files[0]
      }
      select_Id(event){
          this.proof_of_id_upload = <File>event.target.files[0]
      }
      select_empl_Id(event){
          this.proof_of_id_upload = <File>event.target.files[0]
      }
      select_cacdoc(event){
          this.proof_of_id_upload = <File>event.target.files[0]
      }
  ngOnInit() {
  }
  request(){
      var fd = new FormData();
      this.isRequesting = true;
      fd.append('amount',this.sme.amount)
      fd.append('duration',this.sme.duration)
      fd.append('salaryEarned',this.sme.salaryEarned)
      fd.append('proof_of_id',this.sme.proof_of_id)
      fd.append('proof_employment',this.sme.proof_employment)
      fd.append('monthly_repayments',this.sme.monthly_repayments)
      fd.append('companyName',this.sme.company)
      fd.append('companyRegisterationNumber',this.sme.companyRegisterationNumber)
      fd.append('productName',this.sme.productName)
      fd.append('total_cost',this.sme.total_cost)
      fd.append('interestRate',this.sme.interestRate)
      if(this.bank_statement != null){
          fd.append('bank_statement',this.bank_statement)
      }
      if(this.cac_doc != null){
          fd.append('cac_doc',this.cac_doc)
      }
      this.loanService.auto(fd).toPromise().then((data:any)=>{
        //   console.log(data)
        this.isRequesting = true
          this.spinner.hide()
          this.toaster.success(data.message)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
