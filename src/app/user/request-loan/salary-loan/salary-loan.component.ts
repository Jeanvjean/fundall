import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { LoanService } from  '../../../shared/loan.service';
import { ToasterService } from '../../../shared/toaster.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-salary-loan',
  templateUrl: './salary-loan.component.html',
  styleUrls: ['./salary-loan.component.css']
})
export class SalaryLoanComponent implements OnInit {
    bank_statement:File = null;
    proof_of_id_upload: File = null;
    proof_upload:File = null;
    cac_doc:File = null;
    updated:boolean = false;
    salary:any={
        amount:'',
        duration:'',
        salaryEarned:'',
        proof_employment:'',
        proof_of_id:'',
        total_cost:'',
        monthly_repayments:'',
        interestRate:'0.05',
        productName:'Pay Loan'
    }
    calc(){
        this.salary.total_cost = +this.salary.amount + +( this.salary.interestRate * this.salary.amount);
        this.salary.monthly_repayments = this.salary.total_cost / this.salary.duration;
    }
  constructor(
      private loanService: LoanService,
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private spinner:NgxSpinnerService) { }
      onselect_bs(event){
          this.bank_statement = <File>event.target.files[0]
      }
      select_Id(event){
          this.proof_of_id_upload = <File>event.target.files[0]
      }
      select_empl_Id(event){
          this.proof_of_id_upload = <File>event.target.files[0]
      }
      select_cacdoc(event){
          this.proof_of_id_upload = <File>event.target.files[0]
      }
  ngOnInit() {
  }
  request(){
      var fd = new FormData()
      fd.append('amount',this.salary.amount)
      fd.append('duration',this.salary.duration)
      fd.append('salaryEarned',this.salary.salaryEarned)
      fd.append('proof_employment',this.salary.proof_employment)
      fd.append('total_cost',this.salary.total_cost)
      fd.append('monthly_repayments',this.salary.monthly_repayments)
      fd.append('interestRate',this.salary.interestRate)
      fd.append('productName',this.salary.productName)
      if(this.bank_statement != null){
          fd.append('bank_statement',this.bank_statement)
      }
      if(this.proof_upload != null){
          fd.append('proof_upload',this.proof_upload)
      }
      this.loanService.auto(fd).toPromise().then((data:any)=>{
          console.log(data)
          this.spinner.hide()
          this.toaster.success(data.message)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
