import { Component, OnInit } from '@angular/core';
import { WalletService } from '../../../shared/wallet.service';
import { Router,ActivatedRoute } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToasterService } from '../../../shared/toaster.service';

@Component({
  selector: 'app-add-account',
  templateUrl: './add-account.component.html',
  styleUrls: ['./add-account.component.css']
})
export class AddAccountComponent implements OnInit {
        account = {
                code:'',
                account_number:'',
                email:'',
                phone:'',
                birthday:''
        };
  constructor(
      private walletService:WalletService,
      private route:ActivatedRoute,
      private router:Router,
      private toaster:ToasterService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
  }
  // addAccount(){
  //     this.spinner.show()
  //     var data = this.account
  //     this.walletService.addAccount(data).toPromise().then((data)=>{
  //         this.toaster.success(data.message)
  //         this.spinner.hide()
  //         console.log(data)
  //     }).catch((e)=>{
  //         this.spinner.hide()
  //         console.log(e)
  //     })
  // }
}
