import { Component, OnInit } from '@angular/core';
import { WalletService } from '../../../shared/wallet.service';
import { Router,ActivatedRoute } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToasterService } from '../../../shared/toaster.service';

@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.css']
})
export class AddCardComponent implements OnInit {
    card = {
        cvv:'',
        expiry_month:'',
        expiry_year:'',
        number:'',
        type:'',
        email:'',
        phone:'',
        pin:'',
        birthday:''
    }
  constructor(
      private walletService:WalletService,
      private route:ActivatedRoute,
      private router:Router,
      private toaster:ToasterService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
  }
  // addCard(){
  //     var data=this.card
  //     this.walletService.addCard(data).toPromise().then((data:any)=>{
  //         console.log(data)
  //     }).catch((e)=>{
  //         console.log(e)
  //     })
  // }
}
