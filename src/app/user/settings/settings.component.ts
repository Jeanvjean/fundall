import { ToasterService } from './../../shared/toaster.service';
import { UserService } from './../../shared/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  isLoading:boolean = false;
  user = {
    firstName:'',
    lastName:'',
    phone:'',
    currentEmail:'',
    newEmail:'',
    currentPassword:'',
    newPassword:'',
    user:''
  };
  constructor(private userApi: UserService,private toaster: ToasterService) { }

  ngOnInit() {
    this.getUser();

  }

  getUser() {

    this.userApi.userDetails(localStorage.getItem("user_id")).toPromise().then((data: any) => {
      this.user.firstName = data.result.firstName;
      this.user.lastName = data.result.lastName;
      this.user.phone = data.result.phone;
      this.user.user = data.result.accountId;
    }).catch((err: any) => {

    })
  }

  updateProfile() {
    this.isLoading = true;
    this.userApi.updateUserDetails(this.user).toPromise().then((data: any) => {

      if(data.success){
        this.toaster.success(data.message)
      }
      else{
        this.toaster.error(data.message);
      }
      this.isLoading = false;
    }).catch((err: any) => {
      this.isLoading = false;
      this.toaster.error("An unknown error occured, please try again");
    })
  }

}
