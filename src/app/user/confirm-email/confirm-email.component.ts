import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserService } from  '../../shared/user.service';
import { ToasterService } from '../../shared/toaster.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.css']
})
export class ConfirmEmailComponent implements OnInit {
    code='';
  constructor(
      private router: Router,
      private userService:UserService,
      private toaster:ToasterService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
  }
  resend(){
      this.spinner.show()
      var email = {
          email:localStorage.getItem('user_email')
      }
      this.userService.resendCode(email).toPromise().then((data:any)=>{
          console.log(data)
          this.toaster.success(data.message)
          this.spinner.hide()
      }).catch((e)=>{
          console.log(e)
              this.spinner.hide()
              this.toaster.error(e.error.message)
      })
  }
}
