import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from  '../../shared/user.service';
import { ToasterService } from '../../shared/toaster.service'
// import { NgxSpinnerService } from 'ngx-spinner';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    user = {
            email:'',
            password:''
    };
    hasLoggedIn = true;
  constructor(
      private userService: UserService,
      private router:Router,
      private toaster:ToasterService,
      private spinner:Ng4LoadingSpinnerService,
      private route: ActivatedRoute) { }

  ngOnInit() {
      var token = localStorage.getItem('token')
      if(token){
          this.router.navigate([''])
      }
  }

  login(){
      if(!this.user.email){
          this.toaster.warning("Please enter your email")
          return
      }
      if(!this.user.password){
          this.toaster.warning("Please enter your password")
          return;

      }
      this.spinner.show()
      var user = this.user
      this.userService.login(user).toPromise().then((data:any)=>{
                this.hasLoggedIn = false;
                if(data.success){
                    //   window.location.reload();
                    localStorage.setItem('token',data.result.token)
                    localStorage.setItem('user_id',data.result.user._id)
                    localStorage.setItem('role',data.result.user.role)
                    localStorage.setItem('updated',data.result.user.profile_status)
                    this.router.navigate(['u/dashboard']);
                    window.location.reload();
                    //   windows
                    this.toaster.success(data.message)
                }else{
                    this.toaster.error(data.message)
                }
              this.spinner.hide()
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
