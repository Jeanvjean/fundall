import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { ComplainService } from  '../../shared/complain.service';
import { ToasterService } from '../../shared/toaster.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-user-complain',
  templateUrl: './user-complain.component.html',
  styleUrls: ['./user-complain.component.css']
})
export class UserComplainComponent implements OnInit {
    complaint = ''
  constructor(
      private router: Router,
      private complainService:ComplainService,
      private toaster:ToasterService,
      private spinner:NgxSpinnerService) { }

  ngOnInit() {
  }
  makeComplain(){
      this.spinner.show()
      var data = {
          complaint:this.complaint
      }
      this.complainService.makeComplain(data).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
