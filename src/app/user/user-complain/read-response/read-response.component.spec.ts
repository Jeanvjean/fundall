import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadResponseComponent } from './read-response.component';

describe('ReadResponseComponent', () => {
  let component: ReadResponseComponent;
  let fixture: ComponentFixture<ReadResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
