import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { ComplainService } from  '../../../shared/complain.service';
import { ToasterService } from '../../../shared/toaster.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-read-response',
  templateUrl: './read-response.component.html',
  styleUrls: ['./read-response.component.css']
})
export class ReadResponseComponent implements OnInit {
    response = {}
  constructor(
      private router: Router,
      private route:ActivatedRoute,
      private complainService:ComplainService,
      private toaster:ToasterService,
      private spinner:NgxSpinnerService) { }

  ngOnInit() {
      this.read()
  }
  read(){
      var comp_id = this.route.snapshot.params['comp_id']
      this.complainService.readResponse(comp_id).toPromise().then((data:any)=>{
          this.response = data.result
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
