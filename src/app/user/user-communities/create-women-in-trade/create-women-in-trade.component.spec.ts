import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateWomenInTradeComponent } from './create-women-in-trade.component';

describe('CreateWomenInTradeComponent', () => {
  let component: CreateWomenInTradeComponent;
  let fixture: ComponentFixture<CreateWomenInTradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateWomenInTradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateWomenInTradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
