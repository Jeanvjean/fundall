import { Component, OnInit } from '@angular/core';
import { CooprativeService } from '../../../shared/cooprative.service';

@Component({
  selector: 'app-create-women-in-trade',
  templateUrl: './create-women-in-trade.component.html',
  styleUrls: ['./create-women-in-trade.component.css']
})
export class CreateWomenInTradeComponent implements OnInit {
    name = '';
    location = '';
    photo:File = null;
  constructor(private cooprativeService:CooprativeService) { }
  cooprative_photo(event){
      this.photo = <File>event.target.files[0]
  }
  ngOnInit() {
  }
  create(){
      var fd = new FormData()
      fd.append('name',this.name)
      fd.append('location',this.location)
      fd.append('photo',this.photo)
      this.cooprativeService.create_wit(fd).toPromise().then((data:any)=>{
          console.log(data)
      }).catch((e)=>{
          console.log(e)
      })
  }
}
