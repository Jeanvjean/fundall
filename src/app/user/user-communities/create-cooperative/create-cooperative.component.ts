import { ActivatedRoute } from '@angular/router';
import { LoanService } from './../../../shared/loan.service';
import { ToasterService } from './../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Component, OnInit } from '@angular/core';
import { CooprativeService } from '../../../shared/cooprative.service';

@Component({
  selector: 'app-create-cooperative',
  templateUrl: './create-cooperative.component.html',
  styleUrls: ['./create-cooperative.component.css']
})
export class CreateCooperativeComponent implements OnInit {
  cooperative = { name: "", description: "", requirements: [""], photo: "", amount: "", type: "", location: "" };
  communityName = "";
  photo: File = null;
  constructor(private loanService: LoanService,
    private spinner: Ng4LoadingSpinnerService,
    private toaster: ToasterService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.communityName = this.route.snapshot.params['community'];
    this.cooperative.type = this.communityName == "community" ? "1" : "2";
    this.communityName = this.communityName.toUpperCase();
  }

  addRequirement() {
    this.cooperative.requirements.push("");
  }
  deleteRequirement(index) {
    this.cooperative.requirements.splice(index, 1);
  }
  fileSelected(event) {
    // this.LIcon = <File> event.target.files[0]
    var files = event.target.files
    var filename = files.name

    const fileReader = new FileReader()
    fileReader.addEventListener('load', () => {
      this.cooperative.photo = fileReader.result
    })
    fileReader.readAsDataURL(files[0])
    this.photo = files[0]
  }
  trackByFn(index: any, item: any) {
    return index;
  }
  createCoperative() {
    this.spinner.show()
    var formData = new FormData();
    var self = this;
    for (var key in this.cooperative) {
      if (key != "image")
        formData.append(key, this.cooperative[key]);
    }
    console.log(this.cooperative);
    if (this.photo != null) {
      formData.append('photo', this.photo, this.photo.name)
    }
    else {
      this.toaster.error("You must set an Icon");
      return;
    }
    this.loanService.createCoperative(formData).toPromise().then((data: any) => {
      if (data) {
        this.spinner.hide()
        this.toaster.success(data.message)
      }
    }).catch((e) => {
      this.spinner.hide()
      this.toaster.error(e.error.message)
    })
  }
  //   create(){
  //       var fd = new FormData()
  //       fd.append('name',this.name)
  //       fd.append('location',this.location)
  //       fd.append('photo',this.photo)
  //       this.cooprativeService.create(fd).toPromise().then((data:any)=>{
  //           console.log(data)
  //       }).catch((e)=>{
  //           console.log(e)
  //       })
  //   }
}
