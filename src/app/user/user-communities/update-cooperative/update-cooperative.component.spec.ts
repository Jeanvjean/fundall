import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCooperativeComponent } from './update-cooperative.component';

describe('UpdateCooperativeComponent', () => {
  let component: UpdateCooperativeComponent;
  let fixture: ComponentFixture<UpdateCooperativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCooperativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCooperativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
