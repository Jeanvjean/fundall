import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUserCooperativeComponent } from './view-user-cooperative.component';

describe('ViewUserCooperativeComponent', () => {
  let component: ViewUserCooperativeComponent;
  let fixture: ComponentFixture<ViewUserCooperativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUserCooperativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUserCooperativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
