import { Router, ActivatedRoute } from '@angular/router';
import { ToasterService } from './../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { BASE_API } from './../../../shared/api.service';
import { Component, OnInit } from '@angular/core';
import { CooprativeService } from '../../../shared/cooprative.service';
import { CcService } from '../../../shared/cc.service';


@Component({
    selector: 'app-view-user-cooperative',
    templateUrl: './view-user-cooperative.component.html',
    styleUrls: ['./view-user-cooperative.component.css']
})
export class ViewUserCooperativeComponent implements OnInit {
    communties = [];
    type = 1;
    constructor(
        private cooprativeService: CooprativeService,
        private ccService: CcService,
        private toaster: ToasterService,
        private spinner: Ng4LoadingSpinnerService,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.getAll()
    }
    isAdmin(members) {
        return members.find(m => m.id == localStorage.getItem("user_id") && m.type == "admin");
    }
    getMemberShipType(members) {
        return this.isAdmin(members) ? "ADMIN" : "MEMBER";
    }
    getDateJoined(members) {
        return members.find(m => m.id == localStorage.getItem("user_id")).date_joined;

    }
    viewCommunity(id, members) {
        if (this.isAdmin(members)) {
            this.router.navigate(['/admin-community/' + id]);
        }
        else {
            this.router.navigate(['/view-community/' + id]);
        }
    }
    getAll() {
        this.spinner.show();
        var type = this.route.snapshot.params['type'];
        this.type = type;
        this.ccService.fetch_all_communities(type).toPromise().then((data: any) => {
            this.spinner.hide();
            if(data.result.coops)
            this.communties = data.result.coops;
            else
            this.communties = data.result;
            
        }).catch((e) => {
            this.spinner.hide();
            console.log(e)
        })
    }
    isAmember(members) {
        members = members.map(m => m.id);
        return this.compare(members);
    }
    requested(applicants) {
        return this.compare(applicants);
    }
    compare(data) {
        return data.indexOf(localStorage.getItem("user_id")) > -1;
    }
    join(id) {
        this.spinner.show()
        this.ccService.joinCommunity(id).toPromise().then((data: any) => {
            if (data.success) {
                this.toaster.success(data.message)
            }
            else {
                this.toaster.error(data.message)
            }
            this.spinner.hide()
            this.getAll();
        })
            .catch((data: any) => {
                this.spinner.hide()
            })
    }
    setImage(img) {
        return BASE_API + img;
    }

}
