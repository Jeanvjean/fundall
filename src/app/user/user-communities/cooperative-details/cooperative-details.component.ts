import { Component, OnInit } from '@angular/core';
import { CooprativeService } from '../../../shared/cooprative.service';
import { Route,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cooperative-details',
  templateUrl: './cooperative-details.component.html',
  styleUrls: ['./cooperative-details.component.css']
})
export class CooperativeDetailsComponent implements OnInit {

  constructor(
      private cooprativeService:CooprativeService,
      private route:ActivatedRoute) { }

  ngOnInit() {
      this.view()
  }
  view(){
      var id = this.route.snapshot.params['coop_id']
      this.cooprativeService.single(id).toPromise().then((data:any)=>{
          console.log(data)
      }).catch((e)=>{
          console.log(e)
      })
  }
  join(coop_id){
        var appl_id = localStorage.getItem('user_id')
      this.cooprativeService.joinCoop(coop_id,appl_id).toPromise().then((data:any)=>{
          console.log(data)
      }).catch((e)=>{
          console.log(e)
      })
  }
}
