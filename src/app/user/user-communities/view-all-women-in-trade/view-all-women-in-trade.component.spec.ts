import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAllWomenInTradeComponent } from './view-all-women-in-trade.component';

describe('ViewAllWomenInTradeComponent', () => {
  let component: ViewAllWomenInTradeComponent;
  let fixture: ComponentFixture<ViewAllWomenInTradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAllWomenInTradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAllWomenInTradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
