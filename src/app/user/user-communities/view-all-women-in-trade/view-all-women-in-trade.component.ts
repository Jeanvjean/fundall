import { Component, OnInit } from '@angular/core';
import { CooprativeService } from '../../../shared/cooprative.service';
import { CcService } from '../../../shared/cc.service';

@Component({
  selector: 'app-view-all-women-in-trade',
  templateUrl: './view-all-women-in-trade.component.html',
  styleUrls: ['./view-all-women-in-trade.component.css']
})
export class ViewAllWomenInTradeComponent implements OnInit {
    w = []
  constructor(
      private cooprativeService:CooprativeService,
      private ccService:CcService) { }

  ngOnInit() {
  }
  wit(){
      this.ccService.women_society().toPromise().then((data:any)=>{
          console.log(data)
          this.w = data.result
      }).catch((e)=>{
          console.log(e)
      })
  }
}
