import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewOneWomenInTradeComponent } from './view-one-women-in-trade.component';

describe('ViewOneWomenInTradeComponent', () => {
  let component: ViewOneWomenInTradeComponent;
  let fixture: ComponentFixture<ViewOneWomenInTradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewOneWomenInTradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewOneWomenInTradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
