import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { UserService } from  '../../shared/user.service';
import { ToasterService } from '../../shared/toaster.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-account-verified',
  templateUrl: './account-verified.component.html',
  styleUrls: ['./account-verified.component.css']
})
export class AccountVerifiedComponent implements OnInit {

  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private userService:UserService,
      private toaster:ToasterService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.verified()
  }
  verified(){
      this.spinner.show()
      var token = this.route.snapshot.params['token']
      this.userService.verifyUser(token).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.router.navigate(['/signin'])
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }

}
