import { Component, OnInit } from '@angular/core';
import { UserNavComponent } from '../user-nav/user-nav.component';
import { LoanService } from '../../shared/loan.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
@Component({
  selector: 'app-l-details',
  templateUrl: './l-details.component.html',
  styleUrls: ['./l-details.component.css']
})
export class LDetailsComponent implements OnInit {
    loan={
      loanName:'',
      description:'',
      requirements:[],
      interestRate:'',
      _id:''
    };
  constructor(
      private loanService:LoanService,
      private router:Router,
      private route:ActivatedRoute,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.getLoan()
  }
  getLoan(){
      this.spinner.show()
      var loan_id = this.route.snapshot.params['loan_id'];
      this.loanService.loanDetail(loan_id).toPromise().then((data:any)=>{
          console.log(data)
          this.spinner.hide()
        //   this.toaster.success(data.message)
          this.loan = data
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
        //   console.log(e)
      })
  }
}
