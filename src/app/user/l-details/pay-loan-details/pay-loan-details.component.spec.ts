import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayLoanDetailsComponent } from './pay-loan-details.component';

describe('PayLoanDetailsComponent', () => {
  let component: PayLoanDetailsComponent;
  let fixture: ComponentFixture<PayLoanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayLoanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayLoanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
