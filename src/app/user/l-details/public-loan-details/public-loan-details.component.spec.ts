import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicLoanDetailsComponent } from './public-loan-details.component';

describe('PublicLoanDetailsComponent', () => {
  let component: PublicLoanDetailsComponent;
  let fixture: ComponentFixture<PublicLoanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicLoanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicLoanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
