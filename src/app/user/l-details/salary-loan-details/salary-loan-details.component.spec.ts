import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalaryLoanDetailsComponent } from './salary-loan-details.component';

describe('SalaryLoanDetailsComponent', () => {
  let component: SalaryLoanDetailsComponent;
  let fixture: ComponentFixture<SalaryLoanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalaryLoanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalaryLoanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
