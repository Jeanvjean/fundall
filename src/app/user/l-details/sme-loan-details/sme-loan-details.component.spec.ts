import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmeLoanDetailsComponent } from './sme-loan-details.component';

describe('SmeLoanDetailsComponent', () => {
  let component: SmeLoanDetailsComponent;
  let fixture: ComponentFixture<SmeLoanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmeLoanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmeLoanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
