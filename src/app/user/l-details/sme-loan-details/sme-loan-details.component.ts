import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sme-loan-details',
  templateUrl: './sme-loan-details.component.html',
  styleUrls: ['./sme-loan-details.component.css']
})
export class SmeLoanDetailsComponent implements OnInit {    
        amount:number = 0;
        interestRate:number = 0.05;
        total:number = 0;
        monthlyRepay:number = 0;
        duration:number = 0;
    calc(){
        this.total = +this.amount + +( this.interestRate * this.amount);
        this.monthlyRepay = this.total / this.duration;
    }
  constructor() { }

  ngOnInit() {
  }

}
