import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoLoanDetailsComponent } from './auto-loan-details.component';

describe('AutoLoanDetailsComponent', () => {
  let component: AutoLoanDetailsComponent;
  let fixture: ComponentFixture<AutoLoanDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoLoanDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoLoanDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
