import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auto-loan-details',
  templateUrl: './auto-loan-details.component.html',
  styleUrls: ['./auto-loan-details.component.css']
})
export class AutoLoanDetailsComponent implements OnInit {

        amount:number = 0;
        interestRate:number = 0.05;
        total:number = 0;
        monthlyRepay:number = 0;
        duration:number = 0;
    calc(){
        this.total = +this.amount + +( this.interestRate * this.amount);
        this.monthlyRepay = this.total / this.duration;
    }
  constructor() { }

  ngOnInit() {
  }

}
