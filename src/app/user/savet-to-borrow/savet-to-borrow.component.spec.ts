import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavetToBorrowComponent } from './savet-to-borrow.component';

describe('SavetToBorrowComponent', () => {
  let component: SavetToBorrowComponent;
  let fixture: ComponentFixture<SavetToBorrowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavetToBorrowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavetToBorrowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
