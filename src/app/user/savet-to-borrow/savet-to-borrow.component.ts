import { UserService } from './../../shared/user.service';
import { TestBed } from '@angular/core/testing';
import { ToasterService } from './../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-savet-to-borrow',
  templateUrl: './savet-to-borrow.component.html',
  styleUrls: ['../request-loan/auto-loan/auto-loan.component.css']
})
export class SavetToBorrowComponent implements OnInit {
  public canShow = false;
  private saveall = { username: "", password: "",crevance_id:localStorage.getItem("user_id")}
  constructor(private http: HttpClient, private spinner: Ng4LoadingSpinnerService,
   private toaster: ToasterService,private userService:UserService) { }
  setUpSaveToBorrow() {
    this.canShow = true;
  }
  ngOnInit() {
    this.isLinked();
  }
  isLinked(){
     this.spinner.show()
      this.userService.isLinked().toPromise().then((result: any) => {
        this.spinner.hide();
        if(result.status){
          this.canShow = true;
          this.toaster.success("Account has been set up to use save to borrow");
        }
        else{
          this.canShow = false;
        }
      })
      .catch(err=>{
        this.spinner.hide()

      })
  }
  linkAccount() {
    this.spinner.show()
    // this.userService.loginToSaveAll(this.saveall).toPromise().then((result: any) => {

      // if (result.status) {
        // this.toaster.success("Account confirmed");
        // this.toaster.info("Linking your account");
        // this.spinner.show();
        // result = result.data;
        this.userService.confirmAccount(this.saveall).toPromise().then((result: any) => {
            if (result.status) {
              this.toaster.success("Account Linked successfully");

            }
            else {
              this.toaster.info("An error occured Linking your account");
            }
          })
          .catch(err => {
            this.toaster.info("An error occured Linking your account");
            this.spinner.hide();

          })
      // }
      // else {
      //   this.toaster.error("Invalid account");
      // }
      this.spinner.hide();
    // })
    //   .catch(err => {
    //     this.spinner.hide();

    //     this.toaster.info("An error occured Linking your account");
    //   })
  }

}
