import { Component, OnInit } from '@angular/core';
import { ListingService } from '../../../shared/listing.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-create-listing',
  templateUrl: './create-listing.component.html',
  styleUrls: ['../../request-loan/request-loan.component.css']
})
export class CreateListingComponent implements OnInit {
    company_cac: File = null;
    advert_image: File = null;
    isLoading:boolean = false
    listing = {
            name:'',
            description:'',
            company:'',
            contact_information:'',
            duration:'',
            top_advert:'',
            advert_cost:''
    };
  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private listingService:ListingService,
      private spinner:Ng4LoadingSpinnerService) { }
      cac_reg(event){
          this.company_cac = <File>event.target.files[0]
      }
      add_image(event){
          this.advert_image = <File>event.target.files[0]
      }
  ngOnInit() {
  }
  create(){
      this.spinner.show()
      this.isLoading = true;
      var data = new FormData()
      data.append('name',this.listing.name)
      data.append('description',this.listing.description)
      data.append('company',this.listing.company)
      data.append('contact_information',this.listing.contact_information)
      data.append('duration',this.listing.duration)
      data.append('top_advert',this.listing.top_advert)
      data.append('advert_cost',this.listing.advert_cost)
      data.append('company_cac',this.company_cac)
      data.append('photo',this.advert_image)
      this.listingService.create(data).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          this.isLoading = false;
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          this.isLoading = false;
      })
  }
}
