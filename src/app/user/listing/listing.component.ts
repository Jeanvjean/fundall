import { BASE_API } from './../../shared/api.service';
import { Component, OnInit } from '@angular/core';
import { ListingService } from '../../shared/listing.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {
    listing = [];
  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private listingService:ListingService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.myListing()
  }
  myListing(){
      this.spinner.show()
      this.listingService.getAll().toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.info(data.message)
          this.listing = data.result
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
      })
  }
 
}
