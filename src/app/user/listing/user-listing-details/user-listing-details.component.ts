import { BASE_API } from './../../../shared/api.service';
import { Component, OnInit } from '@angular/core';
import { ListingService } from '../../../shared/listing.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-user-listing-details',
  templateUrl: './user-listing-details.component.html',
  styleUrls: ['./user-listing-details.component.css']
})
export class UserListingDetailsComponent implements OnInit {
      listings = [];
      active:number;
      total:number;
      all = false;
      user_id = "";
  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private listingService:ListingService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.allListing();
      this.all = this.route.snapshot.params['type'] == 1 ? true : false;
      this.user_id = localStorage.getItem("user_id");
      console.log("hello");
      
  }
  allListing(){
      this.spinner.show()
      this.listingService.getAll().toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.info(data.message)
          let list = data.result.listing;
          if(this.all){
            this.listings = list.filter(list=>list.approved == 2);
          }else{
            this.listings = list.filter(list=>list.accountHolder == this.user_id);
          }
          this.total = list.length;
          this.active = this.listings.length;
      }).catch((e)=>{
          this.spinner.hide()
        //   this.toaster.error(e.error.message)
          this.spinner.hide()
          console.log(e)
      })
  }
  getOne(){
      var list_id = this.route.snapshot.params['list_id']
      this.listingService.viewListing(list_id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.listings = data.result
          this.toaster.success(data.message)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
      })
  }
  delete(){
      this.spinner.show()
      var list_id = this.route.snapshot.params['list_id']
      this.listingService.delete(list_id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
      })
  }
  setImage(image){
    let api = BASE_API;
    return api + image;
}
}
