import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserListingDetailsComponent } from './user-listing-details.component';

describe('UserListingDetailsComponent', () => {
  let component: UserListingDetailsComponent;
  let fixture: ComponentFixture<UserListingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserListingDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
