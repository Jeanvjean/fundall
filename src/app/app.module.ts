import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { appRoutes } from './routes'
import { AuthGuard } from './auth/auth.guard'
// import { AuthInterceptor } from './auth/auth.interceptor'
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { NinjaService } from './shared/ninja.service'
import { UserService } from './shared/user.service'
import { NgxSpinnerModule } from 'ngx-spinner';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { MomentModule } from 'angular2-moment';
import { NgxPaginationModule } from 'ngx-pagination';
import { DaterangepickerModule } from 'angular-2-daterangepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { LoginComponent } from './user/login/login.component';
import { UserComponent } from './user/user.component';
import { SignupComponent } from './user/signup/signup.component';
import { NinjaComponent } from './ninja/ninja.component';
import { EditNinjaComponent } from './ninja/edit-ninja/edit-ninja.component';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { AdminNavComponent } from './admin/admin-nav/admin-nav.component';
import { UserManagementComponent } from './admin/user-management/user-management.component';
import { UserDetailsComponent } from './admin/user-details/user-details.component';
import { ApprovalComponent } from './approval/approval.component';
import { DisburseComponent } from './disburse/disburse.component';
import { CcComponent } from './cc/cc.component';
import { CreateCcComponent } from './admin/create-cc/create-cc.component';
import { CreateDisburseComponent } from './admin/create-disburse/create-disburse.component';
import { CreateApprovalComponent } from './admin/create-approval/create-approval.component';
import { ViewCcComponent } from './admin/view-cc/view-cc.component';
import { ViewDisburseComponent } from './admin/view-disburse/view-disburse.component';
import { ViewApprovalComponent } from './admin/view-approval/view-approval.component';
import { AmInfoComponent } from './admin/am-info/am-info.component';
import { DInfoComponent } from './admin/d-info/d-info.component';
import { CcInfoComponent } from './admin/cc-info/cc-info.component';
import { CreateLoanComponent } from './admin/create-loan/create-loan.component';
import { UpdateLoanComponent } from './admin/update-loan/update-loan.component';
import { ViewLoansComponent } from './admin/view-loans/view-loans.component';
import { CommunitiesComponent } from './admin/communities/communities.component';
import { AddListingComponent } from './admin/add-listing/add-listing.component';
import { ProfileComponent } from './user/profile/profile.component';
import { EditProfileComponent } from './user/edit-profile/edit-profile.component';
import { RequestLoanComponent } from './user/request-loan/request-loan.component';
import { LoanHistoryComponent } from './user/loan-history/loan-history.component';
import { PendingLoanComponent } from './user/loan-history/pending-loan/pending-loan.component';
import { ActiveLoanComponent } from './user/loan-history/active-loan/active-loan.component';
import { RejectedLoanComponent } from './user/loan-history/rejected-loan/rejected-loan.component';
import { DueLoanComponent } from './user/loan-history/due-loan/due-loan.component';
import { UserNavComponent } from './user/user-nav/user-nav.component';
import { ConfirmEmailComponent } from './user/confirm-email/confirm-email.component';
import { LoanDetailsComponent } from './admin/loan-details/loan-details.component';
import { UserComplainComponent } from './user/user-complain/user-complain.component';
import { ComplainResponseComponent } from './admin/complain-response/complain-response.component';
import { AllComplainsComponent } from './admin/all-complains/all-complains.component';
import { RequestedLoansComponent } from './admin/requested-loans/requested-loans.component';
import { ViewLinstingComponent } from './admin/add-listing/view-linsting/view-linsting.component';
import { CommunityDetailsComponent } from './admin/community-details/community-details.component';
import { ListingComponent } from './user/listing/listing.component';
import { CreateListingComponent } from './user/listing/create-listing/create-listing.component';
import { UpdateListingComponent } from './user/listing/update-listing/update-listing.component';
import { SettingsComponent } from './user/settings/settings.component';
import { UserCommunitiesComponent } from './user/user-communities/user-communities.component';
import { UserTransactionsComponent } from './user/user-transactions/user-transactions.component';
import { LoanNavComponent } from './user/loan-history/loan-nav/loan-nav.component';
import { AccountVerifiedComponent } from './user/account-verified/account-verified.component';
import { HistoryComponent } from './user/loan-history/history/history.component';
import { ReadComplainComponent } from './admin/all-complains/read-complain/read-complain.component';
import { RequestDetailsComponent } from './admin/requested-loans/request-details/request-details.component';
import { ReadResponseComponent } from './user/user-complain/read-response/read-response.component';
import { SmeLoanComponent } from './user/request-loan/sme-loan/sme-loan.component';
import { SalaryLoanComponent } from './user/request-loan/salary-loan/salary-loan.component';
import { PayLoanComponent } from './user/request-loan/pay-loan/pay-loan.component';
import { AutoLoanComponent } from './user/request-loan/auto-loan/auto-loan.component';
import { PublicLoanComponent } from './user/request-loan/public-loan/public-loan.component';
import { LDetailsComponent } from './user/l-details/l-details.component';
import { WalletComponent } from './user/wallet/wallet.component';
import { AddCardComponent } from './user/wallet/add-card/add-card.component';
import { AddAccountComponent } from './user/wallet/add-account/add-account.component';
import { FundWalletComponent } from './user/wallet/fund-wallet/fund-wallet.component';
import { CreateCooperativeComponent } from './user/user-communities/create-cooperative/create-cooperative.component';
import { ViewUserCooperativeComponent } from './user/user-communities/view-user-cooperative/view-user-cooperative.component';
import { CooperativeDetailsComponent } from './user/user-communities/cooperative-details/cooperative-details.component';
import { UpdateCooperativeComponent } from './user/user-communities/update-cooperative/update-cooperative.component';
import { UserListingDetailsComponent } from './user/listing/user-listing-details/user-listing-details.component';
import { AutoLoanDetailsComponent } from './user/l-details/auto-loan-details/auto-loan-details.component';
import { PayLoanDetailsComponent } from './user/l-details/pay-loan-details/pay-loan-details.component';
import { SmeLoanDetailsComponent } from './user/l-details/sme-loan-details/sme-loan-details.component';
import { SalaryLoanDetailsComponent } from './user/l-details/salary-loan-details/salary-loan-details.component';
import { PublicLoanDetailsComponent } from './user/l-details/public-loan-details/public-loan-details.component';
import { CcNavComponent } from './cc/cc-nav/cc-nav.component';
import { CcLoanRequestsComponent } from './cc/cc-loan-requests/cc-loan-requests.component';
import { CcLoanDetailsComponent } from './cc/cc-loan-details/cc-loan-details.component';
import { CcTicketsComponent } from './cc/cc-tickets/cc-tickets.component';
import { CcTicketsViewComponent } from './cc/cc-tickets/cc-tickets-view/cc-tickets-view.component';
import { CcListingRequestsComponent } from './cc/cc-listing-requests/cc-listing-requests.component';

import { CcListingDetailComponent } from './cc/cc-listing-requests/cc-listing-detail/cc-listing-detail.component';

import { RunningListingComponent } from './cc/cc-listing-requests/running-listing/running-listing.component';
import { PasswordComponent } from './password/password.component';
import { ChangePasswordComponent } from './password/change-password/change-password.component';
import { CcTicketsResolvedComponent } from './cc/cc-tickets/cc-tickets-resolved/cc-tickets-resolved.component';
import { OpenTicketsComponent } from './cc/cc-tickets/open-tickets/open-tickets.component';
import { CcDueMessageComoponent } from './cc/cc-due-message/cc-due-message.component';
import { TicketNavComponent } from './cc/cc-tickets/ticket-nav/ticket-nav.component';
import { PendingListingRequestsComponent } from './cc/cc-listing-requests/pending-listing-requests/pending-listing-requests.component';
import { ListingNavComponent } from './cc/cc-listing-requests/listing-nav/listing-nav.component';
import { DuePaymentsComponent } from './cc/due-payments/due-payments.component';
import { ActivityComponent } from './approval/activity/activity.component';
import { ApprovalLoansComponent } from './approval/approval-loans/approval-loans.component';
import { ApprovalNavComponent } from './approval/approval-nav/approval-nav.component';
import { ApprovalCommunityComponent } from './approval/approval-community/approval-community.component';
import { ApprLoanNavComponent } from './approval/approval-loans/appr-loan-nav/appr-loan-nav.component';
import { ApprRejectedLoansComponent } from './approval/approval-loans/appr-rejected-loans/appr-rejected-loans.component';
import { ApprApprovedLoansComponent } from './approval/approval-loans/appr-approved-loans/appr-approved-loans.component';
import { ApprRequestedLoansComponent } from './approval/approval-loans/appr-requested-loans/appr-requested-loans.component';
import { ApprAllLoansComponent } from './approval/approval-loans/appr-all-loans/appr-all-loans.component';
import { ApprLoanReqDetailsComponent } from './approval/appr-loan-req-details/appr-loan-req-details.component';
import { ApprPayLoanComponent } from './approval/approval-loans/appr-pay-loan/appr-pay-loan.component';
import { ApprPublicLoanComponent } from './approval/approval-loans/appr-public-loan/appr-public-loan.component';
import { ApprSalaryLoanComponent } from './approval/approval-loans/appr-salary-loan/appr-salary-loan.component';
import { ApprSmeLoanComponent } from './approval/approval-loans/appr-sme-loan/appr-sme-loan.component';
import { ApprAutoLoanComponent } from './approval/approval-loans/appr-auto-loan/appr-auto-loan.component';
import { ApprWitLoanComponent } from './approval/approval-loans/appr-wit-loan/appr-wit-loan.component';
import { LoanCardComponent } from './shared/loan-card/loan-card.component';
import { PendingLoanCardComponent } from './shared/pending-loan-card/pending-loan-card.component';
import { RejectedLoanCardComponent } from './shared/rejected-loan-card/rejected-loan-card.component';
import { LoanSummaryComponent } from './shared/loan-summary/loan-summary.component';
import { CreateWomenInTradeComponent } from './user/user-communities/create-women-in-trade/create-women-in-trade.component';
import { ViewAllWomenInTradeComponent } from './user/user-communities/view-all-women-in-trade/view-all-women-in-trade.component';
import { ViewOneWomenInTradeComponent } from './user/user-communities/view-one-women-in-trade/view-one-women-in-trade.component';
import { CcUserDetailsComponent } from './cc/cc-user-details/cc-user-details.component';
import { CcRejectedComponent } from './cc/cc-loan-details/cc-rejected/cc-rejected.component';
import { CcRequestedComponent } from './cc/cc-loan-details/cc-requested/cc-requested.component';
import { CcApprovedComponent } from './cc/cc-loan-details/cc-approved/cc-approved.component';
import { CcCommunitiesComponent } from './cc/cc-communities/cc-communities.component';
import { CcJoinCommunityComponent } from './cc/cc-communities/cc-join-community/cc-join-community.component';
import { CcMemberDetailComponent } from './cc/cc-communities/cc-member-detail/cc-member-detail.component';
import { CcLoanDNavComponent } from './cc/cc-loan-details/cc-loan-d-nav/cc-loan-d-nav.component';
import { ViewMyCommunityComponent } from './view-my-community/view-my-community.component';
import { AdminCommunityComponent } from './admin-community/admin-community.component';
import { MemberProfileComponent } from './member-profile/member-profile.component';
import { JoinRequestComponent } from './join-request/join-request.component';
import { ApplyCooperativeLoanComponent } from './apply-cooperative-loan/apply-cooperative-loan.component';
import { CooperativeloanComponent } from './cooperativeloan/cooperativeloan.component';
import { GrantedLoanCardComponent } from './shared/granted-loan-card/granted-loan-card.component';
import { PendingLoanComponentComponent } from './pending-loan-component/pending-loan-component.component';
import { CooperativeLoanRequestComponent } from './cooperative-loan-request/cooperative-loan-request.component';
import { MemberCardComponent } from './shared/member-card/member-card.component';
import { ProcessLoanRequestComponent } from './shared/process-loan-request/process-loan-request.component';
import { CreateAdminCommunitiesComponent } from './admin/communities/create-admin-communities/create-admin-communities.component';
import { AdminAllCooprativesComponent } from './admin/communities/admin-all-coopratives/admin-all-coopratives.component';
import { AdminWomenInTradeComponent } from './admin/communities/admin-women-in-trade/admin-women-in-trade.component';
import { FilterPipe } from './shared/filter.pipe';
import { SideNavBarComponent } from './shared/side-nav-bar/side-nav-bar.component';
import { SpinnerComponent } from './shared/my-spinner';

import { NotificationComponent } from './user/notification/notification.component';
import { SavetToBorrowComponent } from './user/savet-to-borrow/savet-to-borrow.component';
import { DisburseLoanComponent } from './approval/disburse-loan/disburse-loan.component';
import { DisburseNavComponent } from './disburse/disburse-nav/disburse-nav.component';
import { LFilterPipe } from './shared/l-filter.pipe';
import { LoanFilterPipe } from './shared/loan-filter.pipe';
import { SearchPipe } from './search.pipe';
/** config angular i18n **/
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
registerLocaleData(en);


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    LoginComponent,
    SignupComponent,
    NinjaComponent,
    EditNinjaComponent,
    HomeComponent,
    AdminComponent,
    AdminNavComponent,
    UserManagementComponent,
    UserDetailsComponent,
    ApprovalComponent,
    DisburseComponent,
    CcComponent,
    CreateCcComponent,
    CreateDisburseComponent,
    CreateApprovalComponent,
    ViewCcComponent,
    ViewDisburseComponent,
    ViewApprovalComponent,
    AmInfoComponent,
    DInfoComponent,
    CcInfoComponent,
    CreateLoanComponent,
    UpdateLoanComponent,
    ViewLoansComponent,
    CommunitiesComponent,
    AddListingComponent,
    ProfileComponent,
    EditProfileComponent,
    RequestLoanComponent,
    LoanHistoryComponent,
    PendingLoanComponent,
    ActiveLoanComponent,
    RejectedLoanComponent,
    DueLoanComponent,
    UserNavComponent,
    ConfirmEmailComponent,
    LoanDetailsComponent,
    UserComplainComponent,
    ComplainResponseComponent,
    AllComplainsComponent,
    RequestedLoansComponent,
    ViewLinstingComponent,
    CommunityDetailsComponent,
    ListingComponent,
    CreateListingComponent,
    UpdateListingComponent,
    SettingsComponent,
    UserCommunitiesComponent,
    UserTransactionsComponent,
    LoanNavComponent,
    AccountVerifiedComponent,
    HistoryComponent,
    ReadComplainComponent,
    RequestDetailsComponent,
    ReadResponseComponent,
    SmeLoanComponent,
    SalaryLoanComponent,
    PayLoanComponent,
    AutoLoanComponent,
    PublicLoanComponent,
    LDetailsComponent,
    WalletComponent,
    AddCardComponent,
    AddAccountComponent,
    FundWalletComponent,
    CreateCooperativeComponent,
    ViewUserCooperativeComponent,
    CooperativeDetailsComponent,
    UpdateCooperativeComponent,
    UserListingDetailsComponent,
    AutoLoanDetailsComponent,
    PayLoanDetailsComponent,
    SmeLoanDetailsComponent,
    SalaryLoanDetailsComponent,
    PublicLoanDetailsComponent,
    CcNavComponent,
    CcLoanRequestsComponent,
    CcLoanDetailsComponent,
    CcTicketsComponent,
    CcTicketsViewComponent,
    CcListingRequestsComponent,
    CcListingDetailComponent,
    RunningListingComponent,
    PasswordComponent,
    ChangePasswordComponent,
    CcTicketsResolvedComponent,
    CcDueMessageComoponent,
    TicketNavComponent,
    OpenTicketsComponent,
    PendingListingRequestsComponent,
    ListingNavComponent,
    DuePaymentsComponent,
    ActivityComponent,
    ApprovalLoansComponent,
    ApprovalNavComponent,
    ApprovalCommunityComponent,
    ApprLoanNavComponent,
    ApprRejectedLoansComponent,
    ApprApprovedLoansComponent,
    ApprRequestedLoansComponent,
    ApprAllLoansComponent,
    ApprLoanReqDetailsComponent,
    ApprPayLoanComponent,
    ApprPublicLoanComponent,
    ApprSalaryLoanComponent,
    ApprSmeLoanComponent,
    ApprAutoLoanComponent,
    ApprWitLoanComponent,
    LoanCardComponent,
    PendingLoanCardComponent,
    RejectedLoanCardComponent,
    LoanSummaryComponent,
    CreateWomenInTradeComponent,
    ViewAllWomenInTradeComponent,
    ViewOneWomenInTradeComponent,
    CcUserDetailsComponent,
    CcRejectedComponent,
    CcRequestedComponent,
    CcApprovedComponent,
    CcCommunitiesComponent,
    CcJoinCommunityComponent,
    CcMemberDetailComponent,
    CcLoanDNavComponent,
    ViewMyCommunityComponent,
    AdminCommunityComponent,
    MemberProfileComponent,
    JoinRequestComponent,
    ApplyCooperativeLoanComponent,
    CooperativeloanComponent,
    GrantedLoanCardComponent,
    PendingLoanComponentComponent,
    CooperativeLoanRequestComponent,
    MemberCardComponent,
    ProcessLoanRequestComponent,
    CreateAdminCommunitiesComponent,
    AdminAllCooprativesComponent,
    AdminWomenInTradeComponent,
    FilterPipe,
    SpinnerComponent,
    NotificationComponent,
    SideNavBarComponent,
    SavetToBorrowComponent,
    SpinnerComponent,
    DisburseLoanComponent,
    DisburseNavComponent,
    LFilterPipe,
    LoanFilterPipe,
    SearchPipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MomentModule,
    HttpClientModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    Ng4LoadingSpinnerModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    DaterangepickerModule,
    NgZorroAntdModule
    
  ],
  providers: [UserService,NinjaService,AuthGuard,
  { provide: NZ_I18N, useValue: en_US }
 
  ],
  bootstrap: [AppComponent]
})
export class AppModule {  }
