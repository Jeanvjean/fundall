import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CooperativeLoanRequestComponent } from './cooperative-loan-request.component';

describe('CooperativeLoanRequestComponent', () => {
  let component: CooperativeLoanRequestComponent;
  let fixture: ComponentFixture<CooperativeLoanRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CooperativeLoanRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CooperativeLoanRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
