import { BASE_API } from './../shared/api.service';
import { ToasterService } from './../shared/toaster.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CcService } from './../shared/cc.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cooperative-loan-request',
  templateUrl: './cooperative-loan-request.component.html',
  styleUrls: ['./cooperative-loan-request.component.css']
})
export class CooperativeLoanRequestComponent implements OnInit {
user = "";
  loans = [];
  community = "";
   constructor(private api: CcService, private route: ActivatedRoute,
    private spinner: NgxSpinnerService, private toaster: ToasterService,
    private router: Router) { }

  ngOnInit() {
   this.community = this.route.snapshot.params['community'];
   this.getLoans();
  }

  viewApplication(user,loan){
    this.router.navigate(["coopratives/loan/processrequest/"+this.community+"/"+user+"/"+loan])
  }

  getLoans(){
    // if(this.)
    this.spinner.show();
    this.api.getCooperativeLoans(this.community,"",0).toPromise().then((data:any)=>{
       this.loans = data.result;
       this.loans.forEach((loan:any)=>{
         loan.user.image = BASE_API+ loan.user.image
       })
       this.spinner.hide();
    })
    .catch(err=>{
      this.toaster.error("An error occured");
      this.spinner.hide();
    })
  }

}
