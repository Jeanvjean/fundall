import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { NinjaService } from  '../../shared/ninja.service';

@Component({
  selector: 'app-edit-ninja',
  templateUrl: './edit-ninja.component.html',
  styleUrls: ['./edit-ninja.component.css']
})
export class EditNinjaComponent implements OnInit {
    selectedFile: File = null;
    ninjaId = String;
    ninja = {
        name:'',
        belt:'',
        image:''
    }

    constructor(private ninjaService:NinjaService, private route: ActivatedRoute, private router:Router) {  }
    onFileSelected(event){
        // console.log(event)
        this.selectedFile = <File>event.target.files[0]
    }
    updateNinja(){
        const ninjaId = this.route.snapshot.params['ninjaId']
        const fd = new FormData()
            fd.append('name',this.ninja.name)
            fd.append('belt',this.ninja.belt)
            if(this.selectedFile != null){
                fd.append('image',this.selectedFile,this.selectedFile.name)
            }
        this.ninjaService.updateNinja(ninjaId, fd).subscribe((data:any)=>{
            alert('Ninja updated')
            this.router.navigate(['create-ninja'])
        })
    }
    getNinja(){
        const ninjaId = this.route.snapshot.params['ninjaId']
        this.ninjaService.getNinja(ninjaId).subscribe((data:any)=>{
            this.ninja = data
        })
    }
  ngOnInit() {
      this.getNinja()
  }
// {headers : new HttpHeaders({'Authorization': localStorage.getItem('userToken')})}
}
