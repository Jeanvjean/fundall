import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router'
import { NinjaService } from  '../shared/ninja.service';

@Component({
  selector: 'app-ninja',
  templateUrl: './ninja.component.html',
  styleUrls: ['./ninja.component.css']
})
export class NinjaComponent implements OnInit {
    selectedFile: File = null;
    defImage = "/assets/images/move-up.png";
    ninjas = [];
    ninja = {
        name : "",
        belt:"",
        image:""
    }
  constructor(private ninjaService:NinjaService, private route: ActivatedRoute) { }
   onFileSelected(event){
       // console.log(event)
       var files = event.target.files
       var filename = files.name

       const fileReader = new FileReader()
       fileReader.addEventListener('load',()=>{
           this.defImage = fileReader.result
       })
       fileReader.readAsDataURL(files[0])
       this.selectedFile = files[0]
       // this.selectedFile = <File>event.target.files[0]
   }
  addNinja(){
      const fd = new FormData()
      fd.append('name',this.ninja.name)
      fd.append('belt',this.ninja.belt)
      if(this.selectedFile != null){
          fd.append('image',this.selectedFile,this.selectedFile.name)
      }
      this.ninjaService.addNinja(fd).subscribe((data : any)=>{
          // console.log(data)
          this.ninjas.push(data)
          // window.location.reload()
      })
  }
  getNinjas(){
      this.ninjaService.getNinjas().subscribe((data:any)=>{
          this.ninjas = data
      })
  }
  deleteNinja(ninja, id) {
      this.ninjaService.deleteNinja(id).toPromise().then(res=>{
          this.ninjas.splice(ninja,1)
      }).catch(e=>{
          console.log({error:'sorry you are not allowed to delete this object'})
      })
  }
  // deleteNinja(ninja, id) {
  //     this.ninjaService.deleteNinja(id).subscribe((data:any)=>{
  //         console.log(data)
  //         window.location.reload()
  //     },
  //     (err: HttpErrorResponse)=>{
  //         console.log(err)
  //     })
  // }
  ngOnInit() {
      this.getNinjas()
  }

}
