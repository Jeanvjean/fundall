import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from './../shared/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CcService } from './../shared/cc.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-apply-cooperative-loan',
  templateUrl: './apply-cooperative-loan.component.html',
  styleUrls: ['./apply-cooperative-loan.component.css']
})
export class ApplyCooperativeLoanComponent implements OnInit {
  loan = { amount: 0, duration: 0, monthlyRepayment: 0, community: "", user: localStorage.getItem("user_id") };
  contribution = 0;
  hide = true;
  isLoading = false;
  constructor(private api: CcService, private route: ActivatedRoute,
    private spinner: NgxSpinnerService, private toaster: ToasterService,
    private router: Router) { }
  community = {
     amount: 0,
      _id: "" ,
      name:''
    };
  ngOnInit() {
    this.community._id = this.route.snapshot.params['id'];
    this.loan.community = this.community._id;
    this.getDetails();
  }

  setMonthlyRepayment() {
    if (this.loan.amount && this.loan.duration) {
      this.loan.monthlyRepayment = this.loan.amount / this.loan.duration;
    }
  }

  applyLoan() {
    this.api.applyCooperativeLoan(this.loan).toPromise().then((data: any) => {
      this.spinner.show();
      if (data.success) {
        this.toaster.success("Loan Requested successfuly");
        this.loan = { amount: 0, duration: 0, monthlyRepayment: 0, community: this.community._id, user: localStorage.getItem("user_id") };

      }
      else {
        this.toaster.error("Loan could not be requested ");

      }
    })
      .catch(err => {
        this.toaster.error("An error occured");
        this.spinner.hide()
      })
  }
  getDetails() {
    this.spinner.show();
    // var id = this.route.snapshot.params['id'];
    this.api.fetchCommunityDetails(this.community._id).toPromise().then((data: any) => {
      this.community = data.result;
      this.spinner.hide();
    })
      .catch(err => {
        this.toaster.error("Community details unavailable");
        this.spinner.hide();
      })

  }
}
