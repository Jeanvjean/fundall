import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyCooperativeLoanComponent } from './apply-cooperative-loan.component';

describe('ApplyCooperativeLoanComponent', () => {
  let component: ApplyCooperativeLoanComponent;
  let fixture: ComponentFixture<ApplyCooperativeLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplyCooperativeLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyCooperativeLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
