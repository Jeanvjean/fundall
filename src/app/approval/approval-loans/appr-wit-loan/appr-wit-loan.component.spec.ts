import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprWitLoanComponent } from './appr-wit-loan.component';

describe('ApprWitLoanComponent', () => {
  let component: ApprWitLoanComponent;
  let fixture: ComponentFixture<ApprWitLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprWitLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprWitLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
