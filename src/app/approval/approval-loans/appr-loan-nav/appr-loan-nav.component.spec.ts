import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprLoanNavComponent } from './appr-loan-nav.component';

describe('ApprLoanNavComponent', () => {
  let component: ApprLoanNavComponent;
  let fixture: ComponentFixture<ApprLoanNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprLoanNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprLoanNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
