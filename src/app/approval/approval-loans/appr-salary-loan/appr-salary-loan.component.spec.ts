import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprSalaryLoanComponent } from './appr-salary-loan.component';

describe('ApprSalaryLoanComponent', () => {
  let component: ApprSalaryLoanComponent;
  let fixture: ComponentFixture<ApprSalaryLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprSalaryLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprSalaryLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
