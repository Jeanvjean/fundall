import { Component, OnInit } from '@angular/core';
import { CcService } from '../../../shared/cc.service';
import { LoanService } from '../../../shared/loan.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-appr-all-loans',
  templateUrl: './appr-all-loans.component.html',
  styleUrls: ['./appr-all-loans.component.css']
})
export class ApprAllLoansComponent implements OnInit {
  loan = [];
  p:number = 1;
  firstName:'';
  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private ccService:CcService,
      private loanService:LoanService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.disbursed_loan()
  }
  disbursed_loan(){
    this.loanService.getAllLoanRequest().toPromise().then((data:any)=>{
      for(var i=0; i < data.result.loans.length; i++){
        if(data.result.loans[i].disbursed && !data.result.loans[i].repaid){
          this.loan.push(data.result.loans[i])
        }
      }
      // console.log(data)
    }).catch((e)=>{
      // console.log(e)
    })
  }
}
