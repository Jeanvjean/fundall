import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprAllLoansComponent } from './appr-all-loans.component';

describe('ApprAllLoansComponent', () => {
  let component: ApprAllLoansComponent;
  let fixture: ComponentFixture<ApprAllLoansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprAllLoansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprAllLoansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
