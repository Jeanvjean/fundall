import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprAutoLoanComponent } from './appr-auto-loan.component';

describe('ApprAutoLoanComponent', () => {
  let component: ApprAutoLoanComponent;
  let fixture: ComponentFixture<ApprAutoLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprAutoLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprAutoLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
