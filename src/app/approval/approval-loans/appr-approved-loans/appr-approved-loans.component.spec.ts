import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprApprovedLoansComponent } from './appr-approved-loans.component';

describe('ApprApprovedLoansComponent', () => {
  let component: ApprApprovedLoansComponent;
  let fixture: ComponentFixture<ApprApprovedLoansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprApprovedLoansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprApprovedLoansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
