import { Component, OnInit } from '@angular/core';
import { CcService } from '../../../shared/cc.service';
import { LoanService } from '../../../shared/loan.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-appr-approved-loans',
  templateUrl: './appr-approved-loans.component.html',
  styleUrls: ['./appr-approved-loans.component.css']
})
export class ApprApprovedLoansComponent implements OnInit {
  loans = [];
  taken = [];
  repaid = [];
  active = [];
  owed:number = 0;
  p:number = 1;
  firstName:'';
  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private ccService:CcService,
      private loanService:LoanService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.approved()
      this.loan_stats()
  }
  approved(){
      this.spinner.show()
      this.loanService.getAllLoanRequest().toPromise().then((data:any)=>{
        this.spinner.hide()
          // console.log(data)
          for(var i=0; data.result.loans.length; i++){
            if(!data.result.loans[i].disbursed && data.result.loans[i].applicationState == 2){
              // console.log(data.result.loans[i])
              this.loans.push(data.result.loans[i])
              // console.log(this.loans)
            }
          }
      }).catch((e)=>{
        this.spinner.hide()
          // console.log(e)
      })
  }
  loan_stats(){
    this.loanService.getAllLoanRequest().toPromise().then((data:any)=>{
      // this.loans = data.result.loans
      for(var i=0; i < data.result.loans.length; i++){
        if(data.result.loans[i].disbursed && !data.result.loans[i].repaid){
          this.active.push(data.result.loans[i])
          // console.log('active', data.result.loans[i])
        }
        if(data.result.loans[i].disbursed && !data.result.loans[i].repaid){
          this.owed += Number(data.result.loans[i].total_cost)
        }
        if(data.result.loans[i].disbursed && data.result.loans[i].repaid){
          this.repaid.push(data.result.loans[i])
          // console.log('repaid',data.result.loans[i])
        }
        if(data.result.loans[i].disbursed){
          this.taken.push(data.result.loans[i])
          // console.log('taken',data.result.loans[i])
        }
      }
      // console.log(data)
    }).catch((e)=>{
      // console.log(e)
    })
  }
}
