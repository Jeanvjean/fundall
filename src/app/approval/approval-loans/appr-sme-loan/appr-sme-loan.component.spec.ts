import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprSmeLoanComponent } from './appr-sme-loan.component';

describe('ApprSmeLoanComponent', () => {
  let component: ApprSmeLoanComponent;
  let fixture: ComponentFixture<ApprSmeLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprSmeLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprSmeLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
