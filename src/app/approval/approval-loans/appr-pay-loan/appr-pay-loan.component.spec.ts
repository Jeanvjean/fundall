import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprPayLoanComponent } from './appr-pay-loan.component';

describe('ApprPayLoanComponent', () => {
  let component: ApprPayLoanComponent;
  let fixture: ComponentFixture<ApprPayLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprPayLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprPayLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
