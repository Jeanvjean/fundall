import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprRejectedLoansComponent } from './appr-rejected-loans.component';

describe('ApprRejectedLoansComponent', () => {
  let component: ApprRejectedLoansComponent;
  let fixture: ComponentFixture<ApprRejectedLoansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprRejectedLoansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprRejectedLoansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
