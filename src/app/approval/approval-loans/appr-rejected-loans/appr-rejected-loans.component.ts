import { Component, OnInit } from '@angular/core';
import { LoanService } from '../../../shared/loan.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-appr-rejected-loans',
  templateUrl: './appr-rejected-loans.component.html',
  styleUrls: ['./appr-rejected-loans.component.css']
})
export class ApprRejectedLoansComponent implements OnInit {
  loans = [];
  taken = [];
  repaid = [];
  active = [];
  owed:number = 0;
  firstName:'';
  p:number = 1;
  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private loanService:LoanService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.getRepaid()
      this.loan_stats()
  }
  getRepaid(){
      this.spinner.show()
      this.loanService.getAllLoanRequest().toPromise().then((data:any)=>{
        this.spinner.hide()
          for(var i=0; data.result.loans.length; i++){
            if(data.result.loans[i].disbursed && data.result.loans[i].repaid){
              this.loans.push(data.result.loans[i])
              console.log(this.loans)
            }
          }
      }).catch((e)=>{
        this.spinner.hide()
          // console.log(e)
      })
  }
  loan_stats(){
    this.loanService.getAllLoanRequest().toPromise().then((data:any)=>{
      // this.loans = data.result.loans
      for(var i=0; i < data.result.loans.length; i++){
        if(data.result.loans[i].disbursed && !data.result.loans[i].repaid){
          this.active.push(data.result.loans[i])
        }
        if(data.result.loans[i].disbursed && !data.result.loans[i].repaid){
          this.owed += Number(data.result.loans[i].total_cost)
        }
        if(data.result.loans[i].disbursed && data.result.loans[i].repaid){
          this.repaid.push(data.result.loans[i])
        }
        if(data.result.loans[i].disbursed){
          this.taken.push(data.result.loans[i])
        }
      }
      // console.log(data)
    }).catch((e)=>{
      // console.log(e)
    })
  }
}
