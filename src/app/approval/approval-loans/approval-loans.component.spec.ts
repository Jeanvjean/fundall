import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalLoansComponent } from './approval-loans.component';

describe('ApprovalLoansComponent', () => {
  let component: ApprovalLoansComponent;
  let fixture: ComponentFixture<ApprovalLoansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalLoansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalLoansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
