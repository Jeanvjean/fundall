import { Component, OnInit } from '@angular/core';
import { CcService } from '../../../shared/cc.service';
import { LoanService } from '../../../shared/loan.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-appr-requested-loans',
  templateUrl: './appr-requested-loans.component.html',
  styleUrls: ['./appr-requested-loans.component.css']
})
export class ApprRequestedLoansComponent implements OnInit {
  approvalRequests = [];
  p:number = 1;
  taken = [];
  repaid = [];
  active = [];
  isLoading = false;
  owed = 0;
  firstName:'';
  tab = "pending";
  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private ccService:CcService,
      private loanService:LoanService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.requested()
      // this.loan_stats()
  }
  requested(){
      // this.spinner.show()
      this.isLoading = true;
      this.loanService.getLoanRequests(this.tab).toPromise().then((data:any)=>{
        this.spinner.hide()
          // console.log(data)
      this.isLoading = false;
          
         this.approvalRequests = data.result.loans;
      }).catch((e)=>{
        this.spinner.hide()
      this.isLoading = false;
        
          // console.log(e)
      })
  }
  isAdmin(){
    return localStorage.getItem("role") == "1";
  }
  setTab(active){
    this.tab = active;
    this.requested();
    // this.get
  }
  loan_stats(){
    // this.loanService.getAllLoanRequest().toPromise().then((data:any)=>{
    //   // this.loans = data.result.loans

    //       this.active = data.result
     
    //   // console.log(data)
    // })
  }
}
