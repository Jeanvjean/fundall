import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprRequestedLoansComponent } from './appr-requested-loans.component';

describe('ApprRequestedLoansComponent', () => {
  let component: ApprRequestedLoansComponent;
  let fixture: ComponentFixture<ApprRequestedLoansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprRequestedLoansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprRequestedLoansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
