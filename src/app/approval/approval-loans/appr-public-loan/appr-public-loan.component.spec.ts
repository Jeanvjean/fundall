import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprPublicLoanComponent } from './appr-public-loan.component';

describe('ApprPublicLoanComponent', () => {
  let component: ApprPublicLoanComponent;
  let fixture: ComponentFixture<ApprPublicLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprPublicLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprPublicLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
