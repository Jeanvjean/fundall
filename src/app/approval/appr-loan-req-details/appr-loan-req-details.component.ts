import { BASE_API } from './../../shared/api.service';
import { Component, OnInit } from '@angular/core';
import { CcService } from '../../shared/cc.service';
import { LoanService } from '../../shared/loan.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-appr-loan-req-details',
  templateUrl: './appr-loan-req-details.component.html',
  styleUrls: ['./appr-loan-req-details.component.css']
})
export class ApprLoanReqDetailsComponent implements OnInit {
  r = {
    amount: '',
    accountHolder: '',
    duration: '',
    salaryEarned: '',
    interestRate: '',
    loanName: "",
    total_cost: '',
    applicationState: '',
    bankStatement: '',
    goToApproval: false,
    disbursed: false,
    repayments: [],
    comments: [],
    actions: [],
    requirements: [],
    user: {
      firstName: '',
      lastName: '',
      identificationImage: '',
      phone: '',
      email: '',
    }
  };
  isLoading = false;
  file_requirements = [];
  text_requirements = [];
  base_api = BASE_API;
  localStorage: any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toaster: ToasterService,
    private ccService: CcService,
    private loanService: LoanService,
    private spinner: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.localStorage = localStorage
    this.req_details()
  }
  req_details() {
    this.spinner.show()
    var req_id = this.route.snapshot.params['request_id']
    this.loanService.loanRequestDetails(req_id).toPromise().then((data: any) => {
      this.r = data.result;
      this.file_requirements = this.r.requirements.filter(r => r.type == "file");
      this.text_requirements = this.r.requirements.filter(r => r.type != "file");
      this.spinner.hide()
      this.toaster.success(data.message)
      // console.log(data)
    }).catch((e) => {
      this.toaster.error(e.error.message)
      this.spinner.hide()
      // console.log(e)
    })
  }
  reject() {
    var reason = {
      reason: ""
    }
    this.isLoading = true;
    var req_id = this.route.snapshot.params['request_id']
    this.loanService.rejectLoan(req_id, reason).toPromise().then((data: any) => {
      // console.log(data)
      this.toaster.success("Loan rejected successfully");
      history.back();
      this.isLoading = false;
    }).catch((e) => {
      // console.log(e)
    })
  }
  getRequirement(r){
    let x = r.join(",");
    return x[0];
  }
  approve() {
    this.spinner.show()
    this.isLoading = true;
    var req_id = this.route.snapshot.params['request_id']
    this.loanService.approveLoan(req_id).toPromise().then((data: any) => {
      this.toaster.success(data.message)
      history.back();
      // console.log(data)
      this.isLoading = false;
    }).catch((e) => {
      this.toaster.error(e.error.message)
      this.isLoading = false;
      // console.log(e)
    })
  }
}
