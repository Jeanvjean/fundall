import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprLoanReqDetailsComponent } from './appr-loan-req-details.component';

describe('ApprLoanReqDetailsComponent', () => {
  let component: ApprLoanReqDetailsComponent;
  let fixture: ComponentFixture<ApprLoanReqDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprLoanReqDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprLoanReqDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
