import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'approval-nav',
  templateUrl: './approval-nav.component.html',
  styleUrls: ['./approval-nav.component.css']
})
export class ApprovalNavComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  logout(){
      localStorage.clear()
      this.router.navigate(['/signin'])
  }
}
