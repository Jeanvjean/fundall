import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalCommunityComponent } from './approval-community.component';

describe('ApprovalCommunityComponent', () => {
  let component: ApprovalCommunityComponent;
  let fixture: ComponentFixture<ApprovalCommunityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalCommunityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalCommunityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
