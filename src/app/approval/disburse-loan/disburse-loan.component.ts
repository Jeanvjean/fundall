import { Component, OnInit } from '@angular/core';
import { ApprovalService } from '../../shared/approval.service';
import { LoanService } from '../../shared/loan.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-disburse-loan',
  templateUrl: './disburse-loan.component.html',
  styleUrls: ['./disburse-loan.component.css']
})
export class DisburseLoanComponent implements OnInit {
  bank = [];
  disburse = {
    bank:'',
    code:'',
    account_number:'',
    currency:'',
    type:'',
    name:'',

  }
  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private approvalApi:ApprovalService,
      private loanService:LoanService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.banks()
    this.detail()
    // this.p_code()
  }
  p_code(){
    // console.log( 'Bank',this.disburse.bank)
    this.approvalApi.fetch_all_availabale_banks().toPromise().then((data:any)=>{
      for(var i=0; i < data.result.length; i++){
        if(data.result[i].name == this.disburse.bank){
          this.disburse.code = data.result[i].code
          this.disburse.currency = data.result[i].currency
          // console.log(this.disburse)
        }
      }
      // console.log(data)
    }).catch((e)=>{
    this.toaster.error(e.error.message)
      // console.log(e)
    })
  }
  banks(){
    this.approvalApi.fetch_all_availabale_banks().toPromise().then((data:any)=>{
      this.bank = data.result
      // console.log(data)
    }).catch((e)=>{
    this.toaster.error(e.error.message)
      // console.log(e)
    })
  }
  detail(){
    var req_id = this.route.snapshot.params['request_id']
    this.loanService.loanRequestDetails(req_id).toPromise().then((data:any)=>{
      // console.log(data)
    }).catch((e)=>{
      // console.log(e)
      this.toaster.error(e.error.message)
    })
  }
}
