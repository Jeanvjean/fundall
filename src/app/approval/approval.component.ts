import { Component, OnInit } from '@angular/core';
import { CcService } from '../shared/cc.service';
import { LoanService } from '../shared/loan.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
  selector: 'app-approval',
  templateUrl: './approval.component.html',
  styleUrls: ['./approval.component.css']
})
export class ApprovalComponent implements OnInit {
  loans = [];
  p:number = 1;
  disbursed = 0;
  payback = 0;
  interest = 0;
  firstName:'';
  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private ccService:CcService,
      private loanService:LoanService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      var role = localStorage.getItem('role')
      if(role == '1'){
          this.router.navigate(['/admin/dashboard'])
      }else if(role =='2'){
          this.router.navigate(['/approval-manager/dashboard'])
      }else if(role =='3'){
          this.router.navigate(['/disburse-manager/dashboard'])
      }else if(role == '4'){
          this.router.navigate(['/customer-care-agent/dashboard'])
      }else if(role == '5'){
          this.router.navigate(['/u/dashboard'])
      }
      this.all_loan_requests()
      this.loan_stats()
  }
  all_loan_requests(){
    this.loanService.getAllLoanRequest().toPromise().then((data:any)=>{
      this.loans = data.result.loans
      // console.log(data)
    }).catch((e)=>{
      console.log(e)
    })
  }
  loan_stats(){
    this.loanService.getAllLoanRequest().toPromise().then((data:any)=>{
      // this.loans = data.result.loans
      for(var i=0; i < data.result.loans.length; i++){
        if(data.result.loans[i].disbursed && !data.result.loans[i].repaid){
          this.disbursed += Number(data.result.loans[i].amount)
          this.payback += Number(data.result.loans[i].total_cost)
          this.interest += Number(this.payback - this.disbursed)
          // console.log(this.interest)
        }
      }
      // console.log(data)
    }).catch((e)=>{
      console.log(e)
    })
  }

}
