import { CcNavComponent } from './cc/cc-nav/cc-nav.component';
import { UserNavComponent } from './user/user-nav/user-nav.component';
import { AdminNavComponent } from './admin/admin-nav/admin-nav.component';
import { ApprovalNavComponent } from './approval/approval-nav/approval-nav.component';
import { DisburseNavComponent } from './disburse/disburse-nav/disburse-nav.component';
import { Helper } from './shared/helpers';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router,NavigationEnd  } from '@angular/router';
import { NinjaComponent } from './ninja/ninja.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: []
})
export class AppComponent {
  localstorage: any;
  Helper: any;
  constructor(private router: Router) {
    router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        // Show loading indicator
        this.localstorage = localStorage;
        // this.Helper = new Helper();
        // console.log(localStorage.getItem("role"),this.Helper.isUser())
      }

    });
  }

  ngOnInit() {
    this.localstorage = localStorage;
    this.Helper = Helper
  }
}
