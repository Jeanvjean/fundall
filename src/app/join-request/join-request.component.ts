import { BASE_API } from './../shared/api.service';
import { ToasterService } from './../shared/toaster.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CcService } from './../shared/cc.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-join-request',
  templateUrl: './join-request.component.html',
  styleUrls: ['./join-request.component.css']
})
export class JoinRequestComponent implements OnInit {

    community:any;
    communityId:any;
  profile = {image:"/assets/fundal/home/icon/Profile (3) (1).svg"};
  constructor(private api: CcService, private route: ActivatedRoute,
    private spinner: NgxSpinnerService, private toaster: ToasterService,
    private router: Router) { }

  ngOnInit() {
    this.getDetails();
  }
  setProfileImage(img){
    return BASE_API+img;
  }
   viewProfile(member) {
    var id = this.route.snapshot.params['id'];
    this.router.navigate(["cooperative/member/profile/" + id + "/" + member+"/true"]);

  }
   getDetails() {
    this.spinner.show();
    var id = this.route.snapshot.params['id'];
    this.communityId = id;
    this.api.fetchCommunityDetails(id).toPromise().then((data: any) => {
      this.community = data.result.applicants;
      
      this.spinner.hide();
    })
      .catch(err => {
        this.toaster.error("Community details unavailable");
        this.spinner.hide();
      })

  }
}
