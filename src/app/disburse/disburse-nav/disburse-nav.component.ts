import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'disburse-nav',
  templateUrl: './disburse-nav.component.html',
  styleUrls: ['./disburse-nav.component.css']
})
export class DisburseNavComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  logout(){
      localStorage.clear()
      this.router.navigate(['/signin'])
  }
}
