import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisburseNavComponent } from './disburse-nav.component';

describe('DisburseNavComponent', () => {
  let component: DisburseNavComponent;
  let fixture: ComponentFixture<DisburseNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisburseNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisburseNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
