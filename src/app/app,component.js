import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'sidebar': {
    'height': [{ 'unit': 'px', 'value': 800 }, { 'unit': 'string', 'value': '!important' }]
  },
  'right-content': {
    'height': [{ 'unit': 'px', 'value': 800 }, { 'unit': 'string', 'value': '!important' }]
  }
});
