import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from './../shared/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CcService } from './../shared/cc.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-community',
  templateUrl: './admin-community.component.html',
  styleUrls: ['./admin-community.component.css']
})
export class AdminCommunityComponent implements OnInit {
  contribution = 0;
  hide = true;
  isLoading = false;
  constructor(private api: CcService, private route: ActivatedRoute,
    private spinner: NgxSpinnerService, private toaster: ToasterService,
    private router: Router) { }
  community = {
    amount: 0 ,
    _id:"",
    name:'',
    contribution:'',
    applicants:[],
    members:[]
  };
  ngOnInit() {
    this.getDetails();
  }
  toggle() {
    this.hide = !this.hide;
  }
  loanRequests(){
        this.router.navigate(["coopratives/loan/requests/" + this.community._id ]);

  }
  setContribution() {
    if (this.community.amount > 0) {
      this.isLoading = true;
      var id = this.route.snapshot.params['id'];
      this.api.setMonthlyContribution(id, this.community.amount).toPromise().then((data: any) => {
        this.isLoading = false;
        this.toggle();
        this.toaster.success(data.message)
      })
        .catch(err => {
          this.isLoading = false;

          this.toaster.error("An error occureed");
        })
    }
    else {
      this.toaster.error("Set a valid amount")
    }
  }
  isAdmin(members, id) {
    return members.find(m => m.id == id && m.type == "admin");
  }
  viewProfile(member) {
    var id = this.route.snapshot.params['id'];
    // debugger;
    this.router.navigate(["cooperative/member/profile/" + id + "/" + member + "/' '"]);

  }
  viewJoinRequests(id) {
    this.router.navigate(["cooperative/join-request/" + id]);
  }
  getDetails() {
    this.spinner.show();
    var id = this.route.snapshot.params['id'];
    this.api.fetchCommunityDetails(id).toPromise().then((data: any) => {
      this.community = data.result;
      this.spinner.hide();
    })
      .catch(err => {
        this.toaster.error("Community details unavailable");
        this.spinner.hide();
      })

  }
}
