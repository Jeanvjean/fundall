import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'member-card',
  templateUrl: './member-card.component.html',
  styleUrls: ['./member-card.component.css']
})
export class MemberCardComponent implements OnInit {
  @Input() profile = {
    firstName:'',
    lastName:'',
    email:'',
    phone:'',
    image:''
  };
  constructor() { }

  ngOnInit() {
  }

}
