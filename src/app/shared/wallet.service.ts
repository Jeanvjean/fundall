import { BASE_API } from './api.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class WalletService {
    baseUrl = BASE_API;

    constructor(private http: HttpClient) { }
    //add card
    addCard(data) {
        return this.http.post(`${this.baseUrl}api/wallet/add-card`, data, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    // add account
    addAccount(data) {
        return this.http.post(`${this.baseUrl}api/wallet/add-account`, data, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    //fund wallet
    fundWallet(data) {
        return this.http.post(`${this.baseUrl}api/wallet/fund-wallet`, data, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    submitToken(data) {
        return this.http.post(`${this.baseUrl}api/wallet/submit-otp`, data, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    bankTransactions() {
        return this.http.get(`${this.baseUrl}api/wallet/fetch-all-banks`)
    }
    getWalletBallance() {
        return this.http.get(`${this.baseUrl}api/wallet/fetch-wallet`, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    accounts() {
        return this.http.get(`${this.baseUrl}api/wallet/fetch-my-accounts`, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    my_cards() {
        return this.http.get(`${this.baseUrl}api/wallet/fetch-my-cards`, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    createTransfer(_id, data) {
        return this.http.post(`${this.baseUrl}api/wallet/create-transfer-recipient/${_id}`, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    transferToBank(data) {
        return this.http.post(`${this.baseUrl}api/wallet/transfer-to-account`, data, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
}
