import { BASE_API } from './api.service';
import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApprovalService {
  // baseUrl="http://178.128.34.164:3001";
  baseUrl=BASE_API;
  constructor(
      private http:HttpClient) { }

  create(data){
      return this.http.post(`${this.baseUrl}/api/admin/create/apprmanagers`,data,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  getAll(page){
      return this.http.get(`${this.baseUrl}/api/admin/getall/apprmanagers?page=${page}`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  fetch_all_availabale_banks(){
    return this.http.get(`${this.baseUrl}/api/wallet/fetch-all-banks`,{
        headers:new HttpHeaders({'token':localStorage.getItem('token')})
    })
  }

}
