import { BASE_API } from './api.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class CcService {
    baseUrl = BASE_API;


    constructor(
        private http: HttpClient) { }

    create(data) {
        return this.http.post(`${this.baseUrl}api/admin/create/customercare`, data, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    joinCommunity(id) {
        return this.http.put(`${this.baseUrl}api/cooperative/${id}/apply`, { applicant: localStorage.getItem("user_id") }, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    processJoinRequest(community_id, user_id, request) {
        return this.http.put(`${this.baseUrl}api/cooperative/${community_id}/${request}`, { applicant: user_id }, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    getLoanDetails(id) {
        return this.http.get(`${this.baseUrl}api/cooperative/loan/${id}`, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    processLoan(id, status) {
        return this.http.put(`${this.baseUrl}api/cooperative/update_loan/${id}`, { status: status }, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    setMonthlyContribution(id, contribution) {
        return this.http.put(`${this.baseUrl}api/cooperative/create/${id}`, { amount: contribution }, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    applyCooperativeLoan(loan) {
        return this.http.post(`${this.baseUrl}api/cooperative/loan`, loan, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    getAll(page) {
        return this.http.get(`${this.baseUrl}api/admin/getall/customercare?page=${page}`, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    getCooperativeLoans(community, user, filter) {
        filter = !filter ? null : filter;
        user = !user ? null : user;

        return this.http.put(`${this.baseUrl}api/cooperative/loan/`, { filter: filter, user: user, community: community }, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    //due payments
    getDuePayments() {
        return this.http.get(`${this.baseUrl}api/cc/due-payments`, {

            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    // send payment reminder
    duePaymentReminder(data) {
        return this.http.put(`${this.baseUrl}api/cc/send-due-message`, data, {

            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    // fetch all women society
    women_society() {
        return this.http.get(`${this.baseUrl}api/cc/get-women-societies`, {

            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    // fetch all women society
    fetch_all_communities(type) {
        return this.http.get(`${this.baseUrl}api/cc/get-cooperatives/${type}`, {

            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    fetch_all_open_complains() {
        return this.http.get(`${this.baseUrl}api/cc/complaints`, {

            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    fetch_all_resolved() {
        return this.http.get(`${this.baseUrl}api/cc/resolved-complaints`, {

            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    mark_complaint_asresolved(complaint_id) {
        return this.http.get(`${this.baseUrl}api/cc/mark-as-resolved/${complaint_id}`, {

            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }

    ///*attecntion needed*///
    get_all_pending_loan() {
        return this.http.get(`${this.baseUrl}api/loan/pending/loans`, {

            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    ///*attecntion needed*///

    get_all_approved_loans() {
        return this.http.get(`${this.baseUrl}api/loan/approved/loans`, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    get_all_rejected_loans() {
        return this.http.get(`${this.baseUrl}api/loan/rejected/loans`, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    fetch_all_user_loan(id) {
        return this.http.get(`${this.baseUrl}api/cc/loans/${id}`, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    fetchCommunityDetails(id) {
        return this.http.get(`${this.baseUrl}api/cooperative/${id}`, {
            headers: new HttpHeaders({ 'token': localStorage.getItem('token') })
        })
    }
    fetch_all_user_coopratives(user_id){
    return this.http.get(`${this.baseUrl}api/cc/get-cooperatives/${user_id}`,{
        headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
    }
    fetch_all_user_women_group(user_id){
    return this.http.get(`${this.baseUrl}api/cc/get-women-societies/${user_id}`,{
        headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
    }
}
