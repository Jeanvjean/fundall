import { BASE_API } from './api.service';
import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CooprativeService {
 baseUrl = BASE_API

  constructor(private http:HttpClient) { }
  //create user cooprative
  create(fd){
      return this.http.post(`${this.baseUrl}api/cooperative/create`,fd,{
          headers: new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //create woment in trade society
  create_wit(fd){
        return this.http.post(`${this.baseUrl}api/cooperative/create-women-community`,fd,{
            headers: new HttpHeaders({'token':localStorage.getItem('token')})
        })
    }
 // single cooprative details
  single(id){
      return this.http.get(`${this.baseUrl}api/cooperative/${id}`,{
          headers: new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  getAll_wit(){//get all women society a user belongs to

    return this.http.get(`${this.baseUrl}api/cooperative/my-women-communities`,{
          headers: new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //view user coopratives
  userCoopratives(){
      return this.http.get(`${this.baseUrl}api/cooperative/my-cooperatives`,{
            headers: new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }

  allCoopratives(){
      return this.http.get(`${this.baseUrl}api/cooperative/my-cooperatives`,{
          headers: new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //user fund wallet
  contributeToWallet(user_id){
      return this.http.post(`${this.baseUrl}api/cooperative/${user_id}/contribute`,{
          headers: new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //view cooprative wallet
  view_wallet(wallet_id){
      return this.http.get(`${this.baseUrl}api/cooperative/wallet/${wallet_id}`,{
          headers: new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //join a cooprative
  joinCoop(coop_id,appl_id){
      return this.http.put(`${this.baseUrl}api/cooperative/${coop_id}/apply`,appl_id,{
          headers: new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //approve join request
  approveRequest(coop_id,appl_id){
      return this.http.put(`${this.baseUrl}api/cooperative/${coop_id}/approve`,appl_id,{
          headers: new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //reject request
  rejectRequest(coop_id,appl_id){
      return this.http.put(`${this.baseUrl}api/cooperative/${coop_id}/reject`,appl_id,{
          headers: new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
}
