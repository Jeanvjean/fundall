import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(users: any, firstname: any,lastname: any) {
    //check if search term is undefind
    if(users && users.length) {
      return users.filter(user=>{
        if(firstname && user.firstName.toLowerCase().indexOf(firstname.toLowerCase()) === -1){
          return false
        }
        if(lastname && user.lastName.toLowerCase().indexOf(lastname.toLowerCase()) === -1){
          return false
        }
        return true
      })
    }
  }
  // transform(users: any, term: any): any {
  //   //check if search term is undefind
  //   if(term === undefined) return users
  //   //return the updated array
  //   return users.filter(function(user){
  //     return user.firstName.toLowerCase().includes(term.toLowerCase())
  //   })
  // }

}
