import { BASE_API } from './api.service';
import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DisburseService {
    baseUrl=BASE_API


  constructor(
      private http:HttpClient) { }

      create(data){
          return this.http.post(`${this.baseUrl}api/admin/create/disbmanagers`,data,{
              headers:new HttpHeaders({'token':localStorage.getItem('token')})
          })
      }
      getAll(page){
          return this.http.get(`${this.baseUrl}api/admin/getall/disbmanagers?page=${page}`,{
              headers:new HttpHeaders({'token':localStorage.getItem('token')})
          })
      }
}
