import { Component, OnInit } from '@angular/core';
import{ Router } from '@angular/router';

@Component({
  selector: 'app-side-nav-bar',
  templateUrl: './side-nav-bar.component.html',
  styleUrls: ['./side-nav-bar.component.css']
})
export class SideNavBarComponent implements OnInit {
  role:any='';

  constructor(private router:Router) { }
  ngOnInit() {
    this.role = localStorage.getItem('role')
  }
logout(){
  localStorage.clear()
  this.router.navigate(['/signin'])
}
}
