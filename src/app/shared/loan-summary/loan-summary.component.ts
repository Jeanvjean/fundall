import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'loan-summary',
  templateUrl: './loan-summary.component.html',
  styleUrls: ['./loan-summary.component.css']
})
export class LoanSummaryComponent implements OnInit {
  @Input() summary = {};
  constructor() { }

  ngOnInit() {
  }

}
