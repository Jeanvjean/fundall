import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lFilter'
})
export class LFilterPipe implements PipeTransform {

  transform(listings: any, company:any, name:any) {
    //check if search term is undefind
    if(listings && listings.length) {
      return listings.filter(listing=>{
        if(company && listing.company.toLowerCase().indexOf(company.toLowerCase()) === -1){
          return false
        }
        if(name && listing.name.toLowerCase().indexOf(name.toLowerCase()) === -1){
          return false
        }
        return true
      })
    }
  }

}
