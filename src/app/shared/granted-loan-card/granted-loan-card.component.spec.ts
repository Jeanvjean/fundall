import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrantedLoanCardComponent } from './granted-loan-card.component';

describe('GrantedLoanCardComponent', () => {
  let component: GrantedLoanCardComponent;
  let fixture: ComponentFixture<GrantedLoanCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrantedLoanCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrantedLoanCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
