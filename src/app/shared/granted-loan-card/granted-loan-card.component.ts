import { Component, OnInit,Input} from '@angular/core';

@Component({
  selector: 'granted-loan-card',
  templateUrl: './granted-loan-card.component.html',
  styleUrls: ['./granted-loan-card.component.css']
})
export class GrantedLoanCardComponent implements OnInit {

  @Input() loan = {
    amount:'',
    duration:'',
    dateApproved:'',
    monthlyRepayment:'',
    repaid:false,
    
  };
  status = {0:"",1:"",2:"",3:"",4:""};
  constructor() { }

  ngOnInit() {
  }


}
