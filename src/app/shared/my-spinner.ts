import { Component,Input } from '@angular/core';

@Component({
  selector: 'spinner',
  template: '<i *ngIf="isLoading" class="fa fa-spinner fa-spin"></i>',
  
})
export class SpinnerComponent {
@Input() isLoading = false;

}