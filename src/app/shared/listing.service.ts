import { BASE_API } from './api.service';
import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListingService {
 baseUrl=BASE_API;

  constructor(private http:HttpClient) { }
  create(data){
      return this.http.post(`${this.baseUrl}api/listing/create`,data,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  viewListing(list_id){
      return this.http.get(`${this.baseUrl}api/listing/${list_id}`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  getAll(){
      return this.http.get(`${this.baseUrl}api/listing/get/all`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }

  approve(list_id){
      return this.http.get(`${this.baseUrl}api/listing/${list_id}/approve`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  reject(list_id){
      return this.http.get(`${this.baseUrl}api/listing/${list_id}/reject`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  delete(list_id){
      return this.http.delete(`${this.baseUrl}api/listing/${list_id}/delete`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  pending_listing(page=1){
      return this.http.get(`${this.baseUrl}api/listing/get/pending?page=`+page)
  }
  running_listing(){
      return this.http.get(`${this.baseUrl}api/listing/get/approved`)
  }
  rejected_listing(){
      return this.http.get(`${this.baseUrl}api/listing/get/rejected`)
  }
  suspend_listing(id){
      return this.http.get(`${this.baseUrl}api/listing/${id}/suspend`)
  }
  block_listing(id){
      return this.http.get(`${this.baseUrl}api/listing/${id}/block`)
  }
  activate_listing(id){

      return this.http.get(`${this.baseUrl}api/listing/${id}/activate`)
  }
}
