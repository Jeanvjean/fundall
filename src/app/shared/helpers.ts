export class Helper{
    // var role = localStorage.getItem("role");
    isApprovalManager(){
        return  parseInt(localStorage.getItem("role")) == 2;
    }
    isCustomerCare(){
        return  parseInt(localStorage.getItem("role")) == 4;

    }
    isUser(){
        return  parseInt(localStorage.getItem("role")) == 5;

    }
    isDisbursementManager(){
        return  parseInt(localStorage.getItem("role")) == 3;

    }
    isAdmin(){
        return  parseInt(localStorage.getItem("role")) == 1;

    }
}
