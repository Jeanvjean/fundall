import { Component, OnInit,Input } from '@angular/core';
@Component({
  selector: 'app-loan-card',
  templateUrl: './loan-card.component.html',
  styleUrls: ['./loan-card.component.css']
})
export class LoanCardComponent implements OnInit {

  @Input() loan = {
    productName:'',
    approvedDate:'',
    amount:'',
    monthly_repayments:''
  };
  status = {0:"",1:"",2:"",3:"",4:""};
  constructor() { }

  ngOnInit() {
  }


}
