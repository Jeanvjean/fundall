import { BASE_API } from './api.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    baseUrl = BASE_API;

  // baseUrl="http://178.128.34.164:3001";
  constructor(private http: HttpClient) { }

  signUp(user) {
      return this.http.post(`${this.baseUrl}api/users/signup`,user)
  }
  loginToSaveAll(saveall){
      return this.http.post(`http://test.natterbase.com:3002/login`,saveall)
  }
  isLinked(){
     return  this.http.get(`http://test.natterbase.com:3002/link_getfund/`+localStorage.getItem("user_id"))
  }
  confirmAccount(token){
      return this.http.post(`http://test.natterbase.com:3002/link_getfund`, token,
          {

            headers: new HttpHeaders({ 'x-access-token': token })
          })
  }
  //login admin and users
  login(user) {
      return this.http.post(`${this.baseUrl}api/users/login`,user)
  }
  getAll(range,filterby,page){
      let url = `${this.baseUrl}api/admin/getall/users`;
      if(range.length > 0){
          url = url+"?start_date="+range[0]+"?end_date="+range[1];
      }
      if(filterby){
          url = url+"?filter="+filterby
      }
      url = url+"?page="+page;
      return this.http.get(url,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  download(role){
      let url = `${this.baseUrl}api/admin/getallbyrole/${role}`;
      return this.http.get(url,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //get adminUser details
  getOne(id){
      return this.http.get(`${this.baseUrl}api/admin/${id}/details`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //get single user details
  userDetails(user_id){
      return this.http.get(`${this.baseUrl}api/users/${user_id}`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //update user
    updateUser(user_id,fd){
      return this.http.put(`${this.baseUrl}api/users/${user_id}`,fd,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
    }
  //deleteAdminUser user only admin
  deleteUser(user_id){
      return this.http.delete(`${this.baseUrl}api/admin/${user_id}/delete`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  resendCode(email){
      return this.http.post(`${this.baseUrl}api/users/resend-confirmation`,email)
  }
  delete(user_id){
      return this.http.delete(`${this.baseUrl}api/users/${user_id}`)
  }
  verifyUser(token){
      return this.http.get(`${this.baseUrl}api/users/verify/${token}`)
  }
  passwordReset(data){
      return this.http.post(`${this.baseUrl}api/users/recover-password`,data)
  }
  changePassword(data){
      return this.http.post(`${this.baseUrl}api/users/save-new-password`,data)
  }
  disable(id,data){
      return this.http.post(`${this.baseUrl}api/users/toggle_user`,{id:id},{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  toggle(id){
      return this.http.post(`${this.baseUrl}api/users/toggle_user`,{id:id},{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  due_payment(){
      return this.http.get(`${this.baseUrl}api/loan/fetch/due-payments`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  fetch_all_wit(id){
      return this.http.get(`${this.baseUrl}api/cc/get-women-societies/${id}`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  get_all_user_cooprative(id){
      return this.http.get(`${this.baseUrl}api/cc/get-cooperatives/${id}`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  stats(){
     return this.http.get(`${this.baseUrl}api/admin/general`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  updateUserDetails(user){
         return this.http.put(`${this.baseUrl}api/cooperative/update_user`,user,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
}
