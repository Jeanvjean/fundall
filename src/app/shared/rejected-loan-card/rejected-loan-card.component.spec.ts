import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectedLoanCardComponent } from './rejected-loan-card.component';

describe('RejectedLoanCardComponent', () => {
  let component: RejectedLoanCardComponent;
  let fixture: ComponentFixture<RejectedLoanCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectedLoanCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectedLoanCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
