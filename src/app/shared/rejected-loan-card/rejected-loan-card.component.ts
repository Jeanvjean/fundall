import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'rejected-loan',
  templateUrl: './rejected-loan-card.component.html',
  styleUrls: ['./rejected-loan-card.component.css']
})
export class RejectedLoanCardComponent implements OnInit {
  @Input() loan = {
    loanName:'',
    amount:'',
    productName:'',
    approvedDate:'',
    monthly_repayments:''
  }
  constructor() { }

  ngOnInit() {
  }

}
