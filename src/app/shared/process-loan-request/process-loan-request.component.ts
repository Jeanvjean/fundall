import { MemberCardComponent } from './../member-card/member-card.component';
import { BASE_API } from './../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from './../toaster.service';
import { CcService } from './../cc.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-process-loan-request',
  templateUrl: './process-loan-request.component.html',
  styleUrls: ['./process-loan-request.component.css']
})
export class ProcessLoanRequestComponent implements OnInit {
  community: any;
  profile: any;
  loan = {
    status: 0 ,
    amount:'',
    duration:'',
    totalCost:'',
    monthlyRepayment:''
  };
  loanId: any;
  isLoading = false;
  constructor(private api: CcService, private route: ActivatedRoute,
    private spinner: NgxSpinnerService, private toaster: ToasterService,
    private router: Router) { }



  ngOnInit() {
    this.getDetails();
  }
  processLoan(status) {
    this.isLoading = true;
    this.api.processLoan(this.loanId, status).toPromise().then((data: any) => {
      if (data.success) {
        var msg = status == 1 ? "Approved" : "Rejected"
        this.toaster.success("Loan Approved " + msg);
        this.loan.status = status;
      }
      else {
        this.toaster.error("Unable to process Request");
      }
      this.isLoading = false;
    })
      .catch((err: any) => {
        this.isLoading = false;
        this.toaster.error("An error occured.");
      })
  }
  getDetails() {
    this.spinner.show();
    var id = this.route.snapshot.params['community'];
    this.loanId = this.route.snapshot.params['loan'];
    var member = this.route.snapshot.params['user'];
    this.api.fetchCommunityDetails(id).toPromise().then((data: any) => {
      this.community = data.result;

      this.profile = this.community.members.find(m => m._id == member);

      this.profile.image = BASE_API + this.profile.image
      this.profile.identificationImage = BASE_API + this.profile.identificationImage
      this.spinner.hide();
    })
      .catch(err => {
        this.toaster.error("Community details unavailable");
        this.spinner.hide();
      })
    this.spinner.show();
    this.api.getLoanDetails(this.loanId).toPromise().then((data: any) => {
      this.loan = data.result;
      this.spinner.hide();
    })
      .catch(err => {
        this.toaster.error("Loan Request details unavailable");
        this.spinner.hide();
      })

  }
}
