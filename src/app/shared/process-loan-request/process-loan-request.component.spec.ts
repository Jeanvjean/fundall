import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessLoanRequestComponent } from './process-loan-request.component';

describe('ProcessLoanRequestComponent', () => {
  let component: ProcessLoanRequestComponent;
  let fixture: ComponentFixture<ProcessLoanRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessLoanRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessLoanRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
