import { BASE_API } from './api.service';
import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoanService {
    baseUrl=BASE_API;



  constructor(private http:HttpClient) { }
//create Loan Admin only
  create(fd){
      return this.http.post(`${this.baseUrl}api/product/create`,fd,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  getPending(){
       return this.http.get(`${this.baseUrl}api/loan/pending/loans`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //get all loans admin and users
  getLoan(){

      return this.http.get(`${this.baseUrl}api/product/getall/products`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //creat coperative
  createCoperative(data){
      return this.http.post(`${this.baseUrl}api/cooperative/create`,data,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //get due lons
  getDueLoans(){
         return this.http.get(`${this.baseUrl}api/loan/fetch/due-payments`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //updateLoan Admin only
  updateLoan(fd,loan_id){
      return this.http.put(`${this.baseUrl}api/product/${loan_id}/update`,fd,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //get Loan details admin and users
  loanDetail(loan_id){
      return this.http.get(`${this.baseUrl}api/product/${loan_id}`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //delete Loan admin only
  deleteLoan(loan_id){
      return this.http.delete(`${this.baseUrl}api/product/${loan_id}/delete`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //request Loan users only
  requestLoan(fd,loan_id){
      return this.http.post(`${this.baseUrl}api/loan/${loan_id}/apply`,fd,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  approveLoan(id){
      return this.http.put(`${this.baseUrl}api/loan/${id}/approve`,{},{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //reject loan request admin only

  //approve loan admin only
//  di

  rejectLoan(req_id,reason){
      return this.http.put(`${this.baseUrl}api/loan/${req_id}/reject`,reason,
          {headers:new HttpHeaders({'token':localStorage.getItem('token')})})
  }
getLoanRequests(query){
  let url  = `${this.baseUrl}api/loan/getall/loans`;
      if(query){
          url = url+"?status="+query;
      }
      return this.http.get(url,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
}
  getAllLoanRequest(){
      let url  = `${this.baseUrl}api/loan/getall/loans`;
       
      return this.http.get(url,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  loanRequestDetails(req_id){

      return this.http.get(`${this.baseUrl}api/loan/${req_id}`,{
          headers:new HttpHeaders({'token':localStorage.getItem('token')})
      })
  }
  //===============loan request =============//
      sme(fd){
        return this.http.post(`${this.baseUrl}api/loan/sme/apply`,fd,{
            headers:new HttpHeaders({'token':localStorage.getItem('token')})
        })
    }
      apply(fd){
        return this.http.post(`${this.baseUrl}api/loan/apply`,fd,{
            headers:new HttpHeaders({'token':localStorage.getItem('token')})
        })
    }
      salary(fd){
        return this.http.post(`${this.baseUrl}api/loan/salary-advance/apply`,fd,{
            headers:new HttpHeaders({'token':localStorage.getItem('token')})
        })
    }
      pay(fd){
        return this.http.post(`${this.baseUrl}api/loan/pay-loan/apply`,fd,{
            headers:new HttpHeaders({'token':localStorage.getItem('token')})
        })
    }
      auto(fd){
        return this.http.post(`${this.baseUrl}api/loan/auto-finance/apply`,fd,{
            headers:new HttpHeaders({'token':localStorage.getItem('token')})
        })
    }
      public(fd){
        return this.http.post(`${this.baseUrl}api/loan/public-sector/apply`,fd,{
            headers:new HttpHeaders({'token':localStorage.getItem('token')})
        })
    }
    //user loan history
    getLoanHistory(user_id){
        return this.http.get(`${this.baseUrl}api/loan/get-loans-history/${user_id}`,{
            headers:new HttpHeaders({'token':localStorage.getItem('token')})
        })
    }
    //fetch loan images
    loanImage(){
        return this.http.get(`${this.baseUrl}api/product/get-all-images`)
    }
    //fetch loan icons
    loanIcons(){
        return this.http.get(`${this.baseUrl}api/product/get-all-icons`)
    }
    //upload loan images
    add_loan_image(fd){
        return this.http.post(`${this.baseUrl}api/product/upload-product-image`,fd)
    }
    repay_loan_from_wallet(id){
        return this.http.get(`${this.baseUrl}api/loan/${id}/repay`)
    }
    loan_summary(){
        return this.http.get(`${this.baseUrl}api/loan/fetch/loan-summary`,{
            headers:new HttpHeaders({'token':localStorage.getItem('token')})
        })
    }
}
