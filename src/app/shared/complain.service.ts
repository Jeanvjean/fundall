import { BASE_API } from './api.service';
import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ComplainService {
    baseUrl= BASE_API;
  constructor(private http:HttpClient) { }
//users only
  makeComplain(data){
      return this.http.post(`${this.baseUrl}api/users/send-complaint`,data,{
          headers: new HttpHeaders ({'token': localStorage.getItem('token')})
      })
  }
  //admin only
  getAllComplains(){
     return this.http.get(`${this.baseUrl}api/admin/getall/complaints`,{
         headers: new HttpHeaders ({'token': localStorage.getItem('token')})
     })
  }
  //admin only
      respondToComplain(comp_id,data){
        return this.http.post(`${this.baseUrl}api/admin/complaint/${comp_id}/respond`,data,{
            headers: new HttpHeaders ({'token': localStorage.getItem('token')})
        })
    }
    read(comp_id){
        return this.http.get(`${this.baseUrl}api/cc/get/complaint/${comp_id}`,{
            headers: new HttpHeaders ({'token': localStorage.getItem('token')})
        })
    }
    readResponse(comp_id){
        return this.http.get(`${this.baseUrl}.../${comp_id}`,{
            headers: new HttpHeaders ({'token': localStorage.getItem('token')})
        })
    }
}
