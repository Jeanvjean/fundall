import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NinjaService {

  constructor(private http:HttpClient) { }

  addNinja(fd){
      return this.http.post('http://localhost:4000/ninja',fd,{
          headers:new HttpHeaders({'Authorization': localStorage.getItem('userToken')})
      })
  }
  deleteNinja(id){
      return this.http.delete('http://localhost:4000/ninja/'+id,{
          headers:new HttpHeaders({'Authorization': localStorage.getItem('userToken')})
      })
  }
  getNinjas(){
      return this.http.get('http://localhost:4000/ninja')
  }
  getNinja(ninjaId){
      return this.http.get('http://localhost:4000/ninja/'+ninjaId,{
          headers:new HttpHeaders({'Authorization': localStorage.getItem('userToken')})
      })
  }
  updateNinja(ninjaId,fd){
      return this.http.put('http://localhost:4000/ninja/'+ ninjaId,fd,{
          headers:new HttpHeaders({'Authorization': localStorage.getItem('userToken')})
      })
  }
}
