import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'loanFilter'
})
export class LoanFilterPipe implements PipeTransform {

  transform(loans: any, firstName: any): any {
    if(firstName === undefined) return loans
    return loans.filter(function(loan){
      return loan.user.firstName.toLowerCase().includes(firstName.toLowerCase())
    })

  }

}
