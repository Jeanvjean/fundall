import { Component, OnInit,Input} from '@angular/core';

@Component({
  selector: 'pending-loan',
  templateUrl: './pending-loan-card.component.html',
  styleUrls: ['./pending-loan-card.component.css']
})
export class PendingLoanCardComponent implements OnInit {
  @Input() loan = {
    amount:'',
    disbursed:false,
    productName:'',
    approvedDate:'',
  };
  constructor() { }

  ngOnInit() {
  }

}
