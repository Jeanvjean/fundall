import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingLoanCardComponent } from './pending-loan-card.component';

describe('PendingLoanCardComponent', () => {
  let component: PendingLoanCardComponent;
  let fixture: ComponentFixture<PendingLoanCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingLoanCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingLoanCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
