import { MemberCardComponent } from './../shared/member-card/member-card.component';
import { BASE_API } from './../shared/api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToasterService } from './../shared/toaster.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CcService } from './../shared/cc.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-member-profile',
  templateUrl: './member-profile.component.html',
  styleUrls: ['./member-profile.component.css']
})
export class MemberProfileComponent implements OnInit {
  community: any;
  showReject = false;
  profile = { image: "/assets/fundal/home/icon/Profile (3) (1).svg", identificationImage: "/assets/fundal/home/icon/Profile (3) (1).svg" };
  constructor(private api: CcService, private route: ActivatedRoute,
    private spinner: NgxSpinnerService, private toaster: ToasterService,
    private router: Router) { }

  ngOnInit() {
    if (this.route.snapshot.params['process'] == "true") {
      this.showReject = true;
    }
    this.getDetails();
  }
  processRequest(request) {
    var id = this.route.snapshot.params['id'];
    var member = this.route.snapshot.params['member'];
    this.spinner.show();
    this.api.processJoinRequest(id, member, request).toPromise().then((data: any) => {
      if (data) {
        this.toaster.success(request + " successfully");
        this.spinner.hide();
        this.router.navigate(['/admin-community/' + id]);
      }
    })
      .catch(err => {
        this.toaster.error("An error occured");
        this.spinner.hide();
      })

  }
  getDetails() {
    this.spinner.show();
    var id = this.route.snapshot.params['id'];
    var member = this.route.snapshot.params['member'];
    this.api.fetchCommunityDetails(id).toPromise().then((data: any) => {
      this.community = data.result;
      if (this.showReject) {
        this.profile = this.community.applicants.find(m => m._id == member);
      } else {
        this.profile = this.community.members.find(m => m._id == member);
      }
      this.profile.image = BASE_API + this.profile.image
      this.profile.identificationImage = BASE_API + this.profile.identificationImage
      this.spinner.hide();
    })
      .catch(err => {
        this.toaster.error("Community details unavailable");
        this.spinner.hide();
      })

  }
}
