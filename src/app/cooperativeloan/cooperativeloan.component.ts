import { PendingLoanComponentComponent } from './../pending-loan-component/pending-loan-component.component';
import { GrantedLoanCardComponent } from './../shared/granted-loan-card/granted-loan-card.component';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToasterService } from './../shared/toaster.service';
import { CcService } from './../shared/cc.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-cooperativeloan',
  templateUrl: './cooperativeloan.component.html',
  styleUrls: ['./cooperativeloan.component.css']
})
export class CooperativeloanComponent implements OnInit {
  user = "";
  filter = 1;
  loans = [];
  community = "";
  currentTab = "granted"
   constructor(private api: CcService, private route: ActivatedRoute,
    private spinner: NgxSpinnerService, private toaster: ToasterService,
    private router: Router) { }

  ngOnInit() {
   this.community = this.route.snapshot.params['community'];
   this.user = this.route.snapshot.params['user'];
   this.getLoans();
  }

  setTab(tab){

    if(tab == "granted"){
      this.filter = 1;
    }
    else if(tab == "pending"){
      this.filter = 0;
    }
    else if( tab == "rejected"){
      this.filter = 2;
    }
    this.loans = [];
    this.getLoans();
    this.currentTab = tab;
  
  }
  getLoans(){
    // if(this.)
    this.spinner.show();
    this.api.getCooperativeLoans(this.community,this.user,this.filter).toPromise().then((data:any)=>{
       this.loans = data.result;
       this.spinner.hide();
    })
    .catch(err=>{
      this.toaster.error("An error occured");
      this.spinner.hide();
    })
  }

}
