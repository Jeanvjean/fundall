import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CooperativeloanComponent } from './cooperativeloan.component';

describe('CooperativeloanComponent', () => {
  let component: CooperativeloanComponent;
  let fixture: ComponentFixture<CooperativeloanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CooperativeloanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CooperativeloanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
