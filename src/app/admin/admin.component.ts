import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from './admin-nav/admin-nav.component';
import { UserService } from '../shared/user.service'

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
    stats = {
        customercare:'',
        apprmanager:'',
        disbmanager:'',
        users:'',
        loans:''
    };
  constructor(private userService:UserService) { }

  ngOnInit() {
      this.stat()
  }
  stat(){
      this.userService.stats().toPromise().then((data:any)=>{
          this.stats = data.stats
          console.log(data)
      }).catch((e)=>{
          console.log(e)
      })
  }
}
