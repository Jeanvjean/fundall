import { CcService } from './../../shared/cc.service';
import { DisburseService } from './../../shared/disburse.service';
import { ApprovalService } from './../../shared/approval.service';
import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { UserService } from '../../shared/user.service';
import { FilterPipe } from '../../shared/filter.pipe';
import { Router, ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import * as moment from 'moment';
import downloadCsv from 'download-csv'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { en_US, zh_CN, NzI18nService } from 'ng-zorro-antd';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css'],
  styles: [`
    nz-date-picker, nz-month-picker, nz-year-picker, nz-range-picker, nz-week-picker {
    }
  ` ]
  // pipes: [FilterPipe]
})
export class UserManagementComponent implements OnInit {
  date = null; // new Date();
  dateRange = []; // [ new Date(), addDays(new Date(), 3) ];
  isEnglish = false;
  users: any = '';
  isLoading = false;
  page: number = 1;
  role: number = 1;
  isDownloading = false;
  filterBy = "";
  firstname: any = '';
  lastname: any = '';
  query: any = '';
  type: any = '';
  title: any = '';
  range = [];
  key: any = 'managers';

  constructor(
    private userService: UserService,
    private router: Router,
    private toaster: ToasterService,
    private spinner: Ng4LoadingSpinnerService,
    private route: ActivatedRoute,
    private approvalService: ApprovalService,
    private disburseService: DisburseService,
    private ccService: CcService,
    private i18n: NzI18nService) { }

  ngOnInit() {
    this.getUsers()
  }
  getUsers() {
    // this.users = [];
    let toCall = this.userService.getAll(this.range, this.filterBy, this.page);
    this.type = this.route.snapshot.params["type"];
    if (this.key == "users") {
      if (this.range.length > 0) {
        if (this.filterBy == "") {
          this.toaster.warning("Select a field to filter by!");
          return;
        }
      }
    }
    switch (this.type) {
      case "user":
        this.title = "Users";
        this.key = "users";
        this.role = 5;
        toCall = this.userService.getAll(this.range, this.filterBy, this.page)
        break;
      case "am":
        this.title = "Approval Manager";
        toCall = this.approvalService.getAll(this.page);
        this.key = "managers";
        this.role = 2;
        
        break;
      case "dm":
        this.title = "Disbursement Manager";
        toCall = this.disburseService.getAll(this.page);
        this.role = 3;
        
        break;
      case "cc":
        this.title = "Customer Care Agent";
        toCall = this.ccService.getAll(this.page);
        this.role = 4;
        
        break;
    }
    this.isLoading = true;
    toCall.toPromise().then((data: any) => {
      this.users = data.result[this.key];
      this.isLoading = false;
    }).catch((e) => {
      this.isLoading = false;
      this.toaster.error(e.error.message)
      console.log(e)
    })
  }
  createUser() {
    let url = "/admin/create/user/";
    if (this.type == "cc") {
      url = url + "1";
    }
    else if (this.type == "dm") {
      url = url + "3"
    }
    else if (this.type == "am") {
      url = url + "2"
    }
    this.router.navigate([url]);
  }
  onChange(result: Date): void {
    this.range[0] = new Date(result[0]).toISOString();
    this.range[1] = new Date(result[1]).toISOString();

  }
  download(){
    this.isDownloading = true;
    this.userService.download(this.role).toPromise().then((res:any)=>{
      res = res.result;
      if(res[this.key]){
       console.log("dsdsd",downloadCsv(res[this.key],{firstName:"Firstname",lastName:"Lastname"}));
      }
      this.isDownloading = false;
    })
    .catch(err=>{
      this.isDownloading = false;
    })
  }
  next() {
    if (this.users.length > 0) {
      this.page++;
      this.getUsers();
    }
  }
  previous() {

    if (this.page > 1) {
      this.page--;
      this.getUsers();

    }
  }
  search(tada): void {
    this.getUsers();

  }

  changeLanguage(): void {
    this.i18n.setLocale(this.isEnglish ? zh_CN : en_US);
    this.isEnglish = !this.isEnglish;
  }
}
