import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { UserService } from  '../../shared/user.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ApprovalService } from '../../shared/approval.service';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-am-info',
  templateUrl: './am-info.component.html',
  styleUrls: ['./am-info.component.css']
})
export class AmInfoComponent implements OnInit {
    approval = {
        firstName:'',
        lastName:'',
        email:'',
        username:'',
        phone:'',
        password:'',
        enabled:false
    }
  constructor(
      private approvalService:ApprovalService,
      private userService:UserService,
      private route:ActivatedRoute,
      private router:Router,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.view()
  }
  view(){
      this.spinner.show()
      var id = this.route.snapshot.params['appId']
      this.userService.getOne(id).toPromise().then((data:any)=>{
          this.approval = data.result
          this.spinner.hide()
      }).catch((e)=>{
          this.toaster.error(e.error.message)
          this.spinner.hide()
          console.log(e)
      })
  }
  delete(){
      this.spinner.show()
      var user_id = this.route.snapshot.params['appId']
      this.userService.deleteUser(user_id).toPromise().then((data:any)=>{
          this.toaster.success(data.message)
          this.spinner.hide()
          this.router.navigate(['/admin/approvalManagers'])
      }).catch((e)=>{
          this.toaster.error(e.error.message)
          this.spinner.hide()
          console.log(e)
      })
  }
  disable(){
      var id = this.route.snapshot.params['appId']
      var data = {
          enabled:false
      }
      this.userService.disable(id,data).toPromise().then((data:any)=>{
          console.log(data)
      }).catch((e)=>{
          console.log(e)
      })
  }
  enable(){
      var id = this.route.snapshot.params['appId']
      var data = {
          enabled:true
      }
      this.userService.disable(id,data).toPromise().then((data:any)=>{
          console.log(data)
      }).catch((e)=>{
          console.log(e)
      })
  }
}
