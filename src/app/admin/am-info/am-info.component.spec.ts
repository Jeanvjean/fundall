import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmInfoComponent } from './am-info.component';

describe('AmInfoComponent', () => {
  let component: AmInfoComponent;
  let fixture: ComponentFixture<AmInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
