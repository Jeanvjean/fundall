import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplainResponseComponent } from './complain-response.component';

describe('ComplainResponseComponent', () => {
  let component: ComplainResponseComponent;
  let fixture: ComponentFixture<ComplainResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplainResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplainResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
