import { DisburseService } from './../../shared/disburse.service';
import { ApprovalService } from './../../shared/approval.service';
import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { CcService } from '../../shared/cc.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
    selector: 'app-create-cc',
    templateUrl: './create-cc.component.html',
    styleUrls: ['./create-cc.component.css']
})
export class CreateCcComponent implements OnInit {
    cc = {
        firstName: '',
        lastName: '',
        email: '',
        username: '',
        phone: '',
        password: ''
    };
    toCall: any;
    title = "";
    type = "";
    isLoading = false;
    constructor(
        private ccService: CcService,
        private router: Router,
        private spinner: Ng4LoadingSpinnerService,
        private toaster: ToasterService,
        private route: ActivatedRoute,
        private appService: ApprovalService,
        private disburseService: DisburseService) { }

    ngOnInit() {
        this.toCall = this.ccService;
        this.type = this.route.snapshot.params['type'];
        //1 == cc,2 == ap,3==dm
        switch (this.type) {
            case "1":
                this.toCall = this.ccService;
                this.title = "Customer Care Agent"
                break;
            case "2":
                this.toCall = this.appService;
                this.title = "Approval Manager"
                break;
            case "3":
                this.toCall = this.disburseService
                this.title = "Disbursement Manager"
                break;


        }
    }
    goBack() {
        let param = "";
        if (this.type == "1") {
            param = "cc"
        }
        else if (this.type == "2") {
            param = "am"
        }
        else {
            param = "dm"
        }
        this.router.navigate(['/admin/user-management/' + param])
    }
    create() {
        var data = this.cc
        this.isLoading = true;
        this.toCall.create(data).toPromise().then((data: any) => {
            this.toaster.success(data.message)
            this.isLoading = false;
            this.goBack();
        }).catch((e: any) => {
            this.toaster.error(e.error.message);
            this.isLoading = false;

        })
    }
}
