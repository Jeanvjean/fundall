import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { DisburseService } from '../../shared/disburse.service';
import { UserService } from  '../../shared/user.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-d-info',
  templateUrl: './d-info.component.html',
  styleUrls: ['./d-info.component.css']
})
export class DInfoComponent implements OnInit {
    disburse = {
        firstName:'',
        lastName:'',
        email:'',
        username:'',
        phone:'',
        password:'',
        enabled:false
    }
  constructor(
      private disburseService:DisburseService,
      private userService:UserService,
      private route:ActivatedRoute,
      private router:Router,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.view()
  }
  view(){
      var id = this.route.snapshot.params['dId']
      this.userService.getOne(id).toPromise().then((data:any)=>{
          this.disburse = data.result
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  delete(){
      var user_id = this.route.snapshot.params['dId']
      this.userService.deleteUser(user_id).toPromise().then((data:any)=>{
          this.toaster.success(data.message)
          this.router.navigate(['/admin/disburseManagers'])
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  disable(){
      var id = this.route.snapshot.params['dId']
      var data = {
          enabled:false
      }
      this.userService.disable(id,data).toPromise().then((data:any)=>{
          console.log(data)
      }).catch((e)=>{
          console.log(e)
      })
  }
  enable(){
      var id = this.route.snapshot.params['dId']
      var data = {
          enabled:true
      }
      this.userService.disable(id,data).toPromise().then((data:any)=>{
          console.log(data)
      }).catch((e)=>{
          console.log(e)
      })
  }
}
