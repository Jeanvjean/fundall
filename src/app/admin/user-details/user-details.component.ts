import { BASE_API } from './../../shared/api.service';
import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { UserService } from '../../shared/user.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
    difImage="/assets/fundal/admin/Profile (3) (3).png";
    base_api = BASE_API;
    isLoading = true;
    user:any = {
        firstName:'',
        lastName:'',
        email:'',
        identificationImage:'',
        image:'',
        isVerified:false,
        phone:''
    };
  constructor(
      private userService:UserService,
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.getUser()
  }
  getUser(){
      this.spinner.show()
      this.isLoading = true;
      var user_id = this.route.snapshot.params['user_id']
      this.userService.userDetails(user_id).toPromise().then((data:any)=>{
          this.user = data.result;
          if(!this.user.image){
              this.user.image = "../assets/fundal/admin/Profile (3) (3).png";
          }
          else{
          this.user.image =  this.base_api+this.user.image;
          }
          this.difImage= data.result.identificationImage
          this.spinner.hide()
        //   console.log(data)
        this.isLoading = false;
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
        //   console.log(e)
        this.isLoading = false;        
      })
  }
  delete(){
      this.spinner.show()
      var user_id = this.route.snapshot.params['user_id']
      this.userService.delete(user_id).toPromise().then((data:any)=>{
          console.log(data)
          this.toaster.success(data.message)
          this.router.navigate(['/admin/user-management'])
          this.spinner.hide()
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  disable(user){
      var id = this.route.snapshot.params['user_id'];
      this.userService.toggle(id).toPromise().then((data:any)=>{
          if(data.success){
              this.user.enabled = !this.user.enabled;
              this.toaster.success("Updated Successfully");
          }
          else{
              this.user.enabled = false;
              this.toaster.error("An error occurred");
              
          }
      }).catch((e)=>{
          console.log(e)
      })
  }
  enable(){
    //   var id = this.route.snapshot.params['user_id']
    //   var data = {
    //       enabled:true
    //   }
    //   this.userService.disable(id,data).toPromise().then((data:any)=>{
    //       console.log(data)
    //   }).catch((e)=>{
    //       console.log(e)
    //   })
  }
}
