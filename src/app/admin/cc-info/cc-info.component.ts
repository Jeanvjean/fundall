import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { CcService } from '../../shared/cc.service';
import { UserService } from  '../../shared/user.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-cc-info',
  templateUrl: './cc-info.component.html',
  styleUrls: ['./cc-info.component.css']
})
export class CcInfoComponent implements OnInit {
    cc = {
        firstName:'',
        lastName:'',
        email:'',
        username:'',
        phone:'',
        password:'',
        enabled:false
    };
  constructor(
      private ccService:CcService,
      private userService:UserService,
      private route:ActivatedRoute,
      private router:Router,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.view()
  }
  view(){
      var id = this.route.snapshot.params['ccId']
      this.userService.getOne(id).toPromise().then((data:any)=>{
          this.cc = data.result
      }).catch((e)=>{
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  delete(){
      this.spinner.show()
      var user_id = this.route.snapshot.params['ccId']
      this.userService.deleteUser(user_id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          this.router.navigate(['/admin/customerCs'])
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  disable(){
      var id = this.route.snapshot.params['ccId']
      var data = {
          enabled:false
      }
      this.userService.disable(id,data).toPromise().then((data:any)=>{
          console.log(data)
      }).catch((e)=>{
          console.log(e)
      })
  }
  enable(){
      var id = this.route.snapshot.params['ccId']
      var data = {
          enabled:true
      }
      this.userService.disable(id,data).toPromise().then((data:any)=>{
          console.log(data)
      }).catch((e)=>{
          console.log(e)
      })
  }
}
