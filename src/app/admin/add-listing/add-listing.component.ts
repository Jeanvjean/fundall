import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { ListingService } from '../../shared/listing.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-add-listing',
  templateUrl: './add-listing.component.html',
  styleUrls: ['./add-listing.component.css']
})
export class AddListingComponent implements OnInit {
    listings = [];
    rl = [];
    p:number = 1;
    company = ''
  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private listingService:ListingService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.allListing()
      this.running_listing()
  }
  allListing(){
      this.spinner.show()
      this.listingService.getAll().toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.info(data.message)
          this.listings = data.result.listing
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          this.spinner.hide()
          console.log(e)
      })
  }
  running_listing(){
      this.spinner.show()
      this.listingService.running_listing().toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.info(data.message)
          this.rl = data.result.listing
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          // this.toaster.error(e.error.message)
          this.spinner.hide()
          console.log(e)
      })
  }
}
