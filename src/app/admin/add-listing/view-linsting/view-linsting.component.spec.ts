import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewLinstingComponent } from './view-linsting.component';

describe('ViewLinstingComponent', () => {
  let component: ViewLinstingComponent;
  let fixture: ComponentFixture<ViewLinstingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewLinstingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewLinstingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
