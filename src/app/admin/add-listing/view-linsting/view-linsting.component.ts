import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../../admin-nav/admin-nav.component';
import { ListingService } from '../../../shared/listing.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-view-linsting',
  templateUrl: './view-linsting.component.html',
  styleUrls: ['./view-linsting.component.css']
})
export class ViewLinstingComponent implements OnInit {
    listing = {
        name:'',
        company:'',
        date_created:'',
        description:'',
        active:'',
        _id:'',
        listingState:''
    }
    canApproveListing = false;
  constructor(
      private router:Router,
      private route:ActivatedRoute,
      private toaster:ToasterService,
      private listingService:ListingService,
      private spinner:Ng4LoadingSpinnerService) { }

  ngOnInit() {
      this.viewListing();
      this.canApproveListing = (localStorage.getItem('role') != "5") ? true : false;
  }
  viewListing(){
      this.spinner.show()
      var list_id = this.route.snapshot.params['id']
      this.listingService.viewListing(list_id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          this.listing = data.result
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  approve(){
      this.spinner.show()
      var list_id = this.route.snapshot.params['id']
      this.listingService.approve(list_id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  reject(){
      this.spinner.show()
      var list_id = this.route.snapshot.params['id']
      this.listingService.reject(list_id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
  delete(){
      this.spinner.show()
      var list_id = this.route.snapshot.params['id']
      this.listingService.delete(list_id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
