import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { LoanService } from '../../shared/loan.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-requested-loans',
  templateUrl: './requested-loans.component.html',
  styleUrls: ['./requested-loans.component.css']
})
export class RequestedLoansComponent implements OnInit {
    requests = [];
  constructor(
      private loanService:LoanService,
      private router:Router,
      private spinner:NgxSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.loanRequests()
  }
loanRequests(){
    this.spinner.show()
    this.loanService.getAllLoanRequest().toPromise().then((data:any)=>{
        this.requests = data.result
        this.toaster.success(data.message)
        this.spinner.hide()
    }).catch((e)=>{
        this.spinner.hide()
        this.toaster.error(e.error.message)
        console.log(e)
    })
}
}
