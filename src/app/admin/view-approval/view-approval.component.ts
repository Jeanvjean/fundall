import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { ApprovalService } from '../../shared/approval.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-view-approval',
  templateUrl: './view-approval.component.html',
  styleUrls: ['./view-approval.component.css']
})
export class ViewApprovalComponent implements OnInit {
    approval:any = '';
    page:number = 1;
    firstname = '';
    lastname = '';
  constructor(
      private approvalService:ApprovalService,
      private router:Router,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.getAll()
  }
  next(){
      if(this.approval.managers.length > 0){
          this.page++;
          this.getAll();
      }
  }
  previous(){
      if(this.page > 1){
          this.page--;
          this.getAll()
      }
  }
  getAll(){
      this.spinner.show()
      this.approvalService.getAll(this.page).toPromise().then((data:any)=>{
          console.log(data)
          this.approval = data.result
          this.spinner.hide()
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
