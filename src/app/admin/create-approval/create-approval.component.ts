import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { ApprovalService } from '../../shared/approval.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-create-approval',
  templateUrl: './create-approval.component.html',
  styleUrls: ['./create-approval.component.css']
})
export class CreateApprovalComponent implements OnInit {
    approval = {
        firstName:'',
        lastName:'',
        email:'',
        username:'',
        phone:'',
        password:''
    };
  constructor(
      private approvalService:ApprovalService,
      private router:Router,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
  }
  create(){
      this.spinner.show()
      var data = this.approval
      this.approvalService.create(data).toPromise().then((data:any)=>{
        console.log(data)
          this.toaster.success(data.message)
          this.spinner.hide()
          this.router.navigate(['/admin/approvalManagers'])
      }).catch((e)=>{
          console.log(e)
          this.spinner.hide()
          this.toaster.error(e.error.message)
      })
  }
}
