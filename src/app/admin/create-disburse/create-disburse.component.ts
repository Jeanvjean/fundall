import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { DisburseService } from '../../shared/disburse.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-create-disburse',
  templateUrl: './create-disburse.component.html',
  styleUrls: ['./create-disburse.component.css']
})
export class CreateDisburseComponent implements OnInit {
    disburse = {
        firstName:'',
        lastName:'',
        email:'',
        username:'',
        phone:'',
        password:''
    };
  constructor(
      private disburseService:DisburseService,
      private router:Router,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
  }
  create(){
      this.spinner.show()
      var data = this.disburse
      this.disburseService.create(data).toPromise().then((data:any)=>{
          this.spinner.hide()
              this.toaster.success(data.message)
              this.router.navigate(['/admin/disburseManagers'])
          console.log(data)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
