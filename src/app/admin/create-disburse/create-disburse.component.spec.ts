import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDisburseComponent } from './create-disburse.component';

describe('CreateDisburseComponent', () => {
  let component: CreateDisburseComponent;
  let fixture: ComponentFixture<CreateDisburseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDisburseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDisburseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
