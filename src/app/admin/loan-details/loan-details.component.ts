import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { LoanService } from '../../shared/loan.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-loan-details',
  templateUrl: './loan-details.component.html',
  styleUrls: ['./loan-details.component.css']
})
export class LoanDetailsComponent implements OnInit {
  lIcon = "/assets/fundal/loan-desc/Icons-01.png";
  lImage = "/assets/fundal/loan-desc/Pay-Loan.png";
  loan = {
      loanName:'',
      description:'',
      maxAmount:'',
      interestRate:'',
      minAmount:'',
      durationMax:'',
      durationMin:'',
      requirements:[],
      icon:'',
      image:'',
      _id:''
  }
  constructor(
      private loanService:LoanService,
      private router:Router,
      private route:ActivatedRoute,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
    this.getLoan()
  }
  getLoan(){
      this.spinner.show()
      var loan_id = this.route.snapshot.params['loan_id']
      this.loanService.loanDetail(loan_id).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
          this.loan = data
          this.lImage = data.image
          this.lIcon = data.icon
          // console.log(this.loan)
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          // console.log(e)
      })
  }
  delete(loan_id){
    this.loanService.deleteLoan(loan_id).toPromise().then((data:any)=>{
      // console.log(data)
      this.router.navigate(['/admin/loan-management'])
    }).catch((e)=>{
      // console.log(e)
    })
  }
}
