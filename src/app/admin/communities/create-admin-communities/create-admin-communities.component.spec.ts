import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAdminCommunitiesComponent } from './create-admin-communities.component';

describe('CreateAdminCommunitiesComponent', () => {
  let component: CreateAdminCommunitiesComponent;
  let fixture: ComponentFixture<CreateAdminCommunitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAdminCommunitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAdminCommunitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
