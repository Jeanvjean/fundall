import { Component, OnInit } from '@angular/core';
import { CooprativeService } from '../../../shared/cooprative.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ApprovalService } from '../../../shared/approval.service';
import { ToasterService } from '../../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
  selector: 'app-create-admin-communities',
  templateUrl: './create-admin-communities.component.html',
  styleUrls: ['./create-admin-communities.component.css']
})
export class CreateAdminCommunitiesComponent implements OnInit {
  photo_c:File = null;
  photo_w:File = null;
  cooprative = {
    name : '',
    location : ''
  };
  women = {
    name :'',
    location : ''
  }
  constructor(
    private cooprativeService:CooprativeService,
    private router:Router,
    private route:ActivatedRoute,
    private toaster:ToasterService,
    private spinner:Ng4LoadingSpinnerService) { }

  cooprative_photo(event){
      this.photo_c = <File>event.target.files[0]
  }
  women_in_trade_photo(event){
      this.photo_w = <File>event.target.files[0]
  }
  ngOnInit() {
  }

  create_c(){
    var fd = new FormData()
    fd.append('name',this.cooprative.name)
    fd.append('location',this.cooprative.location)
    fd.append('photo',this.photo_c.name)
    this.cooprativeService.create(fd).toPromise().then((data:any)=>{
      this.toaster.success(data.message)
      console.log(data)
    }).catch((e)=>{
      this.toaster.error(e.error.message)
      console.log(e)
    })
  }
  create_w(){
    var fd = new FormData()
    fd.append('name',this.women.name)
    fd.append('location',this.women.location)
    fd.append('photo',this.photo_w.name)
    this.cooprativeService.create(fd).toPromise().then((data:any)=>{
      this.toaster.success(data.message)
      console.log(data)
    }).catch((e)=>{
      this.toaster.error(e.error.message)
      console.log(e)
    })
  }
}
