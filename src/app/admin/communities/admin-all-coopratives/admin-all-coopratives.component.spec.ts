import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAllCooprativesComponent } from './admin-all-coopratives.component';

describe('AdminAllCooprativesComponent', () => {
  let component: AdminAllCooprativesComponent;
  let fixture: ComponentFixture<AdminAllCooprativesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAllCooprativesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAllCooprativesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
