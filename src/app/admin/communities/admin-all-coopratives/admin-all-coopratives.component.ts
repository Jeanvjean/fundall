import { Component, OnInit } from '@angular/core';
import { CooprativeService } from '../../../shared/cooprative.service';

@Component({
  selector: 'app-admin-all-coopratives',
  templateUrl: './admin-all-coopratives.component.html',
  styleUrls: ['./admin-all-coopratives.component.css']
})
export class AdminAllCooprativesComponent implements OnInit {

  constructor(private cooprativeService:CooprativeService) { }

  ngOnInit() {
    this.coopratives()
  }
  coopratives(){
    this.cooprativeService.allCoopratives().toPromise().then((data:any)=>{
      console.log(data)
    }).catch((e)=>{
      console.log(e)
    })
  }
}
