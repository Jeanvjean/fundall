import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWomenInTradeComponent } from './admin-women-in-trade.component';

describe('AdminWomenInTradeComponent', () => {
  let component: AdminWomenInTradeComponent;
  let fixture: ComponentFixture<AdminWomenInTradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWomenInTradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWomenInTradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
