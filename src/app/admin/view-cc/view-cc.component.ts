import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { CcService } from '../../shared/cc.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-view-cc',
  templateUrl: './view-cc.component.html',
  styleUrls: ['./view-cc.component.css']
})
export class ViewCcComponent implements OnInit {
    cc:any = '';
    page:number = 1;
    firstname = '';
    lastname = '';
  constructor(
      private ccService:CcService,
      private router:Router,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.getAll()
  }
      next(){
      if(this.cc.managers.manager.length > 0){
          this.page++;
          this.getAll();
      }
  }
  previous(){
      if(this.page > 1){
          this.page--;
          this.getAll()
      }
  }
  g
  getAll(){
      this.spinner.show()
      this.ccService.getAll(this.page).toPromise().then((data:any)=>{
          console.log(data)
          this.cc = data.result
          this.spinner.hide()
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
