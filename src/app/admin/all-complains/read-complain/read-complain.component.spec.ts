import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadComplainComponent } from './read-complain.component';

describe('ReadComplainComponent', () => {
  let component: ReadComplainComponent;
  let fixture: ComponentFixture<ReadComplainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadComplainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadComplainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
