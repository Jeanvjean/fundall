import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../../admin-nav/admin-nav.component';
import { ComplainService } from '../../../shared/complain.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../../shared/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-read-complain',
  templateUrl: './read-complain.component.html',
  styleUrls: ['./read-complain.component.css']
})
export class ReadComplainComponent implements OnInit {
    complain = {  }
    response = {

    }
  constructor(
      private complainService:ComplainService,
      private router:Router,
      private route:ActivatedRoute,
      private spinner:NgxSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.read()
  }
  read(){
      var comp_id = this.route.snapshot.params['comp_id']
      this.complainService.read(comp_id).toPromise().then((data:any)=>{
          this.complain = data.result
          this.spinner.hide()
      }).catch((e)=>{
          console.log(e)
      })
  }
  respond(){
      var comp_id = this.route.snapshot.params['comp_id']
      var data = this.response
      this.complainService.respondToComplain(comp_id,data).toPromise().then((data:any)=>{
          this.spinner.hide()
          this.toaster.success(data.message)
      }).catch((e)=>{
          this.spinner.hide()
          console.log(e)
      })
  }
}
