import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { ComplainService } from '../../shared/complain.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-all-complains',
  templateUrl: './all-complains.component.html',
  styleUrls: ['./all-complains.component.css']
})
export class AllComplainsComponent implements OnInit {
    complains = []
  constructor(
      private complainService:ComplainService,
      private router:Router,
      private spinner:NgxSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.fetchAll()
  }
  fetchAll(){
      this.complainService.getAllComplains().toPromise().then((data:any)=>{
          console.log(data)
          this.spinner.hide()
          this.complains = data.result
          this.toaster.success(data.message)
      }).catch((e)=>{
          console.log(e)
      })
  }
}
