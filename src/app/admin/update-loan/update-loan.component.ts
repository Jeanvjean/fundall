import { Component, OnInit } from '@angular/core';
import {BASE_API} from './../../shared/api.service';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { LoanService } from '../../shared/loan.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-update-loan',
  templateUrl: './update-loan.component.html',
  styleUrls: ['./update-loan.component.css']
})
export class UpdateLoanComponent implements OnInit {
  product_icon: File = null;
  product_image: File = null;
  isLoading = false;
  loanIcon = '/assets/fundal/admin/loan/_.png';
  loanImage = '/assets/fundal/admin/loan/_.png';
  images = [];
  icons = [];
  loan = {
      loanName: '',
      description: '',
      maxAmount: '',
      interestRate: '',
      minAmount: '',
      durationMax: '',
      durationMin: '',
      requirements: [{ name: "", type: "" }],
      order: '',
      icon:'',
      image:''
  }
  constructor(
      private loanService:LoanService,
      private router:Router,
      private route:ActivatedRoute,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

      onFileSelected(event) {
          // this.profilePic = <File>event.target.files[0]
          var files = event.target.files
          var filename = files.name

          const fileReader = new FileReader()
          fileReader.addEventListener('load', () => {
              this.loanImage = fileReader.result
          })
          fileReader.readAsDataURL(files[0])
          this.product_image = files[0]
      }
      fileSelected(event) {
          // this.LIcon = <File> event.target.files[0]
          var files = event.target.files
          var filename = files.name

          const fileReader = new FileReader()
          fileReader.addEventListener('load', () => {
              this.loanIcon = fileReader.result
          })
          fileReader.readAsDataURL(files[0])
          this.product_icon = files[0]
      }
  ngOnInit() {
      this.get_images()
      this.get_icons()
      this.get_loan()
  }
  addRequirement() {
      this.loan.requirements.push({ name: "", type: "" });
  }
  deleteRequirement(index) {
      this.loan.requirements.splice(index, 1);
  }
  trackByFn(index: any, item: any) {
      return index;
  }
  update(loan_id){
    console.log(this.loan)
    var error = false;
    for (var i in this.loan) {
        if (!this.loan[i]) {
            var field = i.replace("-", " ");
            field = field.toUpperCase();
            this.toaster.warning(`${field} IS REQUIRED`);
            error = true;
            return;
        }
    }
    if (!error) {
        this.isLoading = true;
        this.spinner.show()
        const fd = new FormData()
        fd.append('loanName', this.loan.loanName)
        fd.append('description', this.loan.description)
        fd.append('interestRate', this.loan.interestRate)
        fd.append('maxAmount', this.loan.maxAmount)
        fd.append('minAmount', this.loan.minAmount)
        fd.append('durationMax', this.loan.durationMax)
        fd.append('durationMin', this.loan.durationMin)
        var requirements = JSON.stringify(this.loan.requirements);
        fd.append('requirements', requirements)
        if (this.product_icon != null) {
            fd.append('product_icon', this.product_icon, this.product_icon.name)
        }
        if (this.product_image != null) {
            fd.append('product_image', this.product_image, this.product_image.name)
        }
        this.loanService.updateLoan(fd,loan_id).toPromise().then((data: any) => {
            console.log(data)
            this.spinner.hide()
            this.isLoading = false;
            this.toaster.success(data.result.message)
            this.router.navigate(['/admin/loan-management'])
        }).catch((e) => {
            this.spinner.hide()
            this.toaster.error(e.error.message);
            this.isLoading = false;
            console.log(e)
        })
    }
  }
  get_images(){
      this.loanService.loanImage().toPromise().then((data:any)=>{
          console.log(data)
          this.images = data.result
      }).catch((e)=>{
          console.log(e)
      })
  }
  get_icons(){
      this.loanService.loanIcons().toPromise().then((data:any)=>{
          console.log(data)
          this.icons = data.result
      }).catch((e)=>{
          console.log(e)
      })
  }
  get_loan(){
    var loan_id = this.route.snapshot.params['loan_id']
      this.loanService.loanDetail(loan_id).toPromise().then((data:any)=>{
          console.log(data)
          this.loan = data
          this.loanIcon = BASE_API + this.loan.icon
          this.loanImage = BASE_API + this.loan.image
      }).catch((e)=>{
          console.log(e)
      })
  }
}
