import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'admin-nav',
  templateUrl: './admin-nav.component.html',
  styleUrls: ['./admin-nav.component.css']
})
export class AdminNavComponent implements OnInit {
  role = localStorage.getItem("role");
  constructor(
      private router:Router) { }

  ngOnInit() {
  }
  logout(){
      localStorage.clear()
      this.router.navigate(['/signin'])
  }
}
