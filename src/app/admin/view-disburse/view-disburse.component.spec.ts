import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDisburseComponent } from './view-disburse.component';

describe('ViewDisburseComponent', () => {
  let component: ViewDisburseComponent;
  let fixture: ComponentFixture<ViewDisburseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDisburseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDisburseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
