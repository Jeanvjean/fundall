import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { DisburseService } from '../../shared/disburse.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-view-disburse',
  templateUrl: './view-disburse.component.html',
  styleUrls: ['./view-disburse.component.css']
})
export class ViewDisburseComponent implements OnInit {
    disburse:any = '';
    page:number = 1;
    firstname = '';
    lastname = '';
  constructor(
      private disburseService:DisburseService,
      private router:Router,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.getAll()
  }
    next(){
      if(this.disburse.manager.length > 0){
          this.page++;
          this.getAll();
      }
  }
  previous(){
      if(this.page > 1){
          this.page--;
          this.getAll()
      }
  }
  getAll(){
      this.spinner.show()
      this.disburseService.getAll(this.page).toPromise().then((data:any)=>{
          console.log(data)
          this.disburse = data.result
          this.spinner.hide()
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          console.log(e)
      })
  }
}
