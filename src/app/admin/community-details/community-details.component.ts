import { ToasterService } from './../../shared/toaster.service';
import { LoanService } from './../../shared/loan.service';
import { Component, OnInit } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
  selector: 'app-community-details',
  templateUrl: './community-details.component.html',
  styleUrls: ['./community-details.component.css']
})
export class CommunityDetailsComponent implements OnInit {
  cooperative = { name: "", description: "", requirements: [""], image: "" }
  constructor(private loanService: LoanService, private spinner: Ng4LoadingSpinnerService,
    private toaster: ToasterService) { }

  ngOnInit() {
  }

  addRequirement() {
    this.cooperative.requirements.push("");
  }
  deleteRequirement(index) {
    this.cooperative.requirements.splice(index, 1);
  }
  fileSelected(event) {
    // this.LIcon = <File> event.target.files[0]
    var files = event.target.files
    var filename = files.name

    const fileReader = new FileReader()
    fileReader.addEventListener('load', () => {
      this.cooperative.image = fileReader.result
    })
    fileReader.readAsDataURL(files[0])
    this.cooperative.image = files[0]
  }
  trackByFn(index: any, item: any) {
    return index;
  }
  createCoperative() {
    this.spinner.show()
    var formData = new FormData();
    var self = this;
    for (var key in self.cooperative) {
      formData.append(key, self.cooperative[key]);
    }
    this.loanService.createCoperative(formData).toPromise().then((data:any) => {
      this.spinner.hide()
      this.toaster.success(data.message)
    }).catch((e) => {
      this.spinner.hide()
      // this.toaster.error(e.error.message)
    })
  }

}
