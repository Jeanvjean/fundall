import { Component, OnInit } from '@angular/core';
import { AdminNavComponent } from '../admin-nav/admin-nav.component';
import { LoanService } from '../../shared/loan.service';
import { Router } from '@angular/router';
import { ToasterService } from '../../shared/toaster.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
// import * as moment from 'moment';
import * as GLOBAL from  '../../shared/api.service';

// base
@Component({
  selector: 'app-view-loans',
  templateUrl: './view-loans.component.html',
  styleUrls: ['./view-loans.component.css']
})
export class ViewLoansComponent implements OnInit {
    loans:any = '';
    baseApi = GLOBAL.BASE_API;
    isLoading = false;
  constructor(
      private loanService:LoanService,
      private router:Router,
      private spinner:Ng4LoadingSpinnerService,
      private toaster:ToasterService) { }

  ngOnInit() {
      this.getLoan()
  }
  getLoan(){
      this.spinner.show()
      this.loanService.getLoan().toPromise().then((data:any)=>{
          // console.log(data)
          this.loans = data.result
          this.spinner.hide()
      }).catch((e)=>{
          this.spinner.hide()
          this.toaster.error(e.error.message)
          // console.log(e)
      })
  }
}
