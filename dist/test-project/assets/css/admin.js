import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'body': {
    'fontFamily': '"Nunito Sans", sans-serif',
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'overflowX': 'hidden'
  },
  'container': {
    'width': [{ 'unit': '%H', 'value': 0.9 }, { 'unit': 'string', 'value': '!important' }]
  },
  'community': {
    'marginTop': [{ 'unit': 'px', 'value': 70 }]
  },
  'nav-header': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'nav-header nav-link': {
    'color': '#000',
    'textTransform': 'capitalize',
    'fontSize': [{ 'unit': 'px', 'value': 24 }],
    'lineHeight': [{ 'unit': 'px', 'value': 33 }]
  },
  'import': 'url('https://fonts.googleapis.com/css?family=Nunito+Sans')',
  'navbar-auth': {
    'padding': [{ 'unit': 'px', 'value': 25 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 25 }, { 'unit': 'px', 'value': 8 }],
    'background': '#FFFFFF',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 25 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, 0.25)' }]
  },
  'navbar-auth atext': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'normal',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 24 }],
    'color': '#222233'
  },
  'nav-header': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'body-wrapauth': {
    'background': '#F2F2F2'
  },
  'auth-card': {
    'background': '#FFFFFF',
    // border: 0.5px solid #BDBDBD;
    'boxSizing': 'border-box',
    'boxShadow': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'string', 'value': 'rgba(51, 51, 51, 0.25)' }],
    'borderRadius': '10px',
    'padding': [{ 'unit': 'px', 'value': 150 }, { 'unit': 'px', 'value': 45 }, { 'unit': 'px', 'value': 150 }, { 'unit': 'px', 'value': 45 }]
  },
  'auth-card h2': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': '300',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 36 }],
    'color': '#222233',
    'marginBottom': [{ 'unit': 'px', 'value': 42 }]
  },
  'auth-card label': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': '300',
    'lineHeight': [{ 'unit': 'px', 'value': 30 }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'color': '#222233'
  },
  'auth-card input': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#C3C3C3' }],
    'boxSizing': 'border-box',
    'borderRadius': '25px'
  },
  'auth-card button': {
    'marginTop': [{ 'unit': 'px', 'value': 40 }]
  },
  'page-header': {
    'padding': [{ 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }],
    'background': '#FFFFFF',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 25 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, 0.08)' }],
    'borderRadius': '5px',
    'margin': [{ 'unit': 'px', 'value': 40 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 40 }, { 'unit': 'px', 'value': 0 }]
  },
  'activity-box': {
    'width': [{ 'unit': '%H', 'value': 1 }],
    'padding': [{ 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }],
    'background': '#FFFFFF',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 25 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, 0.08)' }],
    'borderRadius': '3px',
    'marginBottom': [{ 'unit': 'px', 'value': 30 }],
    'cursor': 'pointer',
    'justifyContent': 'center',
    'display': 'flex',
    'flexDirection': 'column',
    'alignItems': 'center',
    'height': [{ 'unit': 'px', 'value': 256 }]
  },
  'activity-box img': {
    'width': [{ 'unit': 'px', 'value': 101 }],
    'height': [{ 'unit': 'px', 'value': 101 }]
  },
  'activity-box p': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'normal',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'textAlign': 'center',
    'color': '#828282'
  },
  'notice-box': {
    'background': '#FFFFFF',
    'borderRadius': '10px',
    'padding': [{ 'unit': 'px', 'value': 29 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 29 }, { 'unit': 'px', 'value': 30 }],
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 25 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, 0.08)' }]
  },
  'notice-box h4': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': '600',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'textAlign': 'center',
    'color': '#52BC00'
  },
  'notice-box line': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#52BC00' }],
    'margin': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 28 }, { 'unit': 'px', 'value': 0 }]
  },
  'notice-box ul li': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'normal',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'marginBottom': [{ 'unit': 'px', 'value': 27 }],
    'color': '#828282'
  },
  'big-data-box': {
    'background': '#FFFFFF',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#BDBDBD' }],
    'boxSizing': 'border-box',
    'borderRadius': '5px'
  },
  'big-data-box head': {
    'padding': [{ 'unit': 'px', 'value': 17 }, { 'unit': 'px', 'value': 40 }, { 'unit': 'px', 'value': 13 }, { 'unit': 'px', 'value': 40 }]
  },
  'big-data-box body': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 30 }]
  },
  'big-data-box head h3': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'normal',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'color': '#222233'
  },
  'big-data-box head i': {
    'fontWeight': '600',
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'color': '#222233'
  },
  'big-data-box head p': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'bold',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'display': 'inline',
    'color': '#828282'
  },
  'big-data-box head dropdownstyled button': {
    'background': '#FFFFFF',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, 0.25)' }],
    'borderRadius': '20px',
    'padding': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 16 }],
    'textAlign': 'center',
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'normal',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 11 }],
    'color': '#BDBDBD'
  },
  'big-data-box head input': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#C4C4C4' }],
    'boxSizing': 'border-box',
    'borderRadius': '25px',
    'marginTop': [{ 'unit': 'px', 'value': 11 }]
  },
  'big-data-box head input ~i': {
    'position': 'absolute',
    'right': [{ 'unit': 'px', 'value': 30 }],
    'top': [{ 'unit': 'px', 'value': 50 }],
    'color': '#828282'
  },
  'big-data-box p': {
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'color': '#828282'
  },
  // utils
  'b-b': {
    'borderBottom': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'grey' }]
  },
  'b-r': {
    'borderRight': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'grey' }]
  },
  'border-red': {
    'borderColor': '#EB5757'
  },
  'border-yellow': {
    'borderColor': '#FFC40E'
  },
  'text-yellow': {
    'color': '#FFC40E'
  },
  'border-green': {
    'borderColor': '#52BC00'
  },
  'border-greened': {
    'borderColor': '#219653'
  },
  'border-blue': {
    'borderColor': '#2D9CDB'
  },
  'border-dark-blue': {
    'borderColor': '#004C4C'
  },
  'text-blue': {
    'color': '#2D9CDB'
  },
  'text-grey': {
    'color': '#828282 !important'
  },
  'text-red': {
    'color': '#EB5757 !important'
  },
  'text-green': {
    'color': '#52BC00'
  },
  'text-lilac': {
    'color': '#00BFBF'
  },
  'border-lilac': {
    'borderColor': '#00BFBF'
  },
  'border-orange': {
    'borderColor': '#F2994A'
  },
  'text-pink': {
    'color': '#F559E4'
  },
  'border-pink': {
    'borderColor': '#f559e4'
  },
  'border-light-pink': {
    'borderColor': '#F771E8'
  },
  'text-light-pink': {
    'color': '#F771E8'
  },
  'text-purple': {
    'color': '#8B66B5'
  },
  'border-purple': {
    'borderColor': '#8B66B5'
  },
  'line': {
    'border': [{ 'unit': 'px', 'value': 0.5 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#BDBDBD' }],
    'margin': [{ 'unit': 'px', 'value': 45 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 45 }, { 'unit': 'px', 'value': 0 }]
  },
  'pt-60': {
    'paddingTop': [{ 'unit': 'px', 'value': 60 }, { 'unit': 'string', 'value': '!important' }]
  },
  'pt-40': {
    'paddingTop': [{ 'unit': 'px', 'value': 40 }, { 'unit': 'string', 'value': '!important' }]
  },
  'pt-50': {
    'paddingTop': [{ 'unit': 'px', 'value': 50 }, { 'unit': 'string', 'value': '!important' }]
  },
  'pt-55': {
    'paddingTop': [{ 'unit': 'px', 'value': 55 }, { 'unit': 'string', 'value': '!important' }]
  },
  'pt-30': {
    'paddingTop': [{ 'unit': 'px', 'value': 30 }, { 'unit': 'string', 'value': '!important' }]
  },
  'pt-20': {
    'paddingTop': [{ 'unit': 'px', 'value': 20 }, { 'unit': 'string', 'value': '!important' }]
  },
  'pt-70': {
    'paddingTop': [{ 'unit': 'px', 'value': 70 }, { 'unit': 'string', 'value': '!important' }]
  },
  'pt-100': {
    'paddingTop': [{ 'unit': 'px', 'value': 100 }, { 'unit': 'string', 'value': '!important' }]
  },
  'pt-300': {
    'paddingTop': [{ 'unit': 'px', 'value': 300 }, { 'unit': 'string', 'value': '!important' }]
  },
  'pb-30': {
    'paddingBottom': [{ 'unit': 'px', 'value': 30 }, { 'unit': 'string', 'value': '!important' }]
  },
  'px-28': {
    'paddingLeft': [{ 'unit': 'px', 'value': 28 }, { 'unit': 'string', 'value': '!important' }],
    'paddingRight': [{ 'unit': 'px', 'value': 28 }, { 'unit': 'string', 'value': '!important' }]
  },
  'pb-77': {
    'paddingBottom': [{ 'unit': 'px', 'value': 77 }, { 'unit': 'string', 'value': '!important' }]
  },
  'display-inline': {
    'display': 'inline'
  },
  'ulcolored-list': {
    'listStyle': 'none',
    // Remove list bullets
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'ulcolored-list li:before': {
    'content': '"•"',
    'paddingRight': [{ 'unit': 'px', 'value': 8 }],
    'color': 'black'
  },
  'ulcolored-list liblueish:before': {
    'color': '#2F80ED'
  },
  'ulcolored-list ligreenish:before': {
    'color': '#63C8B0'
  },
  'btn-outline': {
    'border': [{ 'unit': 'px', 'value': 0.5 }, { 'unit': 'string', 'value': 'solid' }],
    'boxSizing': 'border-box',
    'borderRadius': '3px',
    'background': '#ffffff',
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 17 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 17 }],
    'cursor': 'pointer',
    'textAlign': 'center'
  },
  'navigation-back': {
    'cursor': 'pointer',
    'position': 'relative',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'right': [{ 'unit': 'px', 'value': 50 }],
    'marginBottom': [{ 'unit': 'px', 'value': 30 }]
  },
  'navigation-backtype-2': {
    'right': [{ 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'px', 'value': 20 }]
  },
  'navigation-back i': {
    'fontSize': [{ 'unit': 'px', 'value': 20 }],
    'color': '#52BC00'
  },
  'navigation-back h2': {
    'marginLeft': [{ 'unit': 'px', 'value': 29 }],
    'fontStyle': 'normal',
    'display': 'inline',
    'fontWeight': 'bold',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'color': '#52BC00'
  },
  'section-main-body': {
    'padding': [{ 'unit': 'px', 'value': 60 }, { 'unit': 'px', 'value': 60 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 120 }],
    'height': [{ 'unit': 'px', 'value': 575 }],
    'contain': 'content',
    'overflowX': 'hidden',
    'overflowY': 'scroll',
    '<w500': {
      'padding': [{ 'unit': 'px', 'value': 64 }, { 'unit': 'px', 'value': 60 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 13 }],
      'height': [{ 'unit': 'px', 'value': 568 }],
      'contain': 'content',
      'overflowX': 'hidden',
      'overflowY': 'scroll'
    }
  },
  'ml-5-lg': {
    'marginLeft': [{ 'unit': 'rem', 'value': 3 }, { 'unit': 'string', 'value': '!important' }]
  },
  'fit-width': {
    'width': [{ 'unit': 'string', 'value': 'fit-content' }]
  },
  'visible-xs': {
    'display': 'none'
  },
  'btn': {
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'string', 'value': '#92919178' }],
    'cursor': 'pointer'
  },
  'btn:hover': {
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'string', 'value': '#92919178' }]
  },
  'btn:active': {
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'string', 'value': '#92919178' }]
  },
  'btn-md': {
    'width': [{ 'unit': 'px', 'value': 200 }, { 'unit': 'string', 'value': '!important' }]
  },
  // utils end here
  'big-data-box body table': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 65 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 65 }]
  },
  'big-data-box body table th': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': '600',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'color': '#52BC00'
  },
  'big-data-box body tablered th': {
    'color': '#FA6541'
  },
  'big-data-box body tableblue th': {
    'color': '#2D9CDB'
  },
  'big-data-box body tableyellow th': {
    'color': '#FFC30E'
  },
  'big-data-box body table td': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': '300',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'color': '#222233'
  },
  'form-box': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#E5E5E5' }],
    'padding': [{ 'unit': 'px', 'value': 40 }, { 'unit': 'px', 'value': 60 }, { 'unit': 'px', 'value': 40 }, { 'unit': 'px', 'value': 60 }]
  },
  'form-box h4': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'bold',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'marginBottom': [{ 'unit': 'px', 'value': 25 }],
    'color': '#52BC00'
  },
  'form-box label': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'normal',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'color': '#828282'
  },
  'form-box input': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#BDBDBD' }],
    'boxSizing': 'border-box',
    'borderRadius': '25px'
  },
  'b-t': {
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'grey' }]
  },
  'textareasuperide': {
    'position': 'absolute',
    'width': [{ 'unit': 'px', 'value': 450 }],
    'height': [{ 'unit': 'px', 'value': 123 }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#BDBDBD' }],
    'boxSizing': 'border-box',
    'borderRadius': '5px'
  },
  // check box util starts
  // The container
  'container': {
    'cursor': 'pointer',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none'
  },
  'user-img-box': {
    'width': [{ 'unit': 'px', 'value': 422 }],
    'height': [{ 'unit': 'px', 'value': 345 }]
  },
  'user-img-box img': {
    'background': '#FFFFFF',
    'border': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#C4C4C4' }],
    'boxSizing': 'border-box',
    'borderRadius': '100%'
  },
  'user-img-box-in img': {
    'background': '#FFFFFF',
    'border': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#C4C4C4' }],
    'boxSizing': 'border-box',
    'borderRadius': '100%'
  },
  'iformed': {
    'position': 'absolute',
    'right': [{ 'unit': 'px', 'value': 60 }],
    'top': [{ 'unit': 'px', 'value': 5 }],
    'color': '#828282'
  },
  // Hide the browser's default checkbox
  'container checkbox-styled': {
    'position': 'absolute',
    'opacity': '0',
    'cursor': 'pointer'
  },
  // Create a custom checkbox
  'checkmark': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': 'px', 'value': 25 }],
    'width': [{ 'unit': 'px', 'value': 25 }],
    'backgroundColor': '#eee',
    'borderRadius': '4px'
  },
  // On mouse-over, add a grey background color
  'container:hover input ~ checkmark': {
    'backgroundColor': '#ccc'
  },
  // When the checkbox is checked, add a blue background
  'container input:checked ~ checkmark': {
    'backgroundColor': '#52BC00'
  },
  // Create the checkmark/indicator (hidden when not checked)
  'checkmark:after': {
    'content': '""',
    'position': 'absolute',
    'display': 'none'
  },
  // Show the checkmark when checked
  'container input:checked ~ checkmark:after': {
    'display': 'block'
  },
  // Style the checkmark/indicator
  'container checkmark:after': {
    'left': [{ 'unit': 'px', 'value': 9 }],
    'top': [{ 'unit': 'px', 'value': 5 }],
    'width': [{ 'unit': 'px', 'value': 5 }],
    'height': [{ 'unit': 'px', 'value': 10 }],
    'border': [{ 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'white' }],
    'borderWidth': '0 3px 3px 0',
    'WebkitTransform': 'rotate(45deg)',
    'MsTransform': 'rotate(45deg)',
    'transform': 'rotate(45deg)'
  },
  // check box util ends
  'loan-box': {
    'background': '#FFFFFF',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 25 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, 0.25)' }],
    'borderRadius': '5px',
    'marginTop': [{ 'unit': 'px', 'value': 70 }],
    'marginBottom': [{ 'unit': 'px', 'value': 30 }]
  },
  'loan-box main-img': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'loan-box table': {
    'margin': [{ 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }]
  },
  'loan-box table th': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'normal',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 24 }],
    'paddingBottom': [{ 'unit': 'px', 'value': 16 }]
  },
  'loan-box table td': {
    'paddingBottom': [{ 'unit': 'px', 'value': 14 }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'lineHeight': [{ 'unit': 'px', 'value': 19 }]
  },
  'loan-box table': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'loan-box box-fab': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': -45 }],
    'right': [{ 'unit': 'px', 'value': -25 }],
    'background': '#FFFFFF',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 25 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, 0.25)' }],
    'borderRadius': '100%',
    'width': [{ 'unit': 'px', 'value': 152 }],
    'height': [{ 'unit': 'px', 'value': 152 }]
  },
  'loan-box box-fab img': {
    'width': [{ 'unit': '%H', 'value': 1 }],
    'display': 'block',
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'auto' }]
  },
  'loan-box box-action': {
    'position': 'absolute',
    'bottom': [{ 'unit': 'px', 'value': 20 }],
    'right': [{ 'unit': 'px', 'value': 40 }]
  },
  'loan-box box-action p': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'bold',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'color': '#222233',
    'textAlign': 'center',
    'position': 'relative',
    'right': [{ 'unit': 'px', 'value': 12 }]
  },
  'box': {
    'background': '#FFFFFF',
    'border': [{ 'unit': 'px', 'value': 0.5 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#828282' }],
    'boxSizing': 'border-box',
    'borderRadius': '5px',
    'padding': [{ 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }],
    'marginBottom': [{ 'unit': 'px', 'value': 33 }],
    'display': 'flex',
    'flexDirection': 'column',
    'justifyContent': 'center',
    'alignItems': 'center'
  },
  'box h5': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'bold',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }]
  },
  'box circle': {
    'background': '#FFFFFF',
    'border': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#C4C4C4' }],
    'boxSizing': 'border-box',
    'marginTop': [{ 'unit': 'px', 'value': 28 }],
    'borderRadius': '50%',
    'overflow': 'hidden',
    'width': [{ 'unit': 'px', 'value': 150 }],
    'height': [{ 'unit': 'px', 'value': 150 }],
    'display': 'flex',
    'justifyContent': 'center',
    'alignItems': 'center',
    'position': 'relative'
  },
  'box circle img': {
    'maxWidth': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'box photo-upload-group buttonaction': {
    'background': 'linear-gradient(180deg, #93D802 0%, #04B648 100%), #222233',
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }],
    'borderRadius': '100%',
    'right': [{ 'unit': 'px', 'value': 140 }],
    'bottom': [{ 'unit': 'px', 'value': 125 }],
    'position': 'absolute'
  },
  'box buttonmainaction': {
    'marginTop': [{ 'unit': 'px', 'value': 42 }],
    'width': [{ 'unit': 'px', 'value': 200 }]
  },
  'imgmiddleaction': {
    'marginTop': [{ 'unit': 'px', 'value': 150 }],
    'marginBottom': [{ 'unit': 'px', 'value': 14 }]
  },
  'imgmiddleaction ~p': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'normal',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'textAlign': 'center',
    'color': '#828282'
  },
  'pill': {
    'background': '#FFFFFF',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, 0.08)' }],
    'borderRadius': '3px',
    'padding': [{ 'unit': 'px', 'value': 25 }, { 'unit': 'px', 'value': 25 }, { 'unit': 'px', 'value': 25 }, { 'unit': 'px', 'value': 25 }],
    'marginBottom': [{ 'unit': 'px', 'value': 40 }]
  },
  'pill h4': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': '600',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'letterSpacing': [{ 'unit': 'px', 'value': 1 }],
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'color': '#52BC00'
  },
  'pill h5': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': '600',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'letterSpacing': [{ 'unit': 'px', 'value': 1 }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'color': '#52BC00'
  },
  'pill p': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'normal',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'color': '#2D9CDB'
  },
  'pill imgaction': {
    'width': [{ 'unit': 'px', 'value': 32 }],
    'height': [{ 'unit': 'px', 'value': 32 }],
    'marginBottom': [{ 'unit': 'px', 'value': 21 }]
  },
  'textarea-styled': {
    'height': [{ 'unit': 'px', 'value': 153 }, { 'unit': 'string', 'value': '!important' }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#BDBDBD' }],
    'boxSizing': 'border-box',
    'borderRadius': '25px'
  },
  'inputloan-form-styled': {
    'height': [{ 'unit': 'px', 'value': 27 }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#C3C3C3' }],
    'boxSizing': 'border-box',
    'borderRadius': '25px',
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'normal',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 13 }],
    'padding': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 16 }],
    'marginBottom': [{ 'unit': 'px', 'value': 21 }],
    'color': '#828282'
  },
  'btn-loan-form-dropdown': {
    'width': [{ 'unit': 'px', 'value': 176 }],
    'height': [{ 'unit': 'px', 'value': 27 }],
    'background': '#ffffff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#C3C3C3' }],
    'boxSizing': 'border-box',
    'borderRadius': '25px',
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'normal',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 13 }],
    'padding': [{ 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 6 }],
    'color': '#828282'
  },
  'x-ha': {
    'position': 'absolute',
    'right': [{ 'unit': 'px', 'value': -5 }],
    'top': [{ 'unit': 'px', 'value': 0 }]
  },
  'x-ha-1': {
    'position': 'absolute',
    'right': [{ 'unit': 'px', 'value': -5 }],
    'top': [{ 'unit': 'px', 'value': 55 }]
  },
  // slider util starts
  'slidecontainer': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'slider': {
    'WebkitAppearance': 'none',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': 'px', 'value': 12 }],
    'background': '#ECECEC',
    'borderRadius': '10px',
    'outline': 'none',
    'opacity': '0.7',
    'WebkitTransition': '.2s',
    'transition': 'opacity .2s'
  },
  'slider:hover': {
    'opacity': '1'
  },
  'slider::-webkit-slider-thumb': {
    'WebkitAppearance': 'none',
    'appearance': 'none',
    'width': [{ 'unit': 'px', 'value': 24 }],
    'height': [{ 'unit': 'px', 'value': 24 }],
    'background': '#70E516',
    'cursor': 'pointer',
    'borderRadius': '100%'
  },
  'slider::-moz-range-thumb': {
    'width': [{ 'unit': 'px', 'value': 24 }],
    'height': [{ 'unit': 'px', 'value': 24 }],
    'background': '#70E516',
    'cursor': 'pointer',
    'borderRadius': '100%'
  },
  // slider util ends
  'listing-box': {
    'background': '#FFFFFF',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 25 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, 0.25)' }],
    'borderRadius': '5px',
    'paddingBottom': [{ 'unit': 'px', 'value': 55 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': 'px', 'value': 256 }],
    'marginBottom': [{ 'unit': 'px', 'value': 40 }]
  },
  'listing-box imgplus': {
    'position': 'absolute',
    'right': [{ 'unit': 'px', 'value': 150 }],
    'top': [{ 'unit': 'px', 'value': 11 }]
  },
  'listing-box h5': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'normal',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 24 }],
    'textAlign': 'center',
    'color': '#828282'
  },
  'listing-box p': {
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': '300',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'textAlign': 'center',
    'color': '#828282'
  },
  'labelbadge': {
    'background': '#52BC00',
    'borderRadius': '10px',
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': '600',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'height': [{ 'unit': 'px', 'value': 20 }],
    'color': '#FFFFFF',
    'padding': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }],
    'textAlign': 'center'
  },
  // width
  '::-webkit-scrollbar': {
    'width': [{ 'unit': 'px', 'value': 8 }]
  },
  // Track
  '::-webkit-scrollbar-track': {
    'background': '#f1f1f1'
  },
  // Handle
  '::-webkit-scrollbar-thumb': {
    'background': '#52bc0099',
    'borderRadius': '4px'
  },
  // Handle on hover
  '::-webkit-scrollbar-thumb:hover': {
    'background': '#52BC00'
  },
  'loan-box table tr:last-child td': {
    'paddingBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  // START NAVIGATION STYLE
  'linav-item': {
    'padding': [{ 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 20 }],
    'display': 'flex'
  },
  'linav-item nav-link': {
    'textTransform': 'capitalize',
    'color': '#fff',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'textTransform': 'capitalize',
    'lineHeight': [{ 'unit': 'px', 'value': 19 }],
    'fontWeight': '700'
  },
  'linav-itemactive': {
    'backgroundColor': '#52BC00'
  },
  'ulnavbar-nav': {
    'width': [{ 'unit': '%H', 'value': 1 }],
    'flexDirection': 'column',
    'justifyContent': 'space-between',
    'height': [{ 'unit': 'vh', 'value': 80 }]
  },
  'home-sidebar': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'dashboard-header': {
    'background': '#fff',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 25 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0,0,0,0.12)' }],
    'padding': [{ 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 20 }],
    'height': [{ 'unit': 'vh', 'value': 10 }],
    'only screen&&<w1199': {
      'height': [{ 'unit': 'string', 'value': 'auto' }]
    }
  },
  'admin-dashboard layout': {
    'height': [{ 'unit': 'vh', 'value': 90 }]
  },
  'admin-dashboard layout > col-md-2': {
    'flex': '0 0 16%',
    'maxWidth': [{ 'unit': '%H', 'value': 0.16 }],
    'paddingRight': [{ 'unit': 'px', 'value': 0 }],
    'backgroundColor': '#222233'
  },
  'admin-dashboard layout > col-md-10': {
    'flex': '0 0 84%',
    'maxWidth': [{ 'unit': '%H', 'value': 0.84 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'overflowY': 'auto'
  },
  'right-content': {
    'minHeight': [{ 'unit': 'vh', 'value': 90 }]
  },
  // END NAVIGATION STYLE
  // BUTTON STYLE
  'btn-styled': {
    'background': 'linear-gradient(180deg, #93D802 0%, #04B648 100%)',
    'boxShadow': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'string', 'value': 'rgba(189, 189, 189, 0.5)' }],
    'borderRadius': '25px',
    'padding': [{ 'unit': 'px', 'value': 7 }, { 'unit': 'px', 'value': 36 }, { 'unit': 'px', 'value': 7 }, { 'unit': 'px', 'value': 36 }],
    'textAlign': 'center',
    'fontFamily': 'Nunito Sans',
    'fontStyle': 'normal',
    'fontWeight': 'bold',
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'color': '#FFFFFF'
  },
  'sidebar': {
    'height': [{ 'unit': 'px', 'value': 800 }, { 'unit': 'string', 'value': '!important' }]
  },
  'btn-back-to-topback-to-top-is-visible': {
    'background': 'linear-gradient(180deg, #93D802 0%, #04B648 100%)',
    'boxShadow': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'string', 'value': 'rgba(189, 189, 189, 0.5)' }]
  },
  // PAGE HEADER STYLE
  'page-header': {
    'padding': [{ 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'px', 'value': 30 }],
    'background': '#FFFFFF',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 30 }, { 'unit': 'string', 'value': 'rgba(0, 0, 0, 0.08)' }],
    'borderRadius': '5px',
    'marginBottom': [{ 'unit': 'px', 'value': 40 }, { 'unit': 'px', 'value': 0 }]
  },
  'page-header-title': {
    'fontSize': [{ 'unit': 'px', 'value': 24 }],
    'lineHeight': [{ 'unit': 'px', 'value': 33 }],
    'fontWeight': '300'
  }
});
